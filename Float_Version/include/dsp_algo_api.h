﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com

#ifndef _DSP_ALGO_API_H_
#define _DSP_ALGO_API_H_
#include "dsp_public.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    /* handle of each module */
    void* ptr_envp;
    void* ptr_edet;
    void* ptr_ampd;
    void* ptr_ftc;
    void* ptr_nfd;
	void* ptr_nfd2;

    int16_t envp_key;
    int16_t ampd_key;
    int16_t edet_key;
    int16_t nfd_key;

} DspParam;
/*
    input:
        -len: samples per frame
        -sample_rate: sample rate at least 4fc(fc：58kHz)
*/
void dsp_init_api(DspParam* inst, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output, int* mod_switch);
int16_t dsp_process_api(DspParam* inst, DspInputOutput* input_output, DspConfig* dsp_cfg, float indata);
void dsp_destroy_api(DspParam* inst, DspConfig* dsp_cfg, DspInputOutput* input_output);

#ifdef __cplusplus
}
#endif
#endif // _DSP_ALGO_API_H_
