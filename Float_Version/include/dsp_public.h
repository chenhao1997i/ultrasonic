#ifndef _DSP_PUBLIC_H_
#define _DSP_PUBLIC_H_
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#define M_PI 3.14159265358979323846
#ifdef __cplusplus
extern "C" {
#endif


static float gain_array[] = { 1.000000,1.044359,1.090687,1.139069,1.189597,1.242367,1.297478,1.355033,1.415142,1.477917,1.543476,1.611944,1.683449,1.758126,1.836116,1.917565,2.002627,2.091462,2.184238,2.281130,2.382319,2.487998,2.598364,2.713626,2.834001,2.959716,3.091007,3.228122,3.371320,3.520870,3.677054,3.840166,4.010514,4.188418,4.374214,4.568251,4.770897,4.982531,5.203553,5.434380,5.675446,5.927206,6.190133,6.464724,6.751496,7.050988,7.363766,7.690419,8.031562,8.387837,8.759917,9.148502,9.554325,9.978149,10.420775,10.883034,11.365800,11.869980,12.396526,12.946429,13.520726,14.120498,14.746875,15.401038,16.084220,16.797707,17.542844,18.321035,19.133746,19.982508,20.868921,21.794655,22.761454,23.771139,24.825614,25.926865,27.076966,28.278085,29.532486,30.842530,32.210688,33.639536,35.131768,36.690193,38.317750,40.017504,41.792659,43.646558,45.582695,47.604718,49.716437,51.921831,54.225055,56.630448,59.142544,61.766074,64.505983,67.367433,70.355815,73.476760,76.736149,80.140122,83.695094,87.407762,91.285122,95.334480,99.563464,103.980045,108.592542,113.409647,118.440437,123.694389,129.181404,134.911820,140.896434,147.146522,153.673860,160.490748,167.610029,175.045117,182.810022,190.919373,199.388451,208.233213,217.470323,227.117187,237.191980,247.713685 };
//for (uint8_t i = 0; i < 128; i++)
//{
//	printf("%d:%f,", i,pow(10,(0.377 / 20 * i)));
//}
typedef enum
{
    errorEnvp,
    errorEdet,
    errorAmpd,
    errorFtc,
    errorNfd,
} errorReturn;

typedef struct {
    float coeff[64];
    int16_t length;
} genAdvCoeffReturn;


typedef struct
{
    int16_t framelen;
    // envp
    int16_t envp_cfg;
    int16_t g_cal;
    int16_t envp_afc;
    int32_t fc;
    int16_t fmode;
    bool dsr_sel;
    bool fspi_sel;
    bool aspi_sel;
    // ftc
    bool ftc_en;
    int16_t mode;
    // ampd
    uint8_t g_dig;
    float stc_tb;
    uint8_t g_stc_target;
    uint8_t stc_mod;
    //edet
    float stg_th;
    float stg_tb;
    float stg_step;
    uint8_t atg_ini;
    uint8_t atg_tau;
    uint8_t atg_alpha;
    uint8_t aatg_alpha;
    float STG_TH;
    float STG_TB;
    float STG_STEP;
    uint8_t atg_cfg;
    int16_t eeval_dma_val;
    int16_t eeval_sens;
    int16_t eeval_sel;
    int16_t aatg_cw;
    int16_t aatg_cn;
    // nfd
    uint16_t NFD_CTRL;
    float nfd_th;
    float ring_time;
    int16_t measure_mod;
    int32_t ring_bin;
    int16_t nfd_sens;
    int16_t nfd_irq_cfg;
	//nfd2
	float nfd2_th;
	float NFD2_START;
	float NFD2_LENGTH;

    //fix or advanced path
    bool fix_path_process;
    int16_t npulse;
    int16_t bandPass;

    int16_t f1_coeff_sel;
    int16_t f2_coeff_sel;
    int16_t filt_len_code;
    int16_t filt_len_fix;
    genAdvCoeffReturn* COEFF;
} DspConfig;

typedef struct
{
    //in
    float ENVP_ENV_RAW;
    float ms_ts;
    float STG_TH;
    float STG_TB;
    float STG_STEP;
    float AMPD_ENV;
    float filt1;
    float filt2;
    float conf1;
    float conf2;
    float ATG_TH;
    //out
    float atg_th;
    float stg_th;
    float aatg1_th;
    float aatg2_th;
    float aatg_filt1;
    float aatg_filt2;
    float aatg_conf1;
    float aatg_conf2;
    float std_datapacket[3];//0 evt 1 dma_val 2 timestamp
    float adv_datapacket1[3];//0 conf 1 filt 2 timestamp  
    float adv_datapacket2[3];//0 conf 1 filt 2 timestamp
} EdetIO;


typedef struct
{
    //in
    float ENVP_ENV_RAW;
    float ms_ts;
    uint8_t burst_en;
    uint8_t rtm_rt_end;
    //out
    float NFD_TS1;
    float NFD_TS2;
    float NFD_TS3;
    float NFD_TS4;
    float NFD_TS5;
    float NFD_ECHO1;
    float NFD_ECHO2;
    float NFD_ECHO3;
    float NFD_ECHO4;
    float NFD_ECHO5;

} NfdIO;

typedef struct
{
    //in
    float ms_ts;
    float ENV_FTC;
    //out
    float AMPD_ENV;
} AmpdIO;

typedef struct
{
    // envp
    int16_t adc_eoc;
    int16_t delay;
    int16_t rtm_rt_end;
    int16_t afc_brk;
    int16_t burst_sta;//BRG give out 
    int16_t envp_restart;

    float env_i;
    float env_q;
    float ENVP_ENV_RAW;
    float envd_raw;
    float env_phi;
    float env_phid;

    // ftc
    float ENV_FTC;
    // edet
    EdetIO edet_io;
    int16_t framelen;
    int32_t samplerate;
    int32_t inputlen;
    float ms_ts;
    float* env_raw;
    //float STG_TH;
    //float STG_TB;
    //float STG_STEP;
    // ampd
    AmpdIO ampd_io;
    // nfd
    NfdIO nfd_io;
    int16_t burst_en;
    int16_t nfd_evt;

    float* NFD_ECHO;
    float* NFD_TS;

    //interrupt
    uint16_t DSP_IRQ_STATUS[1];

    int16_t frame_num;

    // Advanced path
    float filt1;
    float conf1;
    float filt2;
    float conf2;
} DspInputOutput;


#ifdef __cplusplus
}
#endif
#endif // _DSP_PUBLIC_H_
