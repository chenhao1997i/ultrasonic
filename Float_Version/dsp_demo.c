﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com
#include "include/dsp_algo_api.h"

genAdvCoeffReturn* generateAdvCoeff(int32_t fc, int16_t B, int16_t npulse, int16_t dsr_sel) {
	int16_t N2;
	genAdvCoeffReturn* result = (genAdvCoeffReturn*)calloc(1, sizeof(genAdvCoeffReturn));
	if (dsr_sel == 0)
		N2 = npulse;
	else
		N2 = (npulse - 1) / 2 + 1;
	float t_down[127];
	float freq_base[127];
	float upi[127];
	float upq[127];
	float coeff2[128];
	float tao = (float)npulse / fc;
	for (int16_t i = 0; i < (N2 - 1) / 2 + 1; ++i) {
		if (N2 == 1)
			t_down[i] = -tao / 2.0;
		else
			t_down[i] = i / (N2 - 1.0) * tao - tao / 2.0;
		freq_base[i] = B / tao * t_down[i];
		upi[i] = cos(M_PI * freq_base[i] * t_down[i]);
		upq[i] = sin(M_PI * freq_base[i] * t_down[i]);
	}

	int len_half = (N2 - 1) / 2 + 1;
	for (int16_t i = 0; i < 2 * len_half; ++i)
	{
		if (i < len_half)
			coeff2[i] = upi[i];
		else
			coeff2[i] = -upq[i- len_half];
	}
	for (int16_t i = 0; i < len_half; ++i)
	{
		result->coeff[i] = coeff2[2 * i];
	}
	result->length = len_half;
	return result;
}

int16_t dsp_init(int32_t sample_rate, DspParam* inst, DspConfig* config, DspInputOutput* input_output, int16_t* mod_switch)
{
	/*cfg for modules*/
	//envp
	config->fix_path_process = true;
	config->dsr_sel =1;
	config->envp_cfg = 7;
	config->fspi_sel = 0;
	config->g_cal = 1;
	config->fc = 58000;
	config->aspi_sel = 0;//advanced path input:0
	config->fmode = sample_rate / config->fc;
	config->npulse = 64;
	config->bandPass = 8000;
	config->f1_coeff_sel =0;
	config->f2_coeff_sel = 0;
	if (config->dsr_sel == 1)
	{
		config->filt_len_code = (config->npulse - 1) / 2;
	}
	else
	{
		config->filt_len_code = config->npulse - 1;
	}
	config->filt_len_fix = config->filt_len_code;
	config->COEFF = generateAdvCoeff(config->fc, config->bandPass, config->npulse, config->dsr_sel);

	//ampd
	config->g_dig = 10;//default 10
	config->g_stc_target = 98;//default 98
	config->stc_tb =20e-6;//0-4096 microsecond default 20 microsecond
	config->stc_mod = 1;//default 1


	//ftc
	config->ftc_en = 0;
	config->mode = 1;
	//edet adv
	config->aatg_cn = 10;// control CFAR cell number
	config->aatg_cw = 8;// control CFAR cell width
	config->aatg_alpha = 0;// control aatg threshold magnitude
	//edet std
	config->atg_ini = 5;//atg initial value
	config->atg_tau = 3;//atg time response configuration,affect the slope of threshold ;  config.atg_tau = 3;
	config->atg_alpha = 3;//control atg threshold magnitude
	config->atg_cfg = 1;//atg switch 1 for on ,0 for off
	config->eeval_sens = 3;//eeval sensitivity
	config->eeval_sel = 0;//eeval event select TLH_EVT 0,THL_EVT 1, MAX 2, MIN 3
	config->eeval_dma_val = 1;//1 for AMPD, 0 for ENVP
	//nfd
	config->NFD_CTRL = 3 + (1 << 3) + (0 << 5);//bit2:0 nfd_sens bit4:3 nfd_irq_cfg bit7:5 nfd_max_num
	config->nfd_th = 0.01;//nfd threshold
	config->measure_mod = 0; //0:direct measure 1:indirect measure
	config->ring_time = 0.002;
	config->ring_bin = (int32_t)sample_rate * config->ring_time;
	//NFD2
	config->NFD2_START = 0.00075;
	config->NFD2_LENGTH = 0.00175;
	config->nfd2_th =10000;
	//interrupt
	input_output->DSP_IRQ_STATUS[0] = 0;

	//envp output init
	input_output->envd_raw = 0;
	input_output->env_phi = 0;
	input_output->env_phid = 0;
	input_output->env_i = 0;
	input_output->env_q = 0;

	dsp_init_api(inst, sample_rate, config, input_output, mod_switch);

	return 0;
}

int16_t dsp_destroy(DspParam* inst, DspConfig* config, DspInputOutput* input_output)
{
	dsp_destroy_api(inst, config, input_output);
	free(input_output);
	free(inst);
	free(config);
	
	return 0;
}

int16_t dsp_algo_demo(float* input_data, float* ms_ts, int* mod_switch, int32_t data_len)
{
	////init
	DspParam* inst = (DspParam*)calloc(1, sizeof(DspParam));
	DspConfig* config = (DspConfig*)calloc(1, sizeof(DspConfig));
	config->COEFF = (genAdvCoeffReturn*)calloc(1, sizeof(genAdvCoeffReturn));
	DspInputOutput* input_output = (DspInputOutput*)calloc(1, sizeof(DspInputOutput));
	int32_t sample_rate;
	if (config->fix_path_process == true)
		sample_rate = 348000;//change with adc_data
	else
		sample_rate = 348000;//advanced path
	dsp_init(sample_rate, inst, config, input_output, mod_switch);
	//edet param 
	float STG_TH[ ] = { 128,64,32,32,32,32,32,32 };
	float STG_TB[ ] = { 2.38,2.38,2.38,2.38,2.38,2.38,2.38,2.38 };
	float STG_STEP[ ] = { -2,-2,-1,-1,-1,-1,-1,-1 };
	for (int i = 0; i < 8; i++)
		 STG_TB[i] = STG_TB[i] * 1e-4;


	//algo process
	do
	{
		float indata = input_data[input_output->frame_num];
		int16_t k;
		input_output->inputlen = data_len;
		input_output->ms_ts = ms_ts[input_output->frame_num];
		if (input_output->frame_num > input_output->delay * config->fmode * (config->dsr_sel + 1)) {
			if (input_output->ms_ts * 1e6 >= 1024)
				//inst->ptr_ampd.stc_tb = 2e-5;
				config->g_stc_target = 64;
			if (input_output->frame_num < input_output->inputlen - 1)
				k = floor(input_output->ms_ts / (ms_ts[input_output->inputlen - 1] / 8));
			else
				k = 7;
			input_output->edet_io.STG_TH = STG_TH[k];
			input_output->edet_io.STG_TB = STG_TB[k];
			input_output->edet_io.STG_STEP = STG_STEP[k];
		}
		//printf("ms_ts[%d]: %f\n", input_output->frame_num, input_output->ms_ts_int);
		input_output->envp_restart = 0;
		input_output->adc_eoc = 1;
		input_output->burst_sta = 0;
		input_output->afc_brk = 0;
		if (config->measure_mod)
			input_output->rtm_rt_end = 1;
		else
			if (input_output->frame_num < config->ring_bin)
				input_output->rtm_rt_end = 0;
			else
				input_output->rtm_rt_end = 1;
		input_output->burst_en = 1;

		dsp_process_api(inst, input_output, config, indata);

		input_output->frame_num++;
		if (data_len - input_output->frame_num <= 0)
			break;
	} while (1);
	free(config->COEFF->coeff);
	dsp_destroy(inst, config, input_output);

	return 0;
}


void dsp_envp_test()
{
	int mod_switch[] = {
		1 /* envp */,
		0 /* ampd */,
		0 /* edet */,
		0 /*nfd*/,
	};
	//const char* filename = "../data/adc_data2.bin";//for fix path
	//const char* filename = "../matlab_data/0602_1025_32k_fixed.bin";
	//const char* filename = "../0602_1005_chirp.bin";
	//const char* filename = "../data/adc_dta_adv2.bin";//for 127 pulses
	//const char* filename = "../data/envp_data/fs_348000Hz/2m_24pulse_fixed.bin";
	const char* filename = "../data/envp_data/fs_348000Hz/2m_64pulse_chirp.bin";
	//const char* filename = "../matlab_data/adc_dta_adv.bin";//for 64 pulses
	//const char* filename = "../data/adc_dta_127pulses.bin";//for 127 pulses
	FILE* fp = NULL;
	fp = fopen(filename, "rb");//loading interpolated envelop from SPM demo
	fseek(fp, 0L, SEEK_END);
	int32_t size = ftell(fp); 
	fseek(fp, 0L, SEEK_SET);//back to the start of bin
	float* input_data = (float*)malloc(size);
	int32_t data_len = size / sizeof(float);
	float* ms_ts = (float*)malloc(sizeof(float) * data_len);
	for (int32_t i = 0; i < data_len; ++i)
		ms_ts[i] = (float)i / 29000;
	fread(input_data, sizeof(float), data_len, fp);
	//for (int i = 0; i < data_len; ++i)
	//	printf("%d:%.30f\n",i+1,input_data[i]);
	dsp_algo_demo(input_data,  ms_ts, &mod_switch, data_len);
	fclose(fp);
	free(ms_ts);
}

void dsp_ampd_test()
{
	int mod_switch[] = {
		0 /* envp */,
		1 /* ampd */,
		0 /* edet */,
		0 /*nfd*/,
	};
	char* filename = "C:/Users/E0004885/Desktop/devtest/elmosdata2/2-ampd_env.bin";
	//char* filename = "C:/Users/E0004885/Documents/WXWorkLocal/1688849889266223_1970326128982512/Cache/File/2023-06/envp_ampd_modpara_1.bin";
	//char* filename = "C:/Users/E0004885/Desktop/devtest/elmosdata/1m_A_envp_env_raw_FTC_off-mode0-2.bin";
	FILE* fp = NULL;
	fp = fopen(filename, "rb");//loading interpolated envelop from SPM demo
	fseek(fp, 0, SEEK_END);
	int32_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);//back to the start of bin
	float* input_data = (float*)malloc(size);
	int32_t data_len = size / sizeof(float);
	fread(input_data, sizeof(float), data_len, fp);
	float* ms_ts = (float*)malloc(sizeof(float) * data_len);
	for (int32_t i = 0; i < data_len; ++i)
			ms_ts[i] = (float)i / 29000;
	dsp_algo_demo(input_data, ms_ts, &mod_switch, data_len);
	fclose(fp);
	free(ms_ts);
}

void dsp_edet_test()
{
	int mod_switch[] = {
		0 /* envp */,
		0 /* ampd */,
		1 /* edet */,
		0 /*nfd*/,
	};
	char* filename = "../data/edet_data/Mresult_envelop_in.bin";
	//char* filename = "../matlab_data/AMPD_ENV.bin";
	FILE* fp = NULL;
	fp = fopen(filename, "rb");//loading interpolated envelop from SPM demo
	fseek(fp, 0, SEEK_END);
	int32_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);//back to the start of bin
	float* input_data = (float*)malloc(size);
	int32_t data_len = size / sizeof(float);
	fread(input_data, sizeof(float), data_len, fp);
	//float* ms_ts = (float*)malloc(sizeof(float) * data_len);
	//for (int32_t i = 0; i < data_len; ++i)
	//	ms_ts[i] = (float)i / 58000;
	FILE* fp2 = NULL;
	fp2 = fopen("../data/edet_data/Mresult_msts.bin", "rb");//read binary file
	fseek(fp2, 0, SEEK_END);
	int32_t size2 = ftell(fp2);
	fseek(fp2, 0, SEEK_SET);//back to the start of bin
	float* ms_ts = (float*)malloc(size2);
	int32_t input_len2 = size2 / sizeof(float);
	fread(ms_ts, sizeof(float), input_len2, fp2);
	//for (int32_t i = 0; i < data_len; ++i)
	//	ms_ts[i] = ms_ts[i] / 58000;
	//for (int32_t i = 0; i < data_len; ++i)
	//	printf("cnt:%d,msts:%f\n", i, ms_ts[i]);
	dsp_algo_demo(input_data, ms_ts, &mod_switch, data_len);
	fclose(fp);
	free(ms_ts);
}

void dsp_nfd_test()
{
	int mod_switch[] = {
		0 /* envp */,
		0 /* ampd */,
		0 /* edet */,
		1 /*1:nfd    2:nfd2*/,
	};
	//const char* filename = "../matlab_data/ENVP_ENV_RAW.bin";
	char* filename = "..//nfd_data/envp_obstacle_20cm.bin";
	FILE* fp = NULL;
	fp = fopen(filename, "rb");//loading interpolated envelop from SPM demo
	fseek(fp, 0, SEEK_END);
	int32_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);//back to the start of bin
	double* input_data = (double*)malloc(size);
	int32_t data_len = size / sizeof(double);
	fread(input_data, sizeof(double), data_len, fp);
	float* ms_ts = (float*)malloc(sizeof(float) * data_len);
	float* float_input_data= (float*)malloc(sizeof(float) * data_len);
	for (int32_t i = 0; i < data_len; ++i)
	{
		ms_ts[i] = (float)i / 29000;
		float_input_data[i] = (float)input_data[i];
		//printf("%f\n", float_input_data[i]);
	}
	dsp_algo_demo(float_input_data, ms_ts, &mod_switch, data_len);
	free(ms_ts);
}

void dsp_demo_test()
{
	int mod_switch[] = {
		1 /* envp */,
		1 /* ampd */,
		1 /* edet */,
		0 /*1:nfd   2:nfd2*/,
	};
	//const char* filename = "../matlab_data/adc_data2.bin";
	//const char* filename = "../matlab_data/0602_1025_32k_fixed.bin";
	//const char* filename = "../matlab_data/0602_1005_chirp.bin";
	//const char* filename = "../matlab_data/adc_dta_adv2.bin";//for 127 pulses
	//const char* filename = "../matlab_data/adc_dta_127pulses.bin";//for 127 pulses
	//const char* filename = "../matlab_data/adc_dta_adv.bin";//for 64 pulses
	const char* filename = "../data/envp_data/fs_348000Hz/2m_8pulse_fixed.bin";
	//const char* filename = "../data/envp_data/fs_348000Hz/2m_64pulse_chirp.bin";
	FILE* fp = NULL;
	fp = fopen(filename, "rb");//loading interpolated envelop from SPM demo
	fseek(fp, 0, SEEK_END);
	int32_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);//back to the start of bin
	float* input_data = (float*)malloc(size);
	int32_t data_len = size / sizeof(float);
	fread(input_data, sizeof(float), data_len, fp);
	for (uint32_t i = 0; i < data_len; ++i)
		input_data[i] = input_data[i] * 8192;
	FILE* fp2 = NULL;
	fp2 = fopen("../data/envp_data/fs_348000Hz/2m_8pulse_fixed_ts.bin", "rb");//read binary file
	//fp2 = fopen("../data/envp_data/fs_348000Hz/2m_64pulse_chirp_ts.bin", "rb");
	fseek(fp2, 0, SEEK_END);
	int32_t size2 = ftell(fp2);
	fseek(fp2, 0, SEEK_SET);//back to the start of bin
	float* ms_ts = (float*)malloc(size2);
	int32_t input_len2 = size2 / sizeof(float);
	fread(ms_ts, sizeof(float), input_len2, fp2);
	dsp_algo_demo(input_data, ms_ts, &mod_switch, data_len);
	free(ms_ts);
}

int main()
{
	//dsp_envp_test();
    dsp_ampd_test(); 
	//dsp_edet_test();
	//dsp_nfd_test();
	//dsp_demo_test();
}

