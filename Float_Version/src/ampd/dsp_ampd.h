﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#ifndef _DSP_AMPD_H_
#define _DSP_AMPD_H_
#include "../../include/dsp_algo_api.h"
#ifdef __cplusplus
extern "C" {
#endif
	typedef struct
	{
		//input
		uint8_t g_dig;
		uint8_t g_stc_target;
		float stc_tb;
		uint8_t stc_mod;
		uint16_t step_cnt;
		float limit;
		float ls_ms_ts;
	}objAMPD;

	void ampd_init(objAMPD* ptr_ampd, DspConfig* dsp_cfg);
	int16_t ampd_process(objAMPD* ptr_ampd,  AmpdIO* input_output);
	void ampd_destroy(objAMPD* ptr_ampd);

#ifdef __cplusplus
}
#endif
#endif //_DSP_AMPD_H_