﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#include "dsp_ampd.h"

int16_t stc_control(objAMPD* ptr_ampd, AmpdIO* input_output) 
{
	if (ptr_ampd->stc_tb > 0)
	{
		if (ptr_ampd->g_dig < ptr_ampd->g_stc_target)
		{
			if ((floor(input_output->ms_ts / ptr_ampd->stc_tb)) > (floor(ptr_ampd->ls_ms_ts / ptr_ampd->stc_tb)))
			{
				ptr_ampd->g_dig += 1;
				if (ptr_ampd->stc_mod == 1)
				{
					ptr_ampd->step_cnt += 1;//increase step when tb is reached
					if (ptr_ampd->step_cnt == 16)
					{
						ptr_ampd->step_cnt = 0;
						ptr_ampd->stc_tb = ptr_ampd->stc_tb * 2;
					}

				}
			}
			//ptr_ampd->stc_gain = ptr_ampd->step_cnt * ptr_ampd->g_dig_step;
			//printf("%f:%f:%f:%f\n", floor(input_output->ms_ts / ptr_ampd->stc_tb), floor(ptr_ampd->ls_ms_ts / ptr_ampd->stc_tb), ptr_ampd->stc_gain, ptr_ampd->g_stc_target);
		}
	}
	ptr_ampd->ls_ms_ts = input_output->ms_ts;
	return 0;
}

void ampd_init(objAMPD* ptr_ampd, DspConfig* dsp_cfg)
{
	ptr_ampd->g_dig = dsp_cfg->g_dig;
	ptr_ampd->stc_tb = dsp_cfg->stc_tb;
	ptr_ampd->stc_mod = dsp_cfg->stc_mod;
	ptr_ampd->step_cnt = 0;
	ptr_ampd->ls_ms_ts = 0;
	ptr_ampd->limit = 256 - 1;
	ptr_ampd->g_stc_target = dsp_cfg->g_stc_target;
}

void ampd_destroy(objAMPD* ptr_ampd)
{
	free(ptr_ampd);
}

int16_t amplifier(objAMPD* ptr_ampd, AmpdIO* input_output) 
{
	//printf("%d:%f:%f:%f\n", ptr_ampd->step_cnt, ptr_ampd->g_dig , ptr_ampd->g_dig * ptr_ampd->step_cnt,input_output->ms_ts);
	//printf("%f\n",gain_array[ptr_ampd->g_dig]);
	//printf("%d\n", ptr_ampd->g_dig);
	float ENV_FTC_shift = input_output->ENV_FTC / 256;//shift 10 bits
	input_output->AMPD_ENV = ENV_FTC_shift * gain_array[ptr_ampd->g_dig];
	//printf("%f\n", input_output->AMPD_ENV);
	return 0;
}

int16_t limiter(objAMPD* ptr_ampd, AmpdIO* input_output) 
{
	if (input_output->AMPD_ENV > ptr_ampd->limit)
		input_output->AMPD_ENV = ptr_ampd->limit;
	//printf("%f\n", input_output->AMPD_ENV);
	return 0;
}

int16_t ampd_process(objAMPD* ptr_ampd, AmpdIO* input_output)
{
	stc_control(ptr_ampd, input_output);
	amplifier(ptr_ampd, input_output);
	limiter(ptr_ampd, input_output);
	return 0;
}
