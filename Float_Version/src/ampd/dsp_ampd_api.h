﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#ifndef _AMPD_API_H_
#define _AMPD_API_H_
#include "../../include/dsp_algo_api.h"

#ifdef __cplusplus 
extern "C" {
#endif
	void* ampd_init_api(DspConfig* dsp_cfg);
	errorReturn ampd_process_api(void* ptr, AmpdIO* input_output);
	void ampd_destroy_api(void* ptr);
#ifdef __cplusplus 
}
#endif
#endif 
