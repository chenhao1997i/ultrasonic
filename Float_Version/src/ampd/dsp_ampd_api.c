﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_ampd_api.h"
#include "dsp_ampd.h"

void* ampd_init_api(DspConfig* dsp_cfg)
{
	void* st = NULL;
	st = (void*)calloc(1, sizeof(objAMPD));
	ampd_init((objAMPD*) st, dsp_cfg);
	return st;
}

errorReturn ampd_process_api(void* ptr,  AmpdIO* input_output)
{
	int16_t ret = ampd_process((objAMPD*) ptr,  input_output);
	if (ret != 0)
		return errorAmpd;
}

void  ampd_destroy_api(void* ptr)
{
	ampd_destroy((objAMPD*)ptr);
}




