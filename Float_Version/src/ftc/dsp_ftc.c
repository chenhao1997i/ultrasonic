﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com
#include "dsp_ftc.h"
static const float kFilterCoeff5[5] = {-0.024058697346403132289, -0.076162982654427877072, 0.89579142938395051043, -0.076162982654427877072, -0.024058697346403132289 };

void* ftc_init(FtcParam* ftc, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	ftc->filterCoeff = (float*)calloc(5, sizeof(float));
	switch (dsp_cfg->mode)
	{
	case 1:
		ftc->filter_order = 5;
		ftc->cut_frequency = 5000;
		memcpy(ftc->filterCoeff, kFilterCoeff5, sizeof(float) * ftc->filter_order);
		//ftc->filterCoeff = kFilterCoeff5;
		break;
	case 2:
		ftc->filter_order = 5;
		ftc->cut_frequency = 5000;
		memcpy(ftc->filterCoeff, kFilterCoeff5, sizeof(float) * ftc->filter_order);
		break;
	case 3:
		ftc->filter_order = 5;
		ftc->cut_frequency = 5000;
		memcpy(ftc->filterCoeff, kFilterCoeff5, sizeof(float) * ftc->filter_order);
		break;
	case 4:
		ftc->filter_order = 5;
		ftc->cut_frequency = 5000;
		memcpy(ftc->filterCoeff, kFilterCoeff5, sizeof(float) * ftc->filter_order);
		break;
	case 5:
		ftc->filter_order = 5;
		ftc->cut_frequency = 5000;
		memcpy(ftc->filterCoeff, kFilterCoeff5, sizeof(float) * ftc->filter_order);
		break;
	case 6:
		ftc->filter_order = 5;
		ftc->cut_frequency = 5000;
		memcpy(ftc->filterCoeff, kFilterCoeff5, sizeof(float) * ftc->filter_order);
		break;
	case 7:
		ftc->filter_order = 5;
		ftc->cut_frequency = 5000;
		memcpy(ftc->filterCoeff, kFilterCoeff5, sizeof(float) * ftc->filter_order);
		break;
	default:
		printf("Unsupported %d\n", dsp_cfg->mode);
	}
	ftc->frame_num = 0;
	ftc->in_buffer = (float*)calloc(ftc->filter_order, sizeof(float));
	return ftc;
}

int16_t FIR_filter(FtcParam* ftc, DspInputOutput* input_output)
{
	for (int16_t i = 0; i < ftc->filter_order; ++i)
	{
		input_output->ENV_FTC = 0;
		int16_t idx = (i < ftc->filter_order) ? i : ftc->filter_order - 1;
		for (int16_t j = 0; j <= idx; ++j)
		{
			int16_t k = i - j;
			input_output->ENV_FTC += ftc->in_buffer[k] * ftc->filterCoeff[j];
		}
	}
	return 0;
}

int16_t ftc_process(FtcParam* ftc, DspInputOutput* input_output)
{
	for (uint16_t i = 0; i < ftc->filter_order - 1; i++)
		ftc->in_buffer[i] = ftc->in_buffer[i + 1];
	ftc->in_buffer[ftc->filter_order - 1] = input_output->ENVP_ENV_RAW;
	FIR_filter(ftc, input_output);
	printf("%f\n", input_output->ENV_FTC);
	ftc->frame_num++;
	return 0;
}

void ftc_destroy(FtcParam* ftc, DspInputOutput* input_output)
{
	free(ftc->filterCoeff);
	free(ftc->in_buffer);
	free(ftc);
}