﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#ifndef _FTC_API_H_
#define _FTC_API_H_
#include "../../include/dsp_algo_api.h"

#ifdef __cplusplus 
extern "C" {
#endif
	void* ftc_init_api(DspConfig* dsp_cfg, DspInputOutput* input_output);
	errorReturn ftc_process_api(void* ptr, DspInputOutput* input_output);
	void ftc_destroy_api(void* ptr, DspInputOutput* input_output);
#ifdef __cplusplus 
}
#endif
#endif 
