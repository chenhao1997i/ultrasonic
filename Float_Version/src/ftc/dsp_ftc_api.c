﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_ftc_api.h"
#include "dsp_ftc.h"

void* ftc_init_api(DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	void* st = NULL;
	st = (void*)calloc(1, sizeof(FtcParam));
	ftc_init((FtcParam*)st, dsp_cfg, input_output);
	return st;
}

errorReturn ftc_process_api(void* ptr, DspInputOutput* input_output)
{
	int16_t ret = ftc_process((FtcParam*)ptr, input_output);
	if (ret != 0)
		return errorFtc;
}

void ftc_destroy_api(void* ptr, DspInputOutput* input_output)
{
	ftc_destroy((FtcParam*)ptr, input_output);
}




