﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com
#ifndef _DSP_FTC_H_
#define _DSP_FTC_H_
#include "../../include/dsp_algo_api.h"
#ifdef __cplusplus
extern "C" {
#endif
typedef struct
{
	int16_t cut_frequency;
	int16_t filter_order;
	int32_t frame_num;
	float* filterCoeff;
	float* in_buffer;

}FtcParam;

	void* ftc_init(FtcParam* ftc, DspConfig* dsp_cfg, DspInputOutput* input_output);
	int16_t ftc_process(FtcParam* ftc, DspInputOutput* input_output);
	void ftc_destroy(FtcParam* ftc, DspInputOutput* input_output);

#ifdef __cplusplus
}
#endif
#endif //_DSP_FTC_H_