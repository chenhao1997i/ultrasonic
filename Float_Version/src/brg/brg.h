﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: sunzhongxiang@eswincomputing.com
#ifndef _BRG_HEADER_H_
#define _BRG_HEADER_H_
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#define _CRTDBG_MAP_ALLOC  //memory leak detecting
//#include<crtdbg.h>
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	int32_t f_clk;
	float T;
	int16_t burst_type;
	int16_t n_pulses;
	int32_t f_c;
	int32_t f_0;
	int32_t f_1;
	float f_delta;
	int16_t sel_inc;
	int16_t code_len;
	char* code;
}BrgConfig_t;

typedef struct {
	int32_t snap_num;
	char code_current;
	float freq_current;
	int32_t cnt_num;
	int32_t freq_target;
	int16_t burst_en;
	int16_t burst_brk;
	int16_t burst_sta;
}BrgRuntimeConfig_t;

typedef enum {
	OK_BRG_PROCESS,
	ERROR_BRG_PROCESS,
	BURST_BRK,
	BURST_SEND_OVER,
}brg_status_t;

typedef struct {
	uint16_t drv_en : 1;
	uint16_t drv1_en : 1;
	uint16_t drv2_en : 1;
}BrgData_t;

struct BRG_CFG{
	int16_t burst_type;
	int16_t n_pulses;
	int16_t sel_inc;
};
struct BRG_CODE{
	int16_t code_len;
	char* code;
};
struct BRG_FC{
	float f_c;
};
struct BRG_F0{
	float f_0;
};
struct BRG_F1{
	float f_1;
};
struct BRG_FDELTA{
	float f_delta;
};

typedef struct{
	float T;
	int32_t f_clk;
	int32_t len;
	int16_t send_over_flag;
	int32_t cnt;
	int32_t pulse_cnt;
	int16_t code_cnt;
	int32_t cnt_freq;
	int16_t* burst_en_list;
	int16_t* burst_brk_list;
	struct BRG_CFG BRG_CFG;
	struct BRG_CODE BRG_CODE;
	struct BRG_FC BRG_FC;
	struct BRG_F0 BRG_F0;
	struct BRG_F1 BRG_F1;
	struct BRG_FDELTA BRG_FDELTA;
}objBrg;

void* brg_init(BrgConfig_t* init_cfg);
brg_status_t brg_get_one_frame(void* instance, BrgRuntimeConfig_t* rtCfg, BrgData_t* output);
void brg_deinit(void* instance);
#ifdef __cplusplus
}
#endif
#endif 

