﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_envp_api.h"
#include "dsp_envp.h"

void* envp_init_api(int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	void* st = NULL;
	st = (void*)calloc(1, sizeof(ENVP_PARAM));
	envp_init((ENVP_PARAM*)st, sample_rate, dsp_cfg, input_output);
	return st;
}

errorReturn envp_process_api(void* ptr, float ADC_DTA, DspInputOutput* input_output)
{
	int16_t ret = envp_process((ENVP_PARAM*)ptr, ADC_DTA, input_output);
	if (ret != 0)
		return errorEnvp;
}

void  envp_destroy_api(void* ptr, DspInputOutput* input_output)
{
	envp_destroy((ENVP_PARAM*)ptr, input_output);
}




