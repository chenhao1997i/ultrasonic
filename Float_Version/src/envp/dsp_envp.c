﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_envp.h"

void filtercoeff_set(ENVP_PARAM* envp)
{
	envp->filter_len = 7;
	float filtCoeff[] = { -0.00365292121588408,0.0391190872232425,0.254101728604064,0.420864210777155,0.254101728604064,0.0391190872232425,-0.00365292121588408 };
	memcpy(envp->filtCoeff_tx, &filtCoeff, sizeof(float) * envp->filter_len);
	if (envp->envp_cfg == 0 || envp->envp_cfg == 1 || envp->envp_cfg == 2)
		memcpy(envp->filtCoeff_rx, &filtCoeff, sizeof(float) * envp->filter_len);
	if (envp->envp_cfg == 3)
	{
		envp->filter_len = 11;
		float filtCoeff[] = { 0.00264903473304637,0.0140887541333066,0.0541896450514841,0.123833046035864,0.193646647381850,0.223185745328900,0.193646647381850,0.123833046035864,0.0541896450514841,0.0140887541333066,0.00264903473304637 };
		memcpy(envp->filtCoeff_rx, &filtCoeff, sizeof(float) * envp->filter_len);
	}
	if (envp->envp_cfg == 4)
	{
		envp->filter_len = 17;
		float filtCoeff[] = { 0.00422590966966618,0.00777594915646621,0.0175831577467444,0.0346134665342125,0.0576361950046906,0.0832855757479131,0.106821781169177,0.123383280922885,0.129349368096491,0.123383280922885,0.106821781169177,0.0832855757479131,0.0576361950046906,0.0346134665342125,0.0175831577467444,0.00777594915646621,0.00422590966966618 };
		memcpy(envp->filtCoeff_rx, &filtCoeff, sizeof(float) * envp->filter_len);
	}
	if (envp->envp_cfg == 5)
	{
		envp->filter_len = 27;
		float filtCoeff[] = { 0.00361213643421483,0.00459497541607495,0.00704814063842317,0.0111194823304255,0.0168041228303139,0.0239333921571899,0.0321805071278121,0.0410829913073985,0.0500799945346610,0.0585609992453572,0.0659210888254095,0.0716171544212586,0.0752192275661009,0.0764515743307197,0.0752192275661009,0.0716171544212586,0.0659210888254095,0.0585609992453572,0.0500799945346610,0.0410829913073985,0.0321805071278121,0.0239333921571899,0.0168041228303139,0.0111194823304255,0.00704814063842317,0.00459497541607495,0.00361213643421483
		};
		memcpy(envp->filtCoeff_rx, &filtCoeff, sizeof(float) * envp->filter_len);
	}
	if (envp->envp_cfg == 6)
	{
		envp->filter_len = 39;
		float filtCoeff[] = { 0.00311492662250697,0.00345485274748638,0.00431344697545198,0.00570602758405049,0.00762831871876625,0.0100559359759989,0.0129446617161530,0.0162315150276224,0.0198365879181874,0.0236655867450636,0.0276129875377816,0.0315656870633365,0.0354070094288295,0.0390209117108305,0.0422962222942670,0.0451307427668126,0.0474350485137914,0.0491358344472756,0.0501786701277805,0.0505300521560149,0.0501786701277805,0.0491358344472756,0.0474350485137914,0.0451307427668126,0.0422962222942670,0.0390209117108305,0.0354070094288295,0.0315656870633365,0.0276129875377816,0.0236655867450636,0.0198365879181874,0.0162315150276224,0.0129446617161530,0.0100559359759989,0.00762831871876625,0.00570602758405049,0.00431344697545198,0.00345485274748638,0.00311492662250697 };
		memcpy(envp->filtCoeff_rx, &filtCoeff, sizeof(float) * envp->filter_len);
	}
	if (envp->envp_cfg == 7)
	{
		envp->filter_len = 65;
		float filtCoeff[] = { 0.00198076129926275,0.00205934137442509,0.00225025306445889,0.00255542226400952,0.00297542060640734,0.00350943591873184,0.00415526008904745,0.00490929470010784,0.00576657454586290,0.00672080890569403,0.00776444020982975,0.00888871949125386,0.0100837977879839,0.0113388324381592,0.0126421070021009,0.0139811633533808,0.0153429443077443,0.0167139450069800,0.0180803711467528,0.0194283020349014,0.0207438563912905,0.0220133587531579,0.0232235043317726,0.0243615201774699,0.0254153205506931,0.0263736544660688,0.0272262434738856,0.0279639078673449,0.0285786796529506,0.0290639007933625,0.0294143054246241,0.0296260849602291,0.0296969352201120,0.0296260849602291,0.0294143054246241,0.0290639007933625,0.0285786796529506,0.0279639078673449,0.0272262434738856,0.0263736544660688,0.0254153205506931,0.0243615201774699,0.0232235043317726,0.0220133587531579,0.0207438563912905,0.0194283020349014,0.0180803711467528,0.0167139450069800,0.0153429443077443,0.0139811633533808,0.0126421070021009,0.0113388324381592,0.0100837977879839,0.00888871949125386,0.00776444020982975,0.00672080890569403,0.00576657454586290,0.00490929470010784,0.00415526008904745,0.00350943591873184,0.00297542060640734,0.00255542226400952,0.00225025306445889,0.00205934137442509,0.00198076129926275 };
		memcpy(envp->filtCoeff_rx, &filtCoeff, sizeof(float) * envp->filter_len);
	}
}

void envp_restart(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	envp->cic_cnt = 0;
	envp->resam_cnt = 0;
	envp->env_raw_fm = 0;
	envp->env_phi_fm = 0;
	//IQ demodulate
	envp->yi = (float*)calloc(envp->fmode, sizeof(float));
	envp->yq = (float*)calloc(envp->fmode, sizeof(float));
	//low pass filter
	envp->filtCoeff_tx = (float*)calloc(7, sizeof(float));
	envp->filtCoeff_rx = (float*)calloc(envp->filter_maxlen, sizeof(float));
	envp->extract_factor = (float)1 / envp->fmode;
	//Fix frequency signal path process
	envp->yi_filt_in = (float*)calloc(envp->filter_maxlen, sizeof(float));
	envp->yq_filt_in = (float*)calloc(envp->filter_maxlen, sizeof(float));
	envp->yi_resam_in = (float*)calloc(envp->resam_filtlen, sizeof(float));
	envp->yq_resam_in = (float*)calloc(envp->resam_filtlen, sizeof(float));
	//complex conv  
	envp->buffer_abs_y_code = (float*)calloc(envp->N2, sizeof(float));
	envp->buffer_y_code_real = (float*)calloc(envp->N2, sizeof(float));
	envp->buffer_y_code_imag = (float*)calloc(envp->N2, sizeof(float));
	//Advanced path output
	if (envp->f1_coeff_sel == 1 || envp->f2_coeff_sel == 1)
	{
		envp->buffer_y_fix_real = (float*)calloc(envp->N1, sizeof(float));
		envp->buffer_y_fix_imag = (float*)calloc(envp->N1, sizeof(float));
		envp->buffer_abs_y_fix = (float*)calloc(envp->N1, sizeof(float));
	}
}

void envp_init(ENVP_PARAM* envp, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	envp->fc = dsp_cfg->fc;
	envp->fs = sample_rate;
	envp->fmode = dsp_cfg->fmode;
	//=========configuration parameter============
	//low pass filter
	envp->filtCoeff_tx = (float*)calloc(7, sizeof(float));
	envp->filtCoeff_rx = (float*)calloc(65, sizeof(float));
	envp->filter_len = 0;
	envp->filter_maxlen = 65;
	envp->extract_factor = (float)1 / envp->fmode;

	envp->f1_coeff_sel = dsp_cfg->f1_coeff_sel;
	envp->f2_coeff_sel = dsp_cfg->f2_coeff_sel;
	envp->N1 = dsp_cfg->filt_len_fix + 1;
	envp->N2 = dsp_cfg->filt_len_code + 1;
	envp->N1_inverse = (float)1 / envp->N1;
	envp->N2_inverse = (float)1 / envp->N2;

	//Fix frequency signal path process
	envp->envp_cfg = dsp_cfg->envp_cfg;
	envp->envp_afc = dsp_cfg->envp_afc;
	envp->dsr_sel = dsp_cfg->dsr_sel;
	envp->g_cal = dsp_cfg->g_cal;
	envp->delta_t = (envp->dsr_sel == 1 ? 2 / (float)envp->fc : 1 / (float)envp->fc);
	/*envp->cnt_len = envp->dsr_sel == 1 ? 2 * envp->fmode : envp->fmode;*/
	//init resample
	envp->resam_upfactor = (envp->dsr_sel == 0 ? 1 : 2);
	envp->resam_filtlen = 11;
	envp->resam_coefs = (float*)calloc(envp->resam_filtlen, sizeof(float));
	float coeff[] = { 0.0,	0.0, -7.12907708368861e-19, -0.0243719743773692, 1.07361130122176e-17, 0.275287077246544,	0.498169794261650, 0.275287077246544, 1.07361130122176e-17, -0.0243719743773692, -7.12907708368861e-19 };
	memcpy(envp->resam_coefs, &coeff, sizeof(float) * envp->resam_filtlen);
	input_output->delay = 3;

	// Advanced path process
	envp->fix_path_process = dsp_cfg->fix_path_process;
	envp->Ts = 1 / (float)envp->fs;
	if (envp->fix_path_process)
		envp->spi_sel = dsp_cfg->fspi_sel;
	else
		envp->spi_sel = dsp_cfg->aspi_sel;

	envp_restart(envp, input_output);

	filtercoeff_set(envp);

	// generateCoeff
	envp->coeffi = (float*)calloc(dsp_cfg->COEFF->length*2, sizeof(float));
	envp->coeffq = (float*)calloc(dsp_cfg->COEFF->length*2, sizeof(float));
	envp->bandPass = dsp_cfg->bandPass;
	float coeff_expand[128];
	int16_t k = 0;
	for (int16_t i = 0; i < dsp_cfg->COEFF->length; ++i)
	{
		coeff_expand[2 * i] = dsp_cfg->COEFF->coeff[i];
		coeff_expand[2 * i+1] = dsp_cfg->COEFF->coeff[i];

	}
	for (int16_t i = 0; i < dsp_cfg->COEFF->length * 2; ++i)
	{
		if (i < dsp_cfg->COEFF->length)
		{
			envp->coeffi[i] = coeff_expand[i];
			envp->coeffq[i] = coeff_expand[i+ dsp_cfg->COEFF->length];
		}	
		else
		{
			envp->coeffi[i] = coeff_expand[dsp_cfg->COEFF->length * 2 - i-1];
			envp->coeffq[i] = coeff_expand[dsp_cfg->COEFF->length * 3 - i - 1];
		}		
	}
}

int16_t iq_demod(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	if (envp->fmode == 4)
	{
		float cos[] = { 1,0,-1,0 };
		float sin[] = { 0,1,0,-1 };
		envp->yi[envp->cic_cnt] = 2.0 * envp->in_data * cos[envp->cic_cnt];
		envp->yq[envp->cic_cnt] = -2.0 * envp->in_data * sin[envp->cic_cnt];
	}
	if (envp->fmode == 6)
	{
		float cos[] = { 1,0.5,-0.5,-1,-0.5,0.5 };
		float sin[] = { 0,0.866,0.866,0,-0.866,-0.866 };
		envp->yi[envp->cic_cnt] = 2.0 * envp->in_data * cos[envp->cic_cnt];
		envp->yq[envp->cic_cnt] = -2.0 * envp->in_data * sin[envp->cic_cnt];
	}
	if (envp->fmode == 8)
	{
		float cos[] = { 1,sqrt(2) / 2,0,-sqrt(2) / 2,-1,-sqrt(2) / 2,0,sqrt(2) / 2 };
		float sin[] = { 0,sqrt(2) / 2,1,sqrt(2) / 2,0,-sqrt(2) / 2,-1,-sqrt(2) / 2 };
		envp->yi[envp->cic_cnt] = 2.0 * envp->in_data * cos[envp->cic_cnt];
		envp->yq[envp->cic_cnt] = -2.0 * envp->in_data * sin[envp->cic_cnt];
	}
	return 0;
}

float filter(float* input, int16_t filt_maxlen, ENVP_PARAM* envp, DspInputOutput* input_output)
{
	float output = 0.0f;
	int16_t idx = (filt_maxlen < envp->filter_len) ? filt_maxlen : envp->filter_len;
	if (envp->envp_afc == 1) // 1:hardware control filter
	{
		if (input_output->burst_sta == 2)
		{
			printf("burst aborted due to VTANK overvoltage!");
			exit(-1);
		}
		if (input_output->burst_sta == 1 || input_output->rtm_rt_end == 0)//input_output->burst_sta
		{
			for (int16_t j = 0; j < idx; ++j)
			{
				int16_t k = filt_maxlen - 1 - j;
				output += input[k] * envp->filtCoeff_tx[j];
			}
		}
		else
			for (int16_t j = 0; j < idx; ++j)
			{
				int16_t k = filt_maxlen - 1 - j;
				output += input[k] * envp->filtCoeff_rx[j];
			}
		if (input_output->afc_brk == 1)
			envp->envp_afc = 0;
	}
	else if (envp->envp_afc == 0)
		for (int16_t j = 0; j < idx; ++j)
		{
			int16_t k = filt_maxlen - 1 - j;
			output += input[k] * envp->filtCoeff_rx[j];
		}
	return output;
}

float resample(ENVP_PARAM* envp, DspInputOutput* input_output, float* indata)
{
	float outdata = 0.0f;
	for (int16_t j = 0; j < envp->resam_filtlen; ++j)
	{
		float acc = 0;
		int16_t k = envp->resam_filtlen - 1 - j;
		acc = indata[k] * envp->resam_coefs[j];
		outdata += acc;
		if (envp->resam_cnt <= input_output->delay * 2 - 1)
			outdata = 0;
	}
	return outdata;
}

int16_t get_result_fix(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	input_output->ENVP_ENV_RAW = sqrt(input_output->env_i * input_output->env_i + input_output->env_q * input_output->env_q);  //fix frequency signal path process
	if (input_output->env_i == 0)
		input_output->env_i = 1e-200;
	input_output->env_phi = atan2(input_output->env_q, input_output->env_i);
	int  x = (envp->dsr_sel == 1 ? envp->fmode *2  : envp->fmode);
	//printf("raw%d: %.10f\n", (input_output->frame_num- envp->fmode+1)/x+1,input_output->ENVP_ENV_RAW);
	input_output->envd_raw = (input_output->ENVP_ENV_RAW - envp->env_raw_fm) / envp->delta_t;
	input_output->env_phid = (input_output->env_phi - envp->env_phi_fm) / envp->delta_t;
	envp->env_raw_fm = input_output->ENVP_ENV_RAW;
	envp->env_phi_fm = input_output->env_phi;
	
	return 0;
}

float cic_simulat(int16_t R, float factor,  float* xn)
{
	float  IntegratorIn = 0;
	float  IntegratorBuffer = 0.0;
	for (int16_t i = 0; i < R; i++)
	{
		IntegratorIn = xn[i];
		IntegratorBuffer = IntegratorIn + IntegratorBuffer;
	}
	float y = factor * IntegratorBuffer;
	return y;
}

int16_t get_result_adv(ENVP_PARAM* envp, DspInputOutput* input_output)
{
		for (int16_t i = 1; i < envp->N2; ++i)
		{
			envp->buffer_y_code_real[i - 1] = envp->buffer_y_code_real[i];
			envp->buffer_y_code_imag[i - 1] = envp->buffer_y_code_imag[i];
			envp->buffer_abs_y_code[i - 1] = envp->buffer_abs_y_code[i];
		}
		//printf("%f\n", input_output->env_i);
		envp->buffer_y_code_real[envp->N2 - 1] = input_output->env_i;
		envp->buffer_y_code_imag[envp->N2 - 1] = input_output->env_q;
		envp->buffer_abs_y_code[envp->N2 - 1] = sqrt(input_output->env_i * input_output->env_i + input_output->env_q * input_output->env_q);
		if (envp->f1_coeff_sel == 0 && envp->f2_coeff_sel == 0)
		{
			float filt1_conv_real = 0; float filt1_conv_imag = 0;
			float conf1_conv_real = 0; float conf1_conv_imag = 0;
			float filt2_conv_real = 0; float filt2_conv_imag = 0;
			float conf2_conv_real = 0; float conf2_conv_imag = 0;
			float crl; float cig;
			float yrl; float yig;
			float crl_mul_yrl;
			float cig_mul_yig;
			float cig_mul_yrl;
			float crl_mul_yig;

			for (int16_t m = 0; m < envp->N2; ++m)
			{
				crl = envp->coeffi[m];
				cig = envp->coeffq[m];
				yrl = envp->buffer_y_code_real[m];
				yig = envp->buffer_y_code_imag[m];
				crl_mul_yrl = crl * yrl;
				cig_mul_yig = cig * yig;
				cig_mul_yrl = cig * yrl;
				crl_mul_yig = crl * yig;
				filt1_conv_real = filt1_conv_real + crl_mul_yrl - cig_mul_yig;
				filt1_conv_imag = filt1_conv_imag + cig_mul_yrl + crl_mul_yig;
				//filt2
				filt2_conv_real = filt2_conv_real + crl_mul_yrl + cig_mul_yig;
				filt2_conv_imag = filt2_conv_imag + crl_mul_yig - cig_mul_yrl;
				//confidence
				if (envp->buffer_abs_y_code[m] > 1e-5)
				{
					conf1_conv_real = conf1_conv_real + (crl_mul_yrl - cig_mul_yig) / envp->buffer_abs_y_code[m];
					conf1_conv_imag = conf1_conv_imag + (cig_mul_yrl + crl_mul_yig) / envp->buffer_abs_y_code[m];

					conf2_conv_real = conf2_conv_real + (crl_mul_yrl + cig_mul_yig) / envp->buffer_abs_y_code[m];
					conf2_conv_imag = conf2_conv_imag + (cig_mul_yrl - crl_mul_yig) / envp->buffer_abs_y_code[m];
				}
			}
			input_output->filt1 = sqrt(filt1_conv_real * filt1_conv_real + filt1_conv_imag * filt1_conv_imag) * envp->N2_inverse;
			input_output->filt2 = sqrt(filt2_conv_real * filt2_conv_real + filt2_conv_imag * filt2_conv_imag) * envp->N2_inverse;
			input_output->conf1 = sqrt(conf1_conv_real * conf1_conv_real + conf1_conv_imag * conf1_conv_imag) * envp->N2_inverse;
			input_output->conf2 = sqrt(conf2_conv_real * conf2_conv_real + conf2_conv_imag * conf2_conv_imag) * envp->N2_inverse;
		}
		if (envp->f1_coeff_sel == 0 && envp->f2_coeff_sel == 1)
		{
			//filter1
			float filt1_conv_real = 0; float filt1_conv_imag = 0;
			float conf1_conv_real = 0; float conf1_conv_imag = 0;
			float crl; float cig;
			float yrl; float yig;
			float crl_mul_yrl;
			float cig_mul_yig;
			float cig_mul_yrl;
			float crl_mul_yig;

			for (int16_t m = 0; m < envp->N2; ++m)
			{
				crl = envp->coeffi[m];
				cig = envp->coeffq[m];
				yrl = envp->buffer_y_code_real[m];
				yig = envp->buffer_y_code_imag[m];
				crl_mul_yrl = crl * yrl;
				cig_mul_yig = cig * yig;
				cig_mul_yrl = cig * yrl;
				crl_mul_yig = crl * yig;
				filt1_conv_real = filt1_conv_real + crl_mul_yrl - cig_mul_yig;
				filt1_conv_imag = filt1_conv_imag + cig_mul_yrl + crl_mul_yig;

				//confidence
				if (envp->buffer_abs_y_code[m] > 1e-5)
				{
					conf1_conv_real = conf1_conv_real + (crl_mul_yrl - cig_mul_yig) / envp->buffer_abs_y_code[m];
					conf1_conv_imag = conf1_conv_imag + (cig_mul_yrl + crl_mul_yig) / envp->buffer_abs_y_code[m];

				}
			}
			input_output->filt1 = sqrt(filt1_conv_real * filt1_conv_real + filt1_conv_imag * filt1_conv_imag) * envp->N2_inverse;
			input_output->conf1 = sqrt(conf1_conv_real * conf1_conv_real + conf1_conv_imag * conf1_conv_imag) * envp->N2_inverse;

			//filter2
			for (int16_t i = 1; i < envp->N1; ++i)
			{
				envp->buffer_y_fix_real[i - 1] = envp->buffer_y_fix_real[i];
				envp->buffer_y_fix_imag[i - 1] = envp->buffer_y_fix_imag[i];
				envp->buffer_abs_y_fix[i - 1] = envp->buffer_abs_y_fix[i];
			}
			envp->buffer_y_fix_real[envp->N1 - 1] = input_output->env_i;
			envp->buffer_y_fix_imag[envp->N1 - 1] = input_output->env_q;
			envp->buffer_abs_y_fix[envp->N1 - 1] = sqrt(input_output->env_i * input_output->env_i + input_output->env_q * input_output->env_q);

			float sum_buffer_y_fix_real = 0;
			float sum_buffer_y_fix_imag = 0;
			for (int i = 0; i < envp->N1; ++i)
			{
				sum_buffer_y_fix_real += envp->buffer_y_fix_real[i];
				sum_buffer_y_fix_imag += envp->buffer_y_fix_imag[i];
			}

			input_output->filt2 = sqrt(sum_buffer_y_fix_real * sum_buffer_y_fix_real + sum_buffer_y_fix_imag * sum_buffer_y_fix_imag) / envp->N1;
			float conf2_conv_real_fix = 0;
			float conf2_conv_imag_fix = 0;
			for (int16_t m = 0; m < envp->N2; ++m)
			{
				if (envp->buffer_abs_y_fix[m] > 1e-5)
				{
					conf2_conv_real_fix = conf2_conv_real_fix + envp->buffer_y_fix_real[m] / envp->buffer_abs_y_fix[m];
					conf2_conv_imag_fix = conf2_conv_imag_fix + envp->buffer_y_fix_imag[m] / envp->buffer_abs_y_fix[m];
				}
			}
			input_output->conf2 = sqrt(conf2_conv_real_fix * conf2_conv_real_fix + conf2_conv_imag_fix * conf2_conv_imag_fix) * envp->N1_inverse;
		}
		if (envp->f1_coeff_sel == 1 && envp->f2_coeff_sel == 0)
		{
			//filter1
			for (int16_t i = 1; i < envp->N1; ++i)
			{
				envp->buffer_y_fix_real[i - 1] = envp->buffer_y_fix_real[i];
				envp->buffer_y_fix_imag[i - 1] = envp->buffer_y_fix_imag[i];
				envp->buffer_abs_y_fix[i - 1] = envp->buffer_abs_y_fix[i];
			}
			envp->buffer_y_fix_real[envp->N1 - 1] = input_output->env_i;
			envp->buffer_y_fix_imag[envp->N1 - 1] = input_output->env_q;
			envp->buffer_abs_y_fix[envp->N1 - 1] = sqrt(input_output->env_i * input_output->env_i + input_output->env_q * input_output->env_q);
			//printf("%d:%.10f\n", (input_output->frame_num - 5) / 6 + 1, input_output->env_i);
			float sum_buffer_y_fix_real=0;
			float sum_buffer_y_fix_imag=0;
			for (int i = 0; i < envp->N1; ++i)
			{
				sum_buffer_y_fix_real += envp->buffer_y_fix_real[i];
				sum_buffer_y_fix_imag += envp->buffer_y_fix_imag[i];
			}

			input_output->filt1 = sqrt(sum_buffer_y_fix_real * sum_buffer_y_fix_real + sum_buffer_y_fix_imag * sum_buffer_y_fix_imag) * envp->N1_inverse;
			float conf1_conv_real_fix = 0;
			float conf1_conv_imag_fix = 0;
			for (int16_t m = 0; m < envp->N1; ++m)
			{
				if (envp->buffer_abs_y_fix[m] > 1e-5)
				{
					conf1_conv_real_fix = conf1_conv_real_fix + envp->buffer_y_fix_real[m] / envp->buffer_abs_y_fix[m];
					conf1_conv_imag_fix = conf1_conv_imag_fix + envp->buffer_y_fix_imag[m] / envp->buffer_abs_y_fix[m];
				}
			}
			input_output->conf1 = sqrt(conf1_conv_real_fix * conf1_conv_real_fix + conf1_conv_imag_fix * conf1_conv_imag_fix) * envp->N1_inverse;

			//filter2
			float filt2_conv_real_code = 0; float filt2_conv_imag_code = 0;
			float conf2_conv_real_code = 0; float conf2_conv_imag_code = 0;
			float crl; float cig;
			float yrl; float yig;
			float crl_mul_yrl;
			float cig_mul_yig;
			float cig_mul_yrl;
			float crl_mul_yig;

			for (int16_t m = 0; m < envp->N2; ++m)
			{
				crl = envp->coeffi[m];
				cig = envp->coeffq[m];
				yrl = envp->buffer_y_code_real[m];
				yig = envp->buffer_y_code_imag[m];
				crl_mul_yrl = crl * yrl;
				cig_mul_yig = cig * yig;
				cig_mul_yrl = cig * yrl;
				crl_mul_yig = crl * yig;
				filt2_conv_real_code = filt2_conv_real_code + crl_mul_yrl + cig_mul_yig;
				filt2_conv_imag_code = filt2_conv_imag_code + cig_mul_yrl - crl_mul_yig;

				//confidence
				if (envp->buffer_abs_y_code[m] > 1e-5)
				{
					conf2_conv_real_code = conf2_conv_real_code + (crl_mul_yrl + cig_mul_yig) / envp->buffer_abs_y_code[m];
					conf2_conv_imag_code = conf2_conv_imag_code + (cig_mul_yrl - crl_mul_yig) / envp->buffer_abs_y_code[m];

				}
			}
			input_output->filt2 = sqrt(filt2_conv_real_code * filt2_conv_real_code + filt2_conv_imag_code * filt2_conv_imag_code) * envp->N2_inverse;
			input_output->conf2 = sqrt(conf2_conv_real_code * conf2_conv_real_code + conf2_conv_imag_code * conf2_conv_imag_code) * envp->N2_inverse;
		}
		if (envp->f1_coeff_sel == 1 && envp->f2_coeff_sel == 1)
		{
			//filter1
			for (int16_t i = 1; i < envp->N1; ++i)
			{
				envp->buffer_y_fix_real[i - 1] = envp->buffer_y_fix_real[i];
				envp->buffer_y_fix_imag[i - 1] = envp->buffer_y_fix_imag[i];
				envp->buffer_abs_y_fix[i - 1] = envp->buffer_abs_y_fix[i];
			}
			envp->buffer_y_fix_real[envp->N1 - 1] = input_output->env_i;
			envp->buffer_y_fix_imag[envp->N1 - 1] = input_output->env_q;
			envp->buffer_abs_y_fix[envp->N1 - 1] = sqrt(input_output->env_i * input_output->env_i + input_output->env_q * input_output->env_q);

			float sum_buffer_y_fix_real = 0;
			float sum_buffer_y_fix_imag = 0;
			for (int i = 0; i < envp->N1; ++i)
			{
				sum_buffer_y_fix_real += envp->buffer_y_fix_real[i];
				sum_buffer_y_fix_imag += envp->buffer_y_fix_imag[i];
			}

			input_output->filt1 = sqrt(sum_buffer_y_fix_real * sum_buffer_y_fix_real + sum_buffer_y_fix_imag * sum_buffer_y_fix_imag) * envp->N1_inverse;
			float conf1_conv_real_fix = 0;
			float conf1_conv_imag_fix = 0;
			for (int16_t m = 0; m < envp->N1; ++m)
			{
				if (envp->buffer_abs_y_fix[m] > 1e-5)
				{
					conf1_conv_real_fix = conf1_conv_real_fix + envp->buffer_y_fix_real[m] / envp->buffer_abs_y_fix[m];
					conf1_conv_imag_fix = conf1_conv_imag_fix + envp->buffer_y_fix_imag[m] / envp->buffer_abs_y_fix[m];
				}
			}
			input_output->conf1 = sqrt(conf1_conv_real_fix * conf1_conv_real_fix + conf1_conv_imag_fix * conf1_conv_imag_fix) * envp->N1_inverse;

			//filter2
			float sum_buffer_y_code_real = 0;
			float sum_buffer_y_code_imag = 0;
			for (int i = 0; i < envp->N2; ++i)
			{
				sum_buffer_y_code_real += envp->buffer_y_code_real[i];
				sum_buffer_y_code_imag += envp->buffer_y_code_imag[i];
			}

			input_output->filt2 = sqrt(sum_buffer_y_code_real * sum_buffer_y_code_real + sum_buffer_y_code_imag * sum_buffer_y_code_imag) * envp->N2_inverse;
			float conf2_conv_real_code = 0;
			float conf2_conv_imag_code = 0;
			for (int16_t m = 0; m < envp->N2; ++m)
			{
				if (envp->buffer_abs_y_code[m] > 1e-5)
				{
					conf2_conv_real_code = conf2_conv_real_code + envp->buffer_y_code_real[m] / envp->buffer_abs_y_code[m];
					conf2_conv_imag_code = conf2_conv_imag_code + envp->buffer_y_code_imag[m] / envp->buffer_abs_y_code[m];
				}
			}
			input_output->conf2 = sqrt(conf2_conv_real_code * conf2_conv_real_code + conf2_conv_imag_code * conf2_conv_imag_code) * envp->N2_inverse;
		}
		//int  x = (envp->dsr_sel == 1 ? envp->fmode*2  : envp->fmode);
		//printf("filt1[%d]:%.10f\n", (input_output->frame_num-envp->fmode+1)/x+1,input_output->filt1);
		//printf("conf1[%d]:%.10f\n", (input_output->frame_num - envp->fmode+1) / x + 1, input_output->conf1);
		//printf("filt2[%d]:%.10f\n",  (input_output->frame_num- envp->fmode+1)/x+1, input_output->filt2);
		//printf("conf2[%d]:%.10f\n", (input_output->frame_num - envp->fmode+1) / x + 1, input_output->conf2);
	return 0;
}

int16_t envp_process(ENVP_PARAM* envp, float ADC_DTA, DspInputOutput* input_output)
{
	if (input_output->envp_restart == 1)
		envp_restart(envp, input_output);
	if (input_output->adc_eoc == 1)
	{
		//input_data
		envp->in_data = ADC_DTA;
		//IQ demode
		iq_demod(envp, input_output);
		envp->cic_cnt++;

		if (envp->cic_cnt == envp->fmode) {
			//CIC samplate
			envp->yi_cic = cic_simulat(envp->fmode, envp->extract_factor, envp->yi);
			envp->yq_cic = cic_simulat(envp->fmode, envp->extract_factor, envp->yq);
			for (int16_t i = 0; i < envp->filter_maxlen - 1; i++) {
				envp->yi_filt_in[i] = envp->yi_filt_in[i + 1];
				envp->yq_filt_in[i] = envp->yq_filt_in[i + 1];
			}
			envp->yi_filt_in[envp->filter_maxlen - 1] = envp->yi_cic;
			envp->yq_filt_in[envp->filter_maxlen - 1] = envp->yq_cic;
			envp->cic_cnt = 0;
			//FIR low-pass filter
			if (envp->spi_sel == 1)
			{
				envp->yi_filt_out = filter(envp->yi_filt_in, envp->filter_maxlen, envp, input_output);
				envp->yq_filt_out = filter(envp->yq_filt_in, envp->filter_maxlen, envp, input_output);//success
			}
			else
			{
				envp->yi_filt_out = envp->yi_filt_in[envp->filter_maxlen - 1];
				envp->yq_filt_out = envp->yq_filt_in[envp->filter_maxlen - 1];
			}
			//input for resample
			for (int16_t i = 0; i < envp->resam_filtlen - 1; i++) {
				envp->yi_resam_in[i] = envp->yi_resam_in[i + 1];
				envp->yq_resam_in[i] = envp->yq_resam_in[i + 1];
			}
			envp->yi_resam_in[envp->resam_filtlen - 1] = envp->yi_filt_out;
			envp->yq_resam_in[envp->resam_filtlen - 1] = envp->yq_filt_out;
			//rsample
			if (envp->resam_cnt % envp->resam_upfactor == 0) {
				if (envp->dsr_sel == 1)
				{
					envp->yi_resam_out = resample(envp, input_output, envp->yi_resam_in);
					envp->yq_resam_out = resample(envp, input_output, envp->yq_resam_in);
				}
				else
				{
					envp->yi_resam_out = envp->yi_resam_in[envp->resam_filtlen - 1];
					envp->yq_resam_out = envp->yq_resam_in[envp->resam_filtlen - 1];
				}
				//get result
				//gain calibration 
				input_output->env_i = envp->yi_resam_out * gain_array[envp->g_cal];
				input_output->env_q = envp->yq_resam_out * gain_array[envp->g_cal];
				if (envp->fix_path_process == true)
					get_result_fix(envp, input_output);
				else
					get_result_adv(envp, input_output);
				//compensate delay
				if (envp->spi_sel == 1)
					input_output->ms_ts = input_output->ms_ts - (float)(envp->filter_len - 1) / 2 / envp->fc * 1000000;//ms_ts(us)
			}

			if (envp->cic_cnt == 0)
				envp->resam_cnt = envp->resam_cnt + 1;
		}
	}
	return 0;
}

void envp_destroy(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	free(envp->yi);
	free(envp->yq);
	free(envp->filtCoeff_tx);
	free(envp->filtCoeff_rx);
	free(envp->buffer_abs_y_code);
	free(envp->buffer_y_code_real);
	free(envp->buffer_y_code_imag);
	if (envp->f1_coeff_sel == 1 || envp->f2_coeff_sel == 1)
	{
		free(envp->buffer_y_fix_real);
		free(envp->buffer_y_fix_imag);
		free(envp->buffer_abs_y_fix);
	}
	free(envp->coeffi);
	free(envp->coeffq);
	free(envp);
}
