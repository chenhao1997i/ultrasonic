﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com

#ifndef _DSP_ENVP_H_
#define _DSP_ENVP_H_
#pragma once 
#include "../../include/dsp_algo_api.h"
#define M_PI 3.14159265358979323846
#ifdef __cplusplus
extern "C" {
#endif
typedef struct
{
	int16_t cic_cnt;  //u3_t
	int16_t resam_cnt;  //u3_t
	int32_t fc;
	int32_t fs;
	int32_t fmode;
	//=========configuration parameter============
	float in_data;
	//IQ demodulate
	float* yi;
	float* yq;
	//low pass filter
	float* filtCoeff_tx;
	float* filtCoeff_rx;
	float* resam_coefs;
	int16_t filter_len;
	int16_t filter_maxlen;
	int16_t resam_filtlen;
	int16_t resam_upfactor;
	float extract_factor;
	int16_t extract_len;
	float env_raw_fm;
	float env_phi_fm;
	//Fix frequency signal path process
	int16_t envp_cfg;
	int16_t envp_afc; // Enable auto bandwidth during burst
	int16_t dsr_sel;
	int16_t spi_sel;
	int16_t g_cal;
	int16_t envp_restart;// Restart envelope processor (set input multiplier state-counter to zero)
	float delta_t;

	// advanced path
	bool fix_path_process;
	int16_t npulse;
	float Ts;
	int16_t N1;
	int16_t N2;
	int16_t Nmax;
	int16_t ncoeffs;
	int16_t bandPass;
	int16_t down_ratio;
	int16_t filt_len_code;
	int16_t filt_len_fix;
	int16_t f1_coeff_sel;
	int16_t f2_coeff_sel;
	float* buffer_abs_y_code;
	float* buffer_y_code_real;
	float* buffer_y_code_imag;
	float* buffer_y_fix_real;
	float* buffer_y_fix_imag;
	float* buffer_abs_y_fix;
	float sum_buffer_y_fix_real;
	float sum_buffer_y_fix_imag;
	float sum_real_buffer_y2;
	float sum_imag_buffer_y2;
	float* abs_y;
	float yi_cic;
	float yq_cic;
	float* yi_filt_in;
	float* yq_filt_in;
	float yi_filt_out;
	float yq_filt_out;
	float* yi_resam_in;
	float* yq_resam_in;
	float yi_resam_out;
	float yq_resam_out;

	float* coeffi;
	float* coeffq;
	float N1_inverse;
	float N2_inverse;
} ENVP_PARAM;

void envp_init(ENVP_PARAM* envp, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output);
int16_t envp_process (ENVP_PARAM* envp, float ADC_DTA, DspInputOutput* input_output);
void envp_destroy(ENVP_PARAM* envp, DspInputOutput* input_output);
#ifdef __cplusplus
}
#endif
#endif // _DSP_ENVP_H_