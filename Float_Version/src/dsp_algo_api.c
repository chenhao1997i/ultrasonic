﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com
#include "../include/dsp_algo_api.h"
#include "../src/envp/dsp_envp_api.h"
#include "../src/edet/dsp_edet_api.h"
#include "../src/ampd/dsp_ampd_api.h"
#include "../src/nfd/dsp_nfd_api.h"
#include "../src/ftc/dsp_ftc_api.h"

void dsp_init_api(DspParam* inst, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output, int* mod_switch)
{
	inst->ptr_envp = NULL;
	inst->ptr_edet = NULL;
	inst->ptr_ampd = NULL;
	inst->ptr_ftc = NULL;
	inst->ptr_nfd = NULL;
	inst->ptr_nfd2 = NULL;
	inst->envp_key = mod_switch[0];
	inst->ampd_key = mod_switch[1];
	inst->edet_key = mod_switch[2];
	inst->nfd_key = mod_switch[3];
	if (dsp_cfg->fix_path_process)
	{
		if (inst->envp_key == 1)
			inst->ptr_envp = envp_init_api(sample_rate, dsp_cfg, input_output);
		if (dsp_cfg->ftc_en)
			inst->ptr_ftc = ftc_init_api(dsp_cfg, input_output);
		if (inst->ampd_key == 1)
			inst->ptr_ampd = ampd_init_api(dsp_cfg);
		if (inst->edet_key == 1)
			inst->ptr_edet = edet_init_api(dsp_cfg);
		if (inst->nfd_key == 1)
			inst->ptr_nfd = nfd_init_api(dsp_cfg);
		if (inst->nfd_key == 2)
			inst->ptr_nfd2 = nfd2_init_api(dsp_cfg, input_output);
	}
	else
	{
		if (inst->envp_key == 1)
			inst->ptr_envp = envp_init_api(sample_rate, dsp_cfg, input_output);
		if (inst->edet_key == 1)
			inst->ptr_edet = edet_init_api(dsp_cfg);
	}
}

int16_t dsp_process_api(DspParam *inst, DspInputOutput* input_output, DspConfig* dsp_cfg, float indata) 
{
	if (dsp_cfg->fix_path_process)
	{
		//envp
		if (inst->envp_key) 
		{
			envp_process_api(inst->ptr_envp, indata, input_output);
			if (input_output->frame_num > input_output->delay * dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1) && input_output->frame_num % (dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1)) == 0)
			{
				//ftc
				if (dsp_cfg->ftc_en)
					ftc_process_api(inst->ptr_ftc, input_output);
				else
					input_output->ENV_FTC = input_output->ENVP_ENV_RAW;

				if (inst->ampd_key == 1)
				{
					input_output->ampd_io.ms_ts= input_output->ms_ts;
					input_output->ampd_io.ENV_FTC = input_output->ENV_FTC;
					ampd_process_api(inst->ptr_ampd, &input_output->ampd_io);
				}
				else
					input_output->ampd_io.AMPD_ENV = input_output->ENV_FTC;

				if (inst->edet_key == 1)
				{
					input_output->edet_io.AMPD_ENV = input_output->ampd_io.AMPD_ENV;
					input_output->edet_io.ENVP_ENV_RAW = input_output->ENVP_ENV_RAW;
					input_output->edet_io.ms_ts = input_output->ms_ts;
					//printf("ms_ts: %f\n", input_output->ENVP_ENV_RAW_point);
					edet_process_api(inst->ptr_edet, &input_output->edet_io,input_output->DSP_IRQ_STATUS);
				}

				if (inst->nfd_key == 1)
				{
					input_output->nfd_io.ENVP_ENV_RAW = input_output->ENVP_ENV_RAW;
					input_output->nfd_io.ms_ts = input_output->ms_ts;
					input_output->nfd_io.rtm_rt_end = 1;
					input_output->nfd_io.burst_en = 1;
					nfd_process_api(inst->ptr_nfd, &input_output->nfd_io, input_output->DSP_IRQ_STATUS);
				}
				//printf("ms_ts:%f,AMPD:%f,ENVP_ENV_RAW:%f,STG:%f,ATG:%f\n", input_output->edet_io.ms_ts, input_output->edet_io.AMPD_ENV, input_output->edet_io.ENVP_ENV_RAW, input_output->edet_io.stg_th, input_output->edet_io.atg_th);
				//printf("ms_ts:%f,STG:%f\n", input_output->edet_io.ms_ts, input_output->edet_io.stg_th);
				if (inst->nfd_key == 2)
				{
					nfd2_process_api(inst->ptr_nfd2, input_output->ms_ts, 1, input_output->ENVP_ENV_RAW);
				}
				
			}
		}
		else{
			input_output->ENVP_ENV_RAW = indata;
			if (dsp_cfg->ftc_en) 
				ftc_process_api(inst->ptr_ftc, input_output);
			else
				input_output->ENV_FTC = input_output->ENVP_ENV_RAW;
			if (inst->ampd_key == 1)
			{
				input_output->ampd_io.ms_ts = input_output->ms_ts;
				input_output->ampd_io.ENV_FTC = input_output->ENV_FTC;
				ampd_process_api(inst->ptr_ampd,  &input_output->ampd_io);
			}
			else
				input_output->ampd_io.AMPD_ENV = input_output->ENV_FTC;
			if (inst->edet_key == 1)
			{
				input_output->edet_io.AMPD_ENV = input_output->ampd_io.AMPD_ENV;
				input_output->edet_io.ENVP_ENV_RAW = input_output->ENVP_ENV_RAW;
				input_output->edet_io.ms_ts = input_output->ms_ts;
				edet_process_api(inst->ptr_edet, &input_output->edet_io,input_output->DSP_IRQ_STATUS);
			}

			if (inst->nfd_key == 1)
			{
				input_output->nfd_io.ENVP_ENV_RAW = input_output->ENVP_ENV_RAW;
				input_output->nfd_io.ms_ts = input_output->ms_ts;
				input_output->nfd_io.rtm_rt_end = 1;
				input_output->nfd_io.burst_en = 1;
				nfd_process_api(inst->ptr_nfd, &input_output->nfd_io,input_output->DSP_IRQ_STATUS);
			}
			if (inst->nfd_key == 2)
			{
				nfd2_process_api(inst->ptr_nfd2, input_output->ms_ts, 1, input_output->ENVP_ENV_RAW);
			}
		}
	}
	else
	{
		if (inst->envp_key) {
			envp_process_api(inst->ptr_envp, indata, input_output);
			if (inst->edet_key == 1)
				if (input_output->frame_num > input_output->delay * dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1) && input_output->frame_num % (dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1)) == 0) 
				{
					
					input_output->edet_io.ms_ts = input_output->ms_ts;
					input_output->edet_io.filt1 = input_output->filt1;
					input_output->edet_io.filt2 = input_output->filt2;
					input_output->edet_io.conf1 = input_output->conf1;
					input_output->edet_io.conf2 = input_output->conf2;
					//printf("ms_ts: %f: %f\n", input_output->filt1, input_output->edet_io.filt1);
					edet_process_api(inst->ptr_edet, &input_output->edet_io, input_output->DSP_IRQ_STATUS);
				}
		}
		else{
			input_output->filt1 = indata;
			input_output->edet_io.ms_ts = input_output->ms_ts;
			input_output->edet_io.filt1 = input_output->filt1;
			input_output->edet_io.filt2 = input_output->filt2;
			input_output->edet_io.conf1 = input_output->conf1;
			input_output->edet_io.conf2 = input_output->conf2;
			//printf("ms_ts: %f: %f\n", input_output->filt1, input_output->edet_io.filt1);
			edet_process_api(inst->ptr_edet, &input_output->edet_io, input_output->DSP_IRQ_STATUS);
		}
	}
	interrupt_process(input_output->DSP_IRQ_STATUS);
	return 0;
}


void dsp_destroy_api(DspParam* inst, DspConfig* dsp_cfg, DspInputOutput* input_output) 
{
	if (dsp_cfg->fix_path_process)
	{
		if (inst->envp_key == 1)
			envp_destroy_api(inst->ptr_envp, input_output);
		if (inst->edet_key == 1)
			edet_destroy_api(inst->ptr_edet);
		if (inst->ampd_key == 1)
			ampd_destroy_api(inst->ptr_ampd);
		if (dsp_cfg->ftc_en)
			ftc_destroy_api(inst->ptr_ftc, input_output);
		if (inst->nfd_key == 1)
			nfd_destroy_api(inst->ptr_nfd);
		if (inst->nfd_key == 2)
		    nfd2_destroy_api(inst->ptr_nfd);
	}
	else
	{
		if (inst->envp_key == 1)
			envp_destroy_api(inst->ptr_envp, input_output);
		if (inst->edet_key == 1)
			edet_destroy_api(inst->ptr_edet);
	}
}
