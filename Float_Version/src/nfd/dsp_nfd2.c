﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#include "dsp_nfd.h"

void nfd2_init(objNFD2* ptr_nfd2, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	ptr_nfd2->frame_len = input_output->framelen;
	ptr_nfd2->NFD2_THRES=dsp_cfg->nfd2_th;
	ptr_nfd2->frame_sum = 0;
	ptr_nfd2->NFD2_RESULT=0;
	ptr_nfd2->NFD2_START = dsp_cfg->NFD2_START;//1ms
	ptr_nfd2->NFD2_LENGTH = dsp_cfg->NFD2_LENGTH;//2ms
	ptr_nfd2->ms_ts_ls = 0;
	//ptr_nfd2->NFD2_START=0.001;//1ms
	//ptr_nfd2->NFD2_LENGTH=0.002;//2ms
	ptr_nfd2->NFD2_Flag = 0;
}

void nfd2_destroy(objNFD2* ptr_nfd2)
{
	free(ptr_nfd2);
}

int16_t nfd2_process(objNFD2* ptr_nfd2, float ms_ts, int16_t rtm_rt_end, float ENVP_ENV_RAW)
{
	//processing
	if ((ms_ts > ptr_nfd2->NFD2_START) && (ms_ts < ptr_nfd2->NFD2_START + ptr_nfd2->NFD2_LENGTH))
	{
		//ms_ts[idx - input_output->delay] = floor(((ptr_nfd2->frame_sum - 1) * ptr_nfd2->frame_len + 1 + i) * 1e6 / ptr_nfd2->samle_rate);
		ptr_nfd2->NFD2_RESULT = ptr_nfd2->NFD2_RESULT + ENVP_ENV_RAW / 30;
	}
	else if ((ms_ts > ptr_nfd2->NFD2_START + ptr_nfd2->NFD2_LENGTH) && (ptr_nfd2->ms_ts_ls <= ptr_nfd2->NFD2_START + ptr_nfd2->NFD2_LENGTH))
	{
		if (ptr_nfd2->NFD2_RESULT > ptr_nfd2->NFD2_THRES)
		{
			ptr_nfd2->NFD2_Flag = 1;
		}
	}
	ptr_nfd2->ms_ts_ls = ms_ts;
	ptr_nfd2->frame_sum++;
	return 0;
}