﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#include "dsp_nfd.h"


void nfd_init(objNFD* ptr_nfd, DspConfig* dsp_cfg)
{
	ptr_nfd->nfd_sens = dsp_cfg->NFD_CTRL & 7;
	ptr_nfd->nfd_irq_cfg = (dsp_cfg->NFD_CTRL & (3 << 3)) >> 3;
	ptr_nfd->nfd_max_num = (dsp_cfg->NFD_CTRL & (7 << 5)) >> 5;
	//printf("%d:%d:%d\n", ptr_nfd->nfd_sens, ptr_nfd->nfd_irq_cfg, ptr_nfd->nfd_max_num);
	//ptr_nfd->fmode = sample_rate / dsp_cfg->fc;
	ptr_nfd->nfd_th = dsp_cfg->nfd_th;
	//ptr_nfd->measure_mod = dsp_cfg->measure_mod;
	ptr_nfd->frame_sum = 0;
	ptr_nfd->fm_data = 0;
	ptr_nfd->positive_cnt = 0;
	ptr_nfd->fm_ms_ts = 0;
	if (ptr_nfd->nfd_irq_cfg >= 0 && ptr_nfd->nfd_irq_cfg < 4)
	{
		ptr_nfd->echos_num = ptr_nfd->nfd_irq_cfg + 2;
	}
	for (uint8_t i = 0; i < ptr_nfd->echos_num; i++)
	{
		ptr_nfd->nfd_echos[i] = 0;
		ptr_nfd->nfd_ts[i] = 0;
	}
	//ptr_nfd->ms_ts = (int32_t*)calloc(ptr_nfd->frame_len, sizeof(int32_t));
	//ptr_nfd->rtm_rt_end = (int16_t*)calloc(ptr_nfd->frame_len, sizeof(int16_t));
}

void nfd_destroy(objNFD* ptr_nfd)
{
	free(ptr_nfd);
}

uint8_t nfd_process(objNFD* ptr_nfd, NfdIO* nfd_io, uint16_t* DSP_IRQ_STATUS)
{
	if (nfd_io->rtm_rt_end == 1)
	{
		if (nfd_io->ENVP_ENV_RAW - ptr_nfd->fm_data > 0)
			ptr_nfd->positive_cnt += 1;
		else
		{
			if ((ptr_nfd->positive_cnt > ptr_nfd->nfd_sens) && (ptr_nfd->fm_data > ptr_nfd->nfd_th) && (ptr_nfd->nfd_max_num < ptr_nfd->echos_num))
			{
				ptr_nfd->nfd_max_num += 1;
				ptr_nfd->nfd_echos[ptr_nfd->nfd_max_num - 1] = ptr_nfd->fm_data;
				ptr_nfd->nfd_ts[ptr_nfd->nfd_max_num - 1] = ptr_nfd->fm_ms_ts;
			}
			ptr_nfd->positive_cnt = 0;
		}
	}
	ptr_nfd->fm_ms_ts = nfd_io->ms_ts;
	ptr_nfd->fm_data = nfd_io->ENVP_ENV_RAW;
	//printf("nfd :%f:%f:%d\n", ptr_nfd->fm_ms_ts, nfd_io->ENVP_ENV_RAW, ptr_nfd->positive_cnt);
	//}
	//printf("maxnum:%d\n",ptr_nfd->nfd_max_num);
	//output
	if (ptr_nfd->nfd_max_num == ptr_nfd->echos_num)
	{

		DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 256;
		//printf("DSP_IRQ_STATUS[0]:%d\n", input_output->DSP_IRQ_STATUS[0]);
		for (int16_t i = 0; i < ptr_nfd->echos_num; i++)
		{
			printf("peak detected:%d,%f,%f\n", i, ptr_nfd->nfd_echos[i], ptr_nfd->nfd_ts[i]);//print many times in different frame?
		}
		nfd_io->NFD_ECHO1 = ptr_nfd->nfd_echos[0];
		nfd_io->NFD_TS1 = ptr_nfd->nfd_ts[0];
		nfd_io->NFD_ECHO2 = ptr_nfd->nfd_echos[1];
		nfd_io->NFD_TS2 = ptr_nfd->nfd_ts[1];
		nfd_io->NFD_ECHO3 = ptr_nfd->nfd_echos[2];
		nfd_io->NFD_TS3 = ptr_nfd->nfd_ts[2];
		nfd_io->NFD_ECHO4 = ptr_nfd->nfd_echos[3];
		nfd_io->NFD_TS4 = ptr_nfd->nfd_ts[3];
		nfd_io->NFD_ECHO5 = ptr_nfd->nfd_echos[4];
		nfd_io->NFD_TS5 = ptr_nfd->nfd_ts[4];
		ptr_nfd->nfd_max_num = 0;
	}

	ptr_nfd->frame_sum++;
	return 0;
}