﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#ifndef _DSP_NFD_H_
#define _DSP_NFD_H_
#include "../../include/dsp_algo_api.h"
#ifdef __cplusplus
extern "C" {
#endif
//typedef struct
//{
//	//input
//	float nfd_th;
//	int16_t rtm_rt_end;
//	//int* burst_en;
//	float nfd_in;
//
//	//output
//	int16_t nfd_evt;
//	float* nfd_echos;
//	float* nfd_ts;
//
//	//parameter
//	int16_t nfd_sens;
//	int16_t nfd_irq_cfg;
//	int16_t echos_num;
//	int16_t echos_num_max;
//	int16_t nfd_max_num;
//
//	int16_t frame_len;
//	int32_t samle_rate;
//	int32_t frame_sum;
//	int16_t measure_mod;
//
//	float fm_data;
//	float fm_ms_ts;
//	int16_t positive_cnt;
//	int16_t fmode;
//	int16_t envp_len;
//} objNFD;


typedef struct
{
	//input
	float nfd_th;
	//output
	float nfd_evt;
	float nfd_echos[5];
	float nfd_ts[5];

	//parameter
	uint16_t nfd_sens;
	uint16_t nfd_irq_cfg;
	uint16_t echos_num;
	uint16_t echos_num_max;
	uint16_t nfd_max_num;

	uint32_t frame_sum;
	uint16_t measure_mod;

	float fm_data;
	float  fm_ms_ts;
	uint16_t positive_cnt;
	uint8_t fmode;
	uint16_t envp_len;
} objNFD;

typedef struct
{
	float NFD2_THRES;
	float NFD2_RESULT;
	float NFD2_START;
	float NFD2_LENGTH;
	float ms_ts_ls;
	int16_t NFD2_Flag;
	int16_t frame_len;
	int32_t frame_sum;
} objNFD2;

void nfd_init(objNFD* ptr_nfd, DspConfig* dsp_cfg);
uint8_t nfd_process(objNFD* ptr_nfd, NfdIO* nfd_io, uint16_t* DSP_IRQ_STATUS);
void nfd_destroy(objNFD* ptr_nfd);
//NFD2
void nfd2_init(objNFD2* ptr_nfd2, DspConfig* dsp_cfg, DspInputOutput* input_output);
int16_t nfd2_process(objNFD2* ptr_nfd2, float ms_ts, int16_t rtm_rt_end, float ENVP_ENV_RAW);
void nfd2_destroy(objNFD2* ptr_nfd2);

#ifdef __cplusplus
}
#endif
#endif //_DSP_NFD_H_