﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_nfd_api.h"

void* nfd_init_api(DspConfig* dsp_cfg)
{
	void* st = NULL;
	st = (void*)calloc(1, sizeof(objNFD));
	nfd_init((objNFD*)st, dsp_cfg);
	return st;
}

errorReturn nfd_process_api(void* ptr_nfd, NfdIO* nfd_io, uint16_t* DSP_IRQ_STATUS)
{
	int16_t ret = nfd_process((objNFD*)ptr_nfd, nfd_io, DSP_IRQ_STATUS);
	if (ret != 0)
		return errorNfd;
}

void  nfd_destroy_api(void* ptr)
{
	nfd_destroy((objNFD*)ptr);
}

void* nfd2_init_api(DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	void* st = NULL;
	st = (void*)calloc(1, sizeof(objNFD2));
	nfd2_init((objNFD2*)st, dsp_cfg, input_output);
	return st;
}

errorReturn nfd2_process_api(void* ptr, float ms_ts, int16_t rtm_rt_end, float ENVP_ENV_RAW)
{
	int16_t ret = nfd2_process( (objNFD2*) ptr, ms_ts, rtm_rt_end, ENVP_ENV_RAW);
	if (ret != 0)
		return errorNfd;
}

void  nfd2_destroy_api(void* ptr)
{
	nfd2_destroy((objNFD*)ptr);
}




