﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#ifndef _NFD_API_H_
#define _NFD_API_H_
#include "../../include/dsp_algo_api.h"
#include "dsp_nfd.h"
#ifdef __cplusplus 
extern "C" {
#endif
	void* nfd_init_api(DspConfig* dsp_cfg);
	errorReturn nfd_process_api(void* ptr, NfdIO* nfd_io, uint16_t* DSP_IRQ_STATUS);
	void nfd_destroy_api(void* ptr);
	void* nfd2_init_api(DspConfig* dsp_cfg, DspInputOutput* input_output);
	errorReturn nfd2_process_api(void* ptr, float ms_ts, int16_t rtm_rt_end, float ENVP_ENV_RAW);
	void  nfd2_destroy_api(void* ptr);
#ifdef __cplusplus 
}
#endif
#endif 
