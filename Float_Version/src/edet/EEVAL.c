﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"


uint8_t EEVAL_process(FIXED_EEVAL* EEVAL, float ms_ts, float AMPD_ENV, float ENVP_ENV_RAW, uint16_t* DSP_IRQ_STATUS)
{
	// EEVAL
	uint8_t maximum_detc = 0;
	uint8_t minimum_detc = 0;
	float dma_val = ENVP_ENV_RAW ;
	if (EEVAL->eeval_dma_val)
	{
		dma_val = AMPD_ENV ;//only used the first 8 bits of AMPD_ENV in DMA.therefore ,there is no need to shift 8 bits to right when printing.
	}
	EEVAL->data_packet.dma_val = dma_val;
	//EEVAL->data_packet = 0;
	if (EEVAL->ls_AMPD_ENV < AMPD_ENV)
	{
		EEVAL->positive_cnt += 1;
		if (EEVAL->negative_cnt > EEVAL->eeval_sens)
		{
			//MIN_EVT minimum on ENVP_ENV_RAW detected
			minimum_detc = 1;
		}
		EEVAL->negative_cnt = 0;
	}
	if (EEVAL->ls_AMPD_ENV > AMPD_ENV)
	{
		EEVAL->negative_cnt += 1;
		if (EEVAL->positive_cnt > EEVAL->eeval_sens)
		{
			//MAX_EVT maximum on ENVP_ENV_RAW detected
			maximum_detc = 1;
		}
		EEVAL->positive_cnt = 0;
	}
	//printf("%d:%f:%f:%f\n", edet->frame_num, (float)ms_ts, (float)AMPD_ENV / (1 << AMPD_ENV_SCALE), (float)EEVAL->threshold / (1 << AMPD_ENV_SCALE));
	//printf("%d:%f:%f:%f\n", edet->frame_num, (float)ms_ts, (float)AMPD_ENV / (1 << AMPD_ENV_SCALE), (float)edet->fixed_out_ptr.stg_th/ (1 << AMPD_ENV_SCALE));
	if (AMPD_ENV >= EEVAL->threshold)
	{
		//printf("in\n");
		//printf("%d:%d:%d:%d:%d\n", EEVAL->eeval_sel, EEVAL->eeval_sel & 1 == 1, (EEVAL->eeval_sel & 8)==8 , EEVAL->eeval_sel & 4 == 4, EEVAL->eeval_sel & 2 == 2);
		if ((EEVAL->ls_AMPD_ENV < EEVAL->ls_threshold) && (EEVAL->eeval_sel == 0))
		{
			//TLH_EVT positive threshold crossing
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 1;
			EEVAL->data_packet.time_stamp = ms_ts;
			EEVAL->data_packet.evt_identification = 0;
		}
		if (minimum_detc && (EEVAL->eeval_sel  == 3))
		{
			//MIN_EVT minimum on ENVP_ENV_RAW detected
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 8;
			EEVAL->data_packet.dma_val = EEVAL->ls_dma_val;
			EEVAL->data_packet.time_stamp= EEVAL->ls_ms_ts;
			EEVAL->data_packet.evt_identification = 2;
		}
		if (maximum_detc && (EEVAL->eeval_sel == 2))
		{
			//MAX_EVT maximum on ENVP_ENV_RAW detected
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 4;
			EEVAL->data_packet.dma_val = EEVAL->ls_dma_val;
			EEVAL->data_packet.time_stamp= EEVAL->ls_ms_ts ;
			EEVAL->data_packet.evt_identification = 3;
		}
	}
	else
	{
		if ((EEVAL->ls_AMPD_ENV >= EEVAL->ls_threshold) && (EEVAL->eeval_sel  == 1))
		{
			//THL_EVT negative threshold crossing
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 2;
			EEVAL->data_packet.time_stamp= ms_ts ;
			EEVAL->data_packet.evt_identification = 1;
		}
	}
	EEVAL->ls_ms_ts = ms_ts;
	EEVAL->ls_dma_val = dma_val;
	EEVAL->ls_AMPD_ENV = AMPD_ENV;
	EEVAL->ls_threshold = EEVAL->threshold;
	//printf("evttype:%d,AMPD:%f msts:%f threshold: %f\n", EEVAL->data_packet.evt_identification, EEVAL->data_packet.dma_val, EEVAL->ls_ms_ts, EEVAL->ls_threshold);
	//edet_result
	if ((DSP_IRQ_STATUS[0] & 15) > 0)
	{
		printf("evttype:%d,AMPD:%f msts:%f\n", EEVAL->data_packet.evt_identification, EEVAL->data_packet.dma_val, EEVAL->data_packet.time_stamp);
	}
	//fixed_EEVAL->data_packet = 0;
	return 0;
}

