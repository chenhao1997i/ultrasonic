﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"


int16_t STG_process(EDET_PARAM* edet, float ms_ts, float STG_TH, float STG_TB, float STG_STEP)
{
    // STG
    if (fabs(STG_TH - edet->fixed_freq.stg_th) >= fabs(STG_STEP) / 2)//abs only work with int in C, need to use fabs for float
    {
        if (floor(ms_ts / STG_TB) > floor(edet->EEVAL.ls_ms_ts / STG_TB))
        {
            //edet->fixed_freq.stg_th = edet->fixed_freq.stg_th + STG_STEP * (STG_TH - edet->fixed_freq.stg_th) / fabs(STG_TH - edet->fixed_freq.stg_th);
            edet->fixed_freq.stg_th = edet->fixed_freq.stg_th + STG_STEP;
        }
    }
	//printf("%f:%f\n",  ms_ts, edet->EEVAL.ls_ms_ts);
    return 0;
}
