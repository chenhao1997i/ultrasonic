﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#ifndef _DSP_EDET_H_
#define _DSP_EDET_H_
#include "../../include/dsp_algo_api.h"
#ifdef __cplusplus
extern "C" {
#endif


typedef struct
{
    float conf;// last ms_ts
    float filt;//last conf
    float time_stamp;// last filt
} ADV_DATAPACKET;


typedef struct
{
    uint8_t evt_identification;// last ms_ts
    float dma_val;//last conf
    float time_stamp;// last filt
} STD_DATAPACKET;


typedef struct
{
    uint8_t eeval_sel;
    uint8_t eeval_sens;
    uint8_t eeval_dma_val;
    uint16_t positive_cnt;
    uint16_t negative_cnt;
    float threshold;
    float ls_ms_ts;// last ms_ts
    float ls_dma_val;// last dma_val
    float ls_AMPD_ENV;// last AMPD_ENV
    float ls_threshold;// threshold
    STD_DATAPACKET data_packet;
} FIXED_EEVAL;

typedef struct
{
    uint8_t irq_bit;
    uint8_t eeval_sens;
    uint8_t positive_cnt;
    float threshold;
    float ls_ms_ts;// last ms_ts
    float ls_conf;//last conf
    float ls_filt;// last filt
    ADV_DATAPACKET data_packet;//only 36 bit is used
} ADV_EEVAL;

typedef struct
{
    uint8_t aatg_buf_full;
    uint16_t aatg_buf_cnt;
    float aatg_alpha;
    uint8_t aatg_cw;
    uint8_t aatg_cn;
    uint16_t cell_cnt;
    uint16_t filt_cnt;
    float aatg_th;
    uint8_t k;
    float aatg_buf[24];
    float last_cell;
    uint8_t aatg_buf_size;
    float cell_register[16];
    float sorted_cell_register[24];
    float msts_buf[208];
    float filt_buf[208];
    float conf_buf[208];
    uint8_t filt_buf_size;
    float current_msts;
    float current_filt;
    float current_conf;
    // GOSCA_CFAR
    uint8_t Lk;
    uint8_t Rk;
    float L_sorted_cell_register[12];
    float R_sorted_cell_register[12];
} FIXED_AATG;

typedef struct
{
    uint8_t atg_cfg;
    float atg_ini;
    float atg_tau;
    float atg_alpha;
    float tmp_atg_th;
    float atg_th;
    float stg_th;
} FIXED_FREQ;

typedef struct
{
    int32_t frame_num;
    // ATG parameter
    FIXED_FREQ fixed_freq;
    // AATG parameter
    // AATG variable
    FIXED_AATG AATG1;
    FIXED_AATG AATG2;
    // EEVAL variable
    FIXED_EEVAL EEVAL;
    ADV_EEVAL adv_EEVAL1;
    ADV_EEVAL adv_EEVAL2;
} EDET_PARAM;

void* edet_init(EDET_PARAM* edet, DspConfig* dsp_cfg);
int16_t edet_process(EDET_PARAM* edet, EdetIO* edetio, uint16_t* DSP_IRQ_STATUS);
void edet_destroy(EDET_PARAM* edet);
int16_t ATG_process(FIXED_FREQ* fixed_freq, float AMPD_ENV);
int16_t STG_process(EDET_PARAM* edet, float ms_ts, float STG_TH, float STG_TB, float STG_STEP); 
void insert_sort(float* a, uint8_t len, float old_element, float new_element); //ascending sort
uint8_t AATG_process(FIXED_AATG* aatg, float ms_ts_fixed, float filt_fixed, float conf);
uint8_t OS_CFAR_process(FIXED_AATG* aatg);
uint8_t GOSCA_CFAR_process(FIXED_AATG* aatg);
uint8_t EEVAL_process(FIXED_EEVAL* EEVAL, float ms_ts, float AMPD_ENV, float ENVP_ENV_RAW, uint16_t* DSP_IRQ_STATUS);
uint8_t adv_EEVAL_process(ADV_EEVAL* adv_EEVAL, float ms_ts, float filt, float conf, uint16_t* DSP_IRQ_STATUS);
#ifdef __cplusplus
}
#endif
#endif //_DSP_EDET_H_
