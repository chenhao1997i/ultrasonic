﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"

void* edet_init(EDET_PARAM* edet, DspConfig* dsp_cfg)
{
    edet->frame_num = 0;

    //fixed aatg1
    edet->AATG1.aatg_th = 0;
    edet->AATG1.aatg_buf_full = 0;
    edet->AATG1.aatg_buf_cnt = 0;
    edet->AATG1.aatg_cn = dsp_cfg->aatg_cn;// cell number
    edet->AATG1.aatg_cw = dsp_cfg->aatg_cw;// cell width
    float aatgalpha_list[] = { 3.5,4,4.5,5 };
    edet->AATG1.aatg_alpha = aatgalpha_list[dsp_cfg->aatg_alpha];// cell number
    //printf("%d:%d:%d\n", edet->AATG1.aatg_cn, edet->AATG1.aatg_cw, edet->AATG1.aatg_alpha);
    edet->AATG1.k = edet->AATG1.aatg_cn + 1;
    edet->AATG1.Lk = edet->AATG1.aatg_cn / 2;
    edet->AATG1.Rk = edet->AATG1.aatg_cn / 2;
    edet->AATG1.aatg_buf_size = edet->AATG1.aatg_cn << 1;
    edet->AATG1.filt_buf_size = edet->AATG1.aatg_cw * (edet->AATG1.aatg_cn + 1);
    //printf("rec_aatg_cw=%d\n", edet->AATG1.rec_aatg_cw);
    //printf("aatg_cn=%d,aatg_cw=%d",edet->AATG1.aatg_cn,edet->AATG1.aatg_cw);
    edet->AATG1.current_filt = 0;
    edet->AATG1.current_msts = 0;
    edet->AATG1.current_conf = 0;
    edet->AATG1.cell_cnt = 0;
    edet->AATG1.filt_cnt = 0;
    edet->AATG1.last_cell = 0;
    for (uint8_t i = 0; i < 2 * edet->AATG1.aatg_cn; i++)
    {
        edet->AATG1.sorted_cell_register[i] = 0;
        edet->AATG1.aatg_buf[i] = 0;
    }
    for (uint8_t i = 0; i < edet->AATG1.aatg_cw; i++)
    {
        edet->AATG1.cell_register[i] = 0;
    }
    for (uint8_t i = 0; i < edet->AATG1.aatg_cw * (edet->AATG1.aatg_cn + 1); i++)
    {
        edet->AATG1.msts_buf[i] = 0;
        edet->AATG1.filt_buf[i] = 0;
        edet->AATG1.conf_buf[i] = 0;
    }

    //fixed aatg2
    edet->AATG2.aatg_th = 0;
    edet->AATG2.aatg_buf_full = 0;
    edet->AATG2.aatg_buf_cnt = 0;
    edet->AATG2.aatg_cn = dsp_cfg->aatg_cn;// cell number
    edet->AATG2.aatg_cw = dsp_cfg->aatg_cw;// cell width
    edet->AATG2.aatg_alpha = aatgalpha_list[dsp_cfg->aatg_alpha];// cell number
    //printf("%d:%d:%d\n", edet->AATG2.aatg_cn, edet->AATG2.aatg_cw, edet->AATG2.aatg_alpha);
    edet->AATG2.k = edet->AATG2.aatg_cn + 1;
    edet->AATG2.Lk = edet->AATG2.aatg_cn / 2;
    edet->AATG2.Rk = edet->AATG2.aatg_cn / 2;
    edet->AATG2.aatg_buf_size = edet->AATG2.aatg_cn << 1;
    edet->AATG2.filt_buf_size = edet->AATG2.aatg_cw * (edet->AATG2.aatg_cn + 1);
    //printf("rec_aatg_cw=%d\n", edet->AATG2.rec_aatg_cw);
    //printf("aatg_cn=%d,aatg_cw=%d",edet->AATG2.aatg_cn,edet->AATG2.aatg_cw);
    edet->AATG2.current_filt = 0;
    edet->AATG2.current_msts = 0;
    edet->AATG2.current_conf = 0;
    edet->AATG2.cell_cnt = 0;
    edet->AATG2.filt_cnt = 0;
    edet->AATG2.last_cell = 0;
    for (uint8_t i = 0; i < 2 * edet->AATG2.aatg_cn; i++)
    {
        edet->AATG2.sorted_cell_register[i] = 0;
        edet->AATG2.aatg_buf[i] = 0;
    }
    for (uint8_t i = 0; i < edet->AATG2.aatg_cw; i++)
    {
        edet->AATG2.cell_register[i] = 0;
    }
    for (uint8_t i = 0; i < edet->AATG2.aatg_cw * (edet->AATG2.aatg_cn + 1); i++)
    {
        edet->AATG2.msts_buf[i] = 0;
        edet->AATG2.filt_buf[i] = 0;
        edet->AATG2.conf_buf[i] = 0;
    }
    
    // ATG variable
    float inilist[] = { 0,32,64,80,96,112,128,144 };
    edet->fixed_freq.atg_ini = inilist[dsp_cfg->atg_ini];// atg initial value; config.atg_ini = 5;
    float taulist[] = { 1, 1.5, 2, 3, 4, 5, 6, 6 };
    edet->fixed_freq.atg_tau = pow(0.997444, (taulist[dsp_cfg->atg_tau]));// atg time response configuration,affect the slope of threshold ;  config.atg_tau = 3;
    float alpha_list[] = {2,2.5,3,3.5,4,4.5,5,6 };
    edet->fixed_freq.atg_alpha = alpha_list[dsp_cfg->atg_alpha] / 1.4;// atg sensitivity configuration
    edet->fixed_freq.tmp_atg_th = edet->fixed_freq.atg_ini / edet->fixed_freq.atg_alpha;
    edet->fixed_freq.atg_th = edet->fixed_freq.atg_ini;
    edet->fixed_freq.atg_cfg = dsp_cfg->atg_cfg;// atg switch 1 on 0 off
    // STG variable
    edet->fixed_freq.stg_th = 128;
    //printf("%f:%f:%f\n", edet->fixed_freq.tmp_atg_th, edet->fixed_freq.atg_ini, edet->fixed_freq.atg_alpha);
    //std EEVAL
    edet->EEVAL.eeval_sel = dsp_cfg->eeval_sel;// TLH_EVT 0,THL_EVT 1, MAX 2, MIN 3
    edet->EEVAL.eeval_sens = dsp_cfg->eeval_sens;
    edet->EEVAL.eeval_dma_val = dsp_cfg->eeval_dma_val;
    edet->EEVAL.ls_AMPD_ENV = 0;
    edet->EEVAL.ls_ms_ts = 0;
    edet->EEVAL.ls_threshold = 0;
    edet->EEVAL.ls_dma_val = 0;
    edet->EEVAL.threshold = 0;
    edet->EEVAL.negative_cnt = 0;
    edet->EEVAL.positive_cnt = 0;
    //adv EEVAL1
    edet->adv_EEVAL1.eeval_sens = dsp_cfg->eeval_sens;
    edet->adv_EEVAL1.ls_filt = 0;
    edet->adv_EEVAL1.ls_conf = 0;
    edet->adv_EEVAL1.ls_ms_ts = 0;
    edet->adv_EEVAL1.threshold = 0;
    edet->adv_EEVAL1.positive_cnt = 0;
    edet->adv_EEVAL1.data_packet.conf = 0;
    edet->adv_EEVAL1.data_packet.filt = 0;
    edet->adv_EEVAL1.data_packet.time_stamp = 0;
    edet->adv_EEVAL1.irq_bit = 16;

    //adv EEVAL2
    edet->adv_EEVAL2.eeval_sens = dsp_cfg->eeval_sens;
    edet->adv_EEVAL2.ls_filt = 0;
    edet->adv_EEVAL2.ls_conf = 0;
    edet->adv_EEVAL2.ls_ms_ts = 0;
    edet->adv_EEVAL2.threshold = 0;
    edet->adv_EEVAL2.positive_cnt = 0;
    edet->adv_EEVAL2.data_packet.conf = 0;
    edet->adv_EEVAL2.data_packet.filt = 0;
    edet->adv_EEVAL2.data_packet.time_stamp = 0;
    edet->adv_EEVAL2.irq_bit = 32;

    return edet;
}

int16_t edet_process(EDET_PARAM* edet, EdetIO* edetio, uint16_t* DSP_IRQ_STATUS)
{
    //printf("ms_ts: %f: %f\n", edetio->ms_ts, edetio->filt1);
    //fixed_frequency_process(edet, ms_ts, AMPD_ENV, STG_TH, STG_TB, STG_STEP);
    ATG_process(&edet->fixed_freq, edetio->AMPD_ENV);
    STG_process(edet, edetio->ms_ts, edetio->STG_TH, edetio->STG_TB, edetio->STG_STEP);
    edet->EEVAL.threshold = fmax(edet->fixed_freq.stg_th, edet->fixed_freq.atg_th);
    EEVAL_process(&edet->EEVAL, edetio->ms_ts, edetio->AMPD_ENV, edetio->ENVP_ENV_RAW, DSP_IRQ_STATUS);
    edetio->std_datapacket[0]= edet->EEVAL.data_packet.evt_identification;
    edetio->std_datapacket[1] = edet->EEVAL.data_packet.dma_val;
    edetio->std_datapacket[2] = edet->EEVAL.data_packet.time_stamp;
    AATG_process(&edet->AATG1, edetio->ms_ts, edetio->filt1, edetio->conf1);
    if (edet->AATG1.aatg_buf_full)
    {
        edet->adv_EEVAL1.threshold = edet->AATG1.aatg_th;
        adv_EEVAL_process(&edet->adv_EEVAL1, edet->AATG1.current_msts, edet->AATG1.current_filt, edet->AATG1.current_conf, DSP_IRQ_STATUS);
        //printf("%f   %f   %f\n", edet->AATG1.current_msts, edet->AATG1.current_filt, edetio->aatg1_th);
    }
    edetio->adv_datapacket1[0] = edet->adv_EEVAL1.data_packet.conf;
    edetio->adv_datapacket1[1] = edet->adv_EEVAL1.data_packet.filt;
    edetio->adv_datapacket1[2] = edet->adv_EEVAL1.data_packet.time_stamp;
    AATG_process(&edet->AATG2, edetio->ms_ts, edetio->filt2, edetio->conf2);
    if (edet->AATG2.aatg_buf_full)
    {
        edet->adv_EEVAL2.threshold = edet->AATG2.aatg_th;
        adv_EEVAL_process(&edet->adv_EEVAL2, edet->AATG2.current_msts, edet->AATG2.current_filt, edet->AATG2.current_conf, DSP_IRQ_STATUS);
    }
    edetio->atg_th= edet->fixed_freq.atg_th; 
    edetio->stg_th= edet->fixed_freq.stg_th;
    edetio->aatg1_th = edet->AATG1.aatg_th;
    edetio->aatg2_th = edet->AATG2.aatg_th;
    //printf("%f   %.10f   %.10f\n", edet->AATG1.current_msts,  edetio->aatg1_th, edetio->aatg2_th);
    edetio->aatg_filt2= edet->AATG2.current_filt;
    edetio->aatg_conf1= edet->AATG1.current_conf;
    edetio->aatg_conf2= edet->AATG2.current_conf;
    edetio->adv_datapacket2[0] = edet->adv_EEVAL1.data_packet.conf;
    edetio->adv_datapacket2[1] = edet->adv_EEVAL1.data_packet.filt;
    edetio->adv_datapacket2[2] = edet->adv_EEVAL1.data_packet.time_stamp;
    edet->frame_num++;
    //printf("%d\n", edet->frame_num);
    return 0;
}

void edet_destroy(EDET_PARAM* edet)
{
    //free(edet->out_ptr);
    free (edet);
}

