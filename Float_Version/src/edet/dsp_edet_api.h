﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#ifndef _EDET_API_H_
#define _EDET_API_H_
#include "../../include/dsp_algo_api.h"

#ifdef __cplusplus 
extern "C" {
#endif

void* edet_init_api(DspConfig* dsp_cfg);
errorReturn edet_process_api(void* ptr, EdetIO* edet_io, uint16_t* DSP_IRQ_STATUS);
void edet_destroy_api(void* ptr);


#ifdef __cplusplus 
}
#endif
#endif 
