﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"


int16_t ATG_process(FIXED_FREQ* fixed_freq, float AMPD_ENV)
{
    // ATG
    //printf("%f:%f:%f\n", fixed_freq->atg_th, fixed_freq->tmp_atg_th, fixed_freq->atg_cfg);
    if (fixed_freq->atg_cfg)
    {
        if (fixed_freq->atg_th > AMPD_ENV)
        {
            fixed_freq->tmp_atg_th = fixed_freq->tmp_atg_th * fixed_freq->atg_tau + (1 - fixed_freq->atg_tau) * AMPD_ENV;
            fixed_freq->atg_th = fixed_freq->atg_alpha * fixed_freq->tmp_atg_th;
        }
    }
    else
    {
        fixed_freq->atg_th = 0;
    }
	//printf("%f:%f:%f\n", fixed_freq->atg_th, fixed_freq->tmp_atg_th, fixed_freq->atg_alpha);
    return 0;
}
