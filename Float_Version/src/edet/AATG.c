﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"

uint8_t AATG_process(FIXED_AATG* aatg, float ms_ts_fixed, float filt_fixed, float conf)
{
    //EDET_PARAM* edet = (EDET_PARAM*)param_ptr;
//printf("%d\t", edet->aatg_buf_cnt);
    if (aatg->cell_cnt >= aatg->aatg_cw)
    {
        aatg->cell_cnt = 0;
        aatg->last_cell = aatg->aatg_buf[aatg->aatg_buf_cnt];//restore last cell for insert sort
        aatg->aatg_buf[aatg->aatg_buf_cnt] = 0;
        //printf("%d:%d\n", edet->frame_num, aatg->aatg_buf_cnt);
        uint32_t temp_mul = 0;
        uint32_t temp_sum = 0;
        for (uint16_t i = 0; i < aatg->aatg_cw; i++)//is there a better way to sum?
        {
            aatg->aatg_buf[aatg->aatg_buf_cnt] = aatg->aatg_buf[aatg->aatg_buf_cnt] + aatg->cell_register[i] / aatg->aatg_cw;
            //temp_shift= temp_mul >> rec_aatg_cw_SCALE;
            //aatg->aatg_buf[aatg->aatg_buf_cnt] = aatg->aatg_buf[aatg->aatg_buf_cnt]+temp_shift;
        }
        //printf("%f\n", (float)aatg->aatg_buf[aatg->aatg_buf_cnt]/(1<< AMPD_ENV_SCALE));
        //edet->aatg_buf[edet->aatg_buf_cnt] = edet->aatg_buf[edet->aatg_buf_cnt] / edet->aatg_cw;
        //GOSCA_CFAR_process(aatg);
        OS_CFAR_process(aatg);
        aatg->aatg_buf_cnt += 1;
    }
    aatg->cell_register[aatg->cell_cnt] = filt_fixed;
    if (aatg->aatg_buf_cnt >= aatg->aatg_buf_size)
    {
        aatg->aatg_buf_full = 1;
        aatg->aatg_buf_cnt = 0;
    }
    aatg->current_msts = aatg->msts_buf[aatg->filt_cnt];
    aatg->current_filt = aatg->filt_buf[aatg->filt_cnt];
    aatg->current_conf = aatg->conf_buf[aatg->filt_cnt];
    aatg->msts_buf[aatg->filt_cnt] = ms_ts_fixed;
    aatg->filt_buf[aatg->filt_cnt] = filt_fixed;
    aatg->conf_buf[aatg->filt_cnt] = conf;
    aatg->cell_cnt += 1;
    aatg->filt_cnt += 1;
    if (aatg->filt_cnt >= aatg->filt_buf_size)
    {
        aatg->filt_cnt = 0;
    }
    return 0;
}
uint8_t OS_CFAR_process(FIXED_AATG* aatg)
{
    insert_sort(aatg->sorted_cell_register, aatg->aatg_cn * 2, aatg->last_cell, aatg->aatg_buf[aatg->aatg_buf_cnt]);
    //edet->fixed_EEVAL.threshold = aatg->sorted_cell_register[aatg->k] * aatg->aatg_alpha;
    aatg->aatg_th = aatg->sorted_cell_register[aatg->k] * aatg->aatg_alpha;
    return 0;
}

uint8_t GOSCA_CFAR_process(FIXED_AATG* aatg)
{
    uint8_t left_new_index = 0;
    if (aatg->aatg_buf_cnt >= aatg->aatg_cn)
    {
        left_new_index = aatg->aatg_buf_cnt - aatg->aatg_cn;
    }
    else
    {
        left_new_index = aatg->aatg_buf_cnt + aatg->aatg_cn;
    }
    insert_sort(aatg->L_sorted_cell_register, aatg->aatg_cn, aatg->last_cell, aatg->aatg_buf[left_new_index]);
    insert_sort(aatg->R_sorted_cell_register, aatg->aatg_cn, aatg->aatg_buf[left_new_index], aatg->aatg_buf[aatg->aatg_buf_cnt]);
    aatg->aatg_th = (aatg->L_sorted_cell_register[aatg->Lk] + aatg->R_sorted_cell_register[aatg->Rk]) / 2 * aatg->aatg_alpha;
    return 0;
}

void insert_sort(float* a, uint8_t len, float old_element, float new_element) //ascending sort
{
    //printf("old:%f,new:%f\n", (float)old_element / (1 << AMPD_ENV_SCALE), (float)new_element/ (1 << AMPD_ENV_SCALE));
    for (uint16_t i = 0; i < len; ++i)//find old element
    {
        if (a[i] == old_element)
        {
            for (uint16_t j = i; j < len - 1; ++j)
            {
                a[j] = a[j + 1];
            }
            a[len - 1] = new_element;
            break;
        }
    }
    //printf("temp result:\n");
    //for (float i = 0; i < len; ++i)
    //{
    //    printf("%f\t", (float)a[i] / (1 << AMPD_ENV_SCALE));
    //    //printf("%f\t", (float)(edet->fixed_AATG.aatg_buf[i])/ (1 << AMPD_ENV_SCALE));
    //}
    //printf("end \n");
    for (uint16_t i = len - 1; i > 0; --i)//sort new element
    {
        if (a[i] < a[i - 1])
        {
            float tmp = a[i - 1];
            a[i - 1] = a[i];
            a[i] = tmp;
        }
    }

}
//int16_t AATG_process(EDET_PARAM* edet, float ms_ts, float filt1, float ENVP_ENV_RAW)
//{
//    //EDET_PARAM* edet = (EDET_PARAM*)param_ptr;
//    //printf("%d\t", edet->aatg_buf_cnt);
//    if (edet->cell_cnt >= edet->aatg_cw)
//    {
//        edet->cell_cnt = 0;
//        edet->aatg_buf[edet->aatg_buf_cnt] = 0;
//        for (int16_t i = 0; i < edet->aatg_cw; i++)//is there a better way to sum?
//        {
//            edet->aatg_buf[edet->aatg_buf_cnt] = edet->aatg_buf[edet->aatg_buf_cnt] + edet->cell_register[i] / edet->aatg_cw;
//        }
//        //edet->aatg_buf[edet->aatg_buf_cnt] = edet->aatg_buf[edet->aatg_buf_cnt] / edet->aatg_cw;
//        //GOSCA_CFAR_process(edet);
//        OS_CFAR_process(edet);
//        edet->aatg_buf_cnt += 1;
//    }
//    edet->cell_register[edet->cell_cnt] = filt1;
//    if (edet->aatg_buf_cnt >= 2 * edet->aatg_cn)
//    {
//        edet->aatg_buf_full = 1;
//        edet->aatg_buf_cnt = 0;
//    }
//    edet->current_msts = edet->msts_buf[edet->filt1_cnt];
//    edet->current_filt1 = edet->filt1_buf[edet->filt1_cnt];
//    edet->msts_buf[edet->filt1_cnt] = ms_ts;
//    edet->filt1_buf[edet->filt1_cnt] = filt1;
//    edet->cell_cnt += 1;
//    edet->filt1_cnt += 1;
//    if (edet->filt1_cnt >= edet->aatg_cw * (edet->aatg_cn + 1))
//    {
//        edet->filt1_cnt = 0;
//    }
//    return 0;
//}
//
//void Quicksort(float* a,  int len) //ascending sort
//{   
//    for (int i = 1; i <= len ; ++i)
//    {
//        for (int j = 0; j < len- i ; ++j)
//            if (a[j] > a[j + 1])
//            {
//                float tmp = a[j + 1];
//                a[j + 1] = a[j];
//                a[j] = tmp;
//            }
//    }
//}
//
//int16_t OS_CFAR_process(EDET_PARAM* edet)
//{
//    memcpy(edet->sorted_cell_register, edet->aatg_buf, sizeof(float) * 2 * edet->aatg_cn);
//    Quicksort(edet->sorted_cell_register,edet->aatg_cn*2);
//    edet->threshold = edet->sorted_cell_register[edet->k] * edet->aatg_alpha;
//    return 0;
//}
//int16_t GOSCA_CFAR_process(EDET_PARAM* edet)
//{
//    memcpy(edet->sorted_cell_register, edet->aatg_buf, sizeof(float) * 2 * edet->aatg_cn);
//    if (edet->aatg_buf_cnt >= edet->aatg_cn-1)
//    {
//        memcpy(edet->R_sorted_cell_register, edet->aatg_buf + (edet->aatg_buf_cnt - edet->aatg_cn+1), sizeof(float) * edet->aatg_cn);
//        memcpy(edet->L_sorted_cell_register,  edet->aatg_buf + edet->aatg_buf_cnt+1, sizeof(float) * (2 * edet->aatg_cn - edet->aatg_buf_cnt-1));
//        memcpy(edet->L_sorted_cell_register + (2 * edet->aatg_cn - edet->aatg_buf_cnt-1), edet->aatg_buf, sizeof(float)* (edet->aatg_cn - (2 * edet->aatg_cn - edet->aatg_buf_cnt-1)));
//    }
//    else
//    {
//        memcpy(edet->L_sorted_cell_register, edet->aatg_buf + edet->aatg_buf_cnt+1 , sizeof(float) * 1 * edet->aatg_cn);
//        memcpy(edet->R_sorted_cell_register, edet->aatg_buf + (edet->aatg_buf_cnt + edet->aatg_cn+1), sizeof(float)* (edet->aatg_cn - edet->aatg_buf_cnt-1));
//        memcpy(edet->R_sorted_cell_register + (edet->aatg_cn - edet->aatg_buf_cnt - 1), edet->aatg_buf, sizeof(float) * (edet->aatg_buf_cnt+1));
//    }
//    Quicksort(edet->L_sorted_cell_register, edet->aatg_cn);
//    Quicksort(edet->R_sorted_cell_register, edet->aatg_cn);
//    edet->threshold =(edet->L_sorted_cell_register[edet->Lk]+edet->R_sorted_cell_register[edet->Rk])/2 * edet->aatg_alpha;
//    return 0;
//}

