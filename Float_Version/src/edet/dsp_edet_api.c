﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_edet_api.h"
#include "dsp_edet.h"

void* edet_init_api(DspConfig* dsp_cfg)
{
	void* st = NULL;
	st = (void*)calloc(1, sizeof(EDET_PARAM));
	edet_init((EDET_PARAM*) st, dsp_cfg);
	return st;
}

errorReturn edet_process_api(void* ptr, EdetIO* edetio, uint16_t* DSP_IRQ_STATUS)
{
	int16_t ret = edet_process((EDET_PARAM*)ptr, edetio, DSP_IRQ_STATUS);
	if (ret != 0)
		return errorEdet;
}

void  edet_destroy_api(void* ptr)
{
	edet_destroy((EDET_PARAM*) ptr);
}




