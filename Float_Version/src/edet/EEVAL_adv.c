﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"


uint8_t adv_EEVAL_process(ADV_EEVAL* adv_EEVAL, float ms_ts, float filt, float conf, uint16_t* DSP_IRQ_STATUS)
{
    // EEVAL
    int16_t maximum_detc = 0;
    if (adv_EEVAL->ls_filt < filt)
    {
        adv_EEVAL->positive_cnt += 1;
    }
    if (adv_EEVAL->ls_filt > filt)
    {
        if (adv_EEVAL->positive_cnt > adv_EEVAL->eeval_sens)
        {
            //MAX_EVT maximum on ENVP_ENV_RAW detected
            maximum_detc = 1;
        }
        adv_EEVAL->positive_cnt = 0;
    }
    //printf("%f:%f:%f:%d\n", adv_EEVAL->threshold, filt, ms_ts, adv_EEVAL->positive_cnt);
    if (maximum_detc == 1)
    {
        if (adv_EEVAL->ls_filt >= adv_EEVAL->threshold)
        {
            //MAX_EVT maximum on ENVP_ENV_RAW detected
            DSP_IRQ_STATUS[0]|= adv_EEVAL->irq_bit;
            //edet->fixed_out_ptr.IRQ_ptr.eeval1_max_evt = 1;
            //adv_EEVAL->data_packet = adv_EEVAL->ls_ms_ts + (adv_EEVAL->ls_filt << 16) + (adv_EEVAL->ls_conf << 32);
            adv_EEVAL->data_packet.conf=adv_EEVAL->ls_conf;
            adv_EEVAL->data_packet.filt=adv_EEVAL->ls_filt;
            adv_EEVAL->data_packet.time_stamp=adv_EEVAL->ls_ms_ts;
        }
    }
    adv_EEVAL->ls_ms_ts = ms_ts;
    adv_EEVAL->ls_filt = filt;
    adv_EEVAL->ls_conf = conf;
    //edet_result
    //printf("%d:%d:%f\n", DSP_IRQ_STATUS[0], adv_EEVAL->irq_bit, ms_ts);
    if ((DSP_IRQ_STATUS[0]&adv_EEVAL->irq_bit) > 0)
    {
        //edet->fixed_out_ptr.IRQ_ptr.eeval1_max_evt = 0;
        //edet->fixed_out_ptr.IRQ_ptr.eeval2_max_evt = 0;
        //showing results in float
        //float mstsf = (float)ms_ts;
        //printf("EVT detected type:%d,ms_ts:%f\n", edet->fixed_out_ptr.event_identification, mstsf);
        //printf("evttype:%d,AMPD:%d msts:%d\n", (adv_EEVAL->data_packet & 3221225472) >> 30,(adv_EEVAL->data_packet & 1073676288)>>16, adv_EEVAL->data_packet & 65535);
        printf("conf:%f,filt:%f msts:%f\n", adv_EEVAL->data_packet.conf, adv_EEVAL->data_packet.filt, adv_EEVAL->data_packet.time_stamp);
    }
    return 0;
}
