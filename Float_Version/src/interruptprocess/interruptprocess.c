﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "interruptprocess.h"

void interrupt_process(uint16_t* DSP_IRQ_STATUS)
{
	if (DSP_IRQ_STATUS[0] >0)
	printf("interrupt detected\n");
	//printf("DSP_IRQ_STATUS[0]:%d\n", DSP_IRQ_STATUS[0]&256);
	if ((DSP_IRQ_STATUS[0] & 1) == 1)
	{
		printf("eeval_tlh_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 2) == 2)
	{
		printf("eeval_thl_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 4) == 4)
	{
		printf("eeval_max_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 8 )== 8)
	{
		printf("eeval_min_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 16) == 16)
	{
		printf("eeval1_max_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 32) == 32)
	{
		printf("eeval2_max_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 64) == 64)
	{
		printf("rfm_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 128) == 128)
	{
		printf("rtm_evt\n");
	}
	if ((DSP_IRQ_STATUS[0] & 256) == 256)
	{
		printf("nfd_evt\n");
	}
	DSP_IRQ_STATUS[0] = 0;
}