﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#ifndef _DSP_interruptprocess_H_
#define _DSP_interruptprocess_H_
#include "../../include/dsp_algo_api.h"
#ifdef __cplusplus
extern "C" {
#endif

void interrupt_process(u16_t* DSP_IRQ_STATUS);

#ifdef __cplusplus
}
#endif
#endif //_DSP_NFD_H_