﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: sunzhongxiang@eswincomputing.com
#ifndef _FTC_API_H_
#define _FTC_API_H_
#include "../../include/dsp_algo_api.h"
#include "../../include/dsp_dsptypes.h"
#ifdef __cplusplus 
extern "C" {
#endif

void ftc_init_api(FTC_PARAM* ftc, DspConfig* dsp_cfg);
errorReturn ftc_process_api(FTC_PARAM* ftc, FtcIO* ftc_io);
void ftc_destroy_api(FTC_PARAM* ftc);

#ifdef __cplusplus 
}
#endif
#endif 
