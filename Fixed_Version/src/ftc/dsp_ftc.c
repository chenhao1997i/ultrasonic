﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: sunzhongxiang@eswincomputing.com
#include "dsp_ftc.h"
#include "../../include/dsp_dsptypes.h"


void* ftc_init(FTC_PARAM* ftc, DspConfig* dsp_cfg)
{
	ftc->ftc_en = (dsp_cfg->FTC_CTRL&8)>>3;
	ftc->ftc_cfg = (dsp_cfg->FTC_CTRL&7);
	ftc->diff_coeff=diff_coeff_list[ftc->ftc_cfg];
	ftc->out_coeff=out_coeff_list[ftc->ftc_cfg];
	ftc->ls_ENVP = 0;
	ftc->ls_ftc_out = 0;
	//printf("init:%d %d\n", ftc->filterparam1.coeff_a[0], ftc->filterparam1.coeff_a[1]);
	//printf("%d\n", (-128) >> 3);
	return ftc;
}


u1_t ftc_process(FTC_PARAM* ftc, FtcIO* ftc_io)
{
	if ((ftc->ftc_en) && (ftc_io->ENVP_ENV_RAW<65535))
	{
		s32_t diff= ftc_io->ENVP_ENV_RAW - ftc->ls_ENVP;
		s32_t ftc_out = ((ftc->diff_coeff*diff) + (ftc->out_coeff*ftc->ls_ftc_out)) >> Coeff_SCALE;
		if (ftc_out > 65535)
			ftc_out = 65535;
		else if (ftc_out < 0)
			ftc_out = 0;
		ftc_io->ENV_FTC = ftc_out;
	}
	else
		ftc_io->ENV_FTC = ftc_io->ENVP_ENV_RAW;//ignore saturated input
	ftc->ls_ENVP=ftc_io->ENVP_ENV_RAW;
	ftc->ls_ftc_out = ftc_io->ENV_FTC;
	return 0;
}

void ftc_destroy(FTC_PARAM* ftc)
{
	free(ftc);
}