﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: sunzhongxiang@eswincomputing.com
#ifndef _DSP_FTC_H_
#define _DSP_FTC_H_
#include "../../include/dsp_public.h"
#include "../../include/dsp_dsptypes.h"
#ifdef __cplusplus
extern "C" {
#endif



static u9_t diff_coeff_list[8] =
{256,256,256,256,307,307,307,358 };
static u8_t out_coeff_list[8] =
{ 128,128,128,204,230,243,247,251};



typedef struct
{
	u1_t ftc_en;
	u3_t ftc_cfg;
	u9_t diff_coeff;
	u8_t out_coeff;
	u16_t ls_ftc_out;
	u16_t ls_ENVP;
}FTC_PARAM;



void* ftc_init(FTC_PARAM* ftc, DspConfig* dsp_cfg);
u1_t ftc_process(FTC_PARAM* ftc, FtcIO* ftc_io);
void ftc_destroy(FTC_PARAM* ftc);

#ifdef __cplusplus
}
#endif
#endif //_DSP_FTC_H_