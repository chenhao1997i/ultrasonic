﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: sunzhongxiang@eswincomputing.com
#include "dsp_ftc_api.h"
#include "dsp_ftc.h"
#include "../../include/dsp_dsptypes.h"
void ftc_init_api(FTC_PARAM* ftc, DspConfig* dsp_cfg)
{
	ftc_init(ftc, dsp_cfg);
}

errorReturn ftc_process_api(FTC_PARAM* ftc, FtcIO* ftc_io)
{
	u1_t ret = ftc_process(ftc, ftc_io);
	if (ret != 0)
		return errorFtc;
}

void ftc_destroy_api(FTC_PARAM* ftc)
{
	ftc_destroy(ftc);
}