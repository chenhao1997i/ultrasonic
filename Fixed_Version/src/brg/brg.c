﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: sunzhongxiang@eswincomputing.com
#include "brg.h"

void* brg_init(BrgConfig_t* init_cfg)
{
	void* ptr = NULL;
	ptr = (void*)calloc(1, sizeof(objBrg));
	objBrg* ptr_brg;
	ptr_brg = (objBrg*)ptr;
	ptr_brg->f_clk = init_cfg->f_clk;
	ptr_brg->T = init_cfg->T;  //ms
	ptr_brg->len = ptr_brg->T * (10e-3) * ptr_brg->f_clk;
	ptr_brg->send_over_flag = 0;
	ptr_brg->cnt = 0;
	ptr_brg->pulse_cnt = 0;
	ptr_brg->code_cnt = 0;
	ptr_brg->cnt_freq = 0;
	ptr_brg->BRG_CFG.burst_type = init_cfg->burst_type;
	ptr_brg->BRG_CFG.n_pulses = init_cfg->n_pulses;
	ptr_brg->BRG_FC.f_c = init_cfg->f_c;
	ptr_brg->BRG_F0.f_0 = init_cfg->f_0;
	ptr_brg->BRG_F1.f_1 = init_cfg->f_1;
	ptr_brg->BRG_FDELTA.f_delta = init_cfg->f_delta;
	ptr_brg->BRG_CFG.sel_inc = init_cfg->sel_inc;
	ptr_brg->BRG_CODE.code_len = init_cfg->code_len;
	ptr_brg->BRG_CODE.code = (char*)malloc(sizeof(char) * ptr_brg->BRG_CODE.code_len);
	memcpy(ptr_brg->BRG_CODE.code, init_cfg->code, sizeof(char) * ptr_brg->BRG_CODE.code_len);
	ptr_brg->burst_en_list = (int16_t*)calloc(ptr_brg->len, sizeof(int16_t));
	ptr_brg->burst_en_list[99] = 1;
	ptr_brg->burst_brk_list = (int16_t*)calloc(ptr_brg->len, sizeof(int16_t));
	
	return ptr;
}

brg_status_t  brg_deinit(void* instance)
{
	if (instance == NULL)
		return ERROR_BRG_PROCESS;
	objBrg* ptr_brg = (objBrg*)instance;
	free(ptr_brg->BRG_CODE.code);
	free(ptr_brg->burst_en_list);
	free(ptr_brg->burst_brk_list);
	free(ptr_brg);
}

brg_status_t mode_select(void* instance, BrgRuntimeConfig_t* rtCfg)
{
	if (instance == NULL)
		return ERROR_BRG_PROCESS;
	objBrg* ptr_brg = (objBrg*)instance;
	rtCfg->freq_current = 0;
	if (ptr_brg->BRG_CFG.burst_type == 0)
	{
		if (rtCfg->burst_sta == 1)
		{
			rtCfg->freq_current = ptr_brg->BRG_F0.f_0;
			if (ptr_brg->cnt == round((float)(ptr_brg->f_clk / ptr_brg->BRG_F0.f_0) * ptr_brg->BRG_CFG.n_pulses) - 1)
			{
				ptr_brg->send_over_flag = 1;
				return BURST_SEND_OVER;
			}
			else
				ptr_brg->cnt += 1;
		}
	}
	else if (ptr_brg->BRG_CFG.burst_type == 1)
	{
		if (rtCfg->burst_sta == 1)
		{
			rtCfg->code_current = ptr_brg->BRG_CODE.code[ptr_brg->BRG_CODE.code_len - ptr_brg->code_cnt - 1];
			if (rtCfg->code_current == '1')
				rtCfg->freq_current = ptr_brg->BRG_F0.f_0 + ptr_brg->pulse_cnt * ptr_brg->BRG_FDELTA.f_delta;
			else
				rtCfg->freq_current = ptr_brg->BRG_F1.f_1 - ptr_brg->pulse_cnt * ptr_brg->BRG_FDELTA.f_delta;
			//counter
			if (ptr_brg->cnt == round(ptr_brg->f_clk / rtCfg->freq_current) - 1)
			{
				if (ptr_brg->pulse_cnt == ptr_brg->BRG_CFG.n_pulses - 1)
				{
					if (ptr_brg->code_cnt == ptr_brg->BRG_CODE.code_len - 1)
					{
						ptr_brg->code_cnt = 0;
						ptr_brg->send_over_flag = 1;
						return BURST_SEND_OVER;
					}
					else
						ptr_brg->code_cnt += 1;
					ptr_brg->pulse_cnt = 0;
				}
				else
					ptr_brg->pulse_cnt += 1;
				ptr_brg->cnt = 0;
			}
			else
				ptr_brg->cnt += 1;
		}
	}
	else if (ptr_brg->BRG_CFG.burst_type == 2)
	{
		if (rtCfg->burst_sta == 1)
		{
			rtCfg->code_current = ptr_brg->BRG_CODE.code[ptr_brg->BRG_CODE.code_len - ptr_brg->code_cnt - 1];
			if (rtCfg->code_current == '0')
				rtCfg->freq_current = ptr_brg->BRG_F0.f_0;
			else
				rtCfg->freq_current = ptr_brg->BRG_F1.f_1;
			//counter
			if (ptr_brg->BRG_CFG.sel_inc == 0)
				rtCfg->cnt_num = round((float)ptr_brg->f_clk / rtCfg->freq_current * ptr_brg->BRG_CFG.n_pulses);
			else if (ptr_brg->BRG_CFG.sel_inc == 1)
				rtCfg->cnt_num = round((float)ptr_brg->f_clk / ptr_brg->BRG_FC.f_c * ptr_brg->BRG_CFG.n_pulses);
			if (ptr_brg->cnt == rtCfg->cnt_num - 1)
			{
				if (ptr_brg->code_cnt == ptr_brg->BRG_CODE.code_len - 1)
				{
					ptr_brg->code_cnt = 0;
					ptr_brg->send_over_flag = 1;
					return BURST_SEND_OVER;
				}
				else
					ptr_brg->code_cnt += 1;
				ptr_brg->cnt = 0;
			}
			else
				ptr_brg->cnt += 1;
		}
	}
	else if (ptr_brg->BRG_CFG.burst_type == 3)
	{
		if (rtCfg->burst_sta == 1)
		{
			rtCfg->code_current = ptr_brg->BRG_CODE.code[ptr_brg->BRG_CODE.code_len - ptr_brg->code_cnt - 1];
			if (rtCfg->code_current == '0')
				rtCfg->freq_current = ptr_brg->BRG_F0.f_0;
			else
				rtCfg->freq_current = ptr_brg->BRG_F1.f_1;
			//counter
			rtCfg->cnt_num = round((float)ptr_brg->f_clk / ptr_brg->BRG_FC.f_c * ptr_brg->BRG_CFG.n_pulses);
			if (ptr_brg->cnt == rtCfg->cnt_num - 1)
			{
				if (ptr_brg->code_cnt == ptr_brg->BRG_CODE.code_len - 1)
				{
					ptr_brg->code_cnt = 0;
					ptr_brg->send_over_flag = 1;
					return BURST_SEND_OVER;
				}
				else
					ptr_brg->code_cnt += 1;
				ptr_brg->cnt = 0;
			}
			else
				ptr_brg->cnt += 1;
		}
	}

	return 0;
}

brg_status_t clock_gen(void* instance, BrgRuntimeConfig_t* rtCfg, BrgData_t* output)
{
	if (instance == NULL)
		return ERROR_BRG_PROCESS;
	objBrg* ptr_brg = (objBrg*)instance;
	output->drv_en = (rtCfg->burst_sta == 1);
	output->drv1_en = 0;
	output->drv2_en = 0;
	if (output->drv_en == 1)
	{
		if (ptr_brg->cnt_freq < pow(2, 21))
			output->drv1_en = 0;
		else
			output->drv1_en = 1;
		output->drv2_en = 1 - output->drv1_en;
		rtCfg->freq_target = round(rtCfg->freq_current / ptr_brg->f_clk * pow(2, 22));
		ptr_brg->cnt_freq += rtCfg->freq_target;
		if (ptr_brg->cnt_freq >= pow(2, 22))
			ptr_brg->cnt_freq -= pow(2, 22);
	}

	return 0;
}


brg_status_t brg_get_one_frame(void* instance, BrgRuntimeConfig_t* rtCfg, BrgData_t* output)
{
	if (instance == NULL)
		return ERROR_BRG_PROCESS;
	objBrg* ptr_brg = (objBrg*)instance;
	rtCfg->burst_en = ptr_brg->burst_en_list[rtCfg->snap_num];
	rtCfg->burst_brk = ptr_brg->burst_brk_list[rtCfg->snap_num];
	if (rtCfg->burst_en)
		rtCfg->burst_sta = 1;
	else if (rtCfg->burst_sta == 1 && rtCfg->burst_brk == 1)
	{
		rtCfg->burst_sta = 2;
		return BURST_BRK;
	}
	else if (rtCfg->burst_sta == 1 && ptr_brg->send_over_flag && rtCfg->burst_brk != 1)
		rtCfg->burst_sta = 0;
	mode_select(instance, rtCfg);
	clock_gen(instance, rtCfg, output);
	
	return OK_BRG_PROCESS;
}

