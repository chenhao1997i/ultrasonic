﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com
#include "../include/dsp_algo_api.h"
#include "../src/envp/dsp_envp_api.h"
#include "../src/edet/dsp_edet_api.h"
#include "../src/ampd/dsp_ampd_api.h"
#include "../src/nfd/dsp_nfd_api.h"
#include "../src/ftc/dsp_ftc_api.h"
#include "../src/RFM/dsp_rfm_api.h"
#include "../src/interruptprocess/interruptprocess.h"
void dsp_init_block_api(DspParam* inst, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output, int* mod_switch)
{
	//inst->ptr_envp = NULL;
	////inst->ptr_edet = NULL;
	//inst->ptr_ampd = NULL;
	//inst->ptr_ftc = NULL;
	//inst->ptr_nfd = NULL;
	//inst->ptr_nfd2 = NULL;
	inst->envp_key = mod_switch[0];
	inst->ampd_key = mod_switch[1];
	inst->edet_key = mod_switch[2];
	inst->nfd_key = mod_switch[3];
	inst->rfm_key = mod_switch[4];
	

	if (inst->envp_key == 1)
		envp_init_api(&inst->envp, sample_rate, dsp_cfg, input_output);
	ftc_init_api(&inst->ftc, dsp_cfg);
	if (inst->ampd_key == 1)
		ampd_init_api(&inst->ampd, dsp_cfg);
	if (inst->edet_key == 1)
		edet_init_api(&inst->edet,dsp_cfg);
	if (inst->nfd_key == 1)
		nfd_init_api(&inst->nfd, dsp_cfg);
	if(inst->rfm_key==1)
		 rfm_init_api(&inst->rfm, dsp_cfg);
}

int16_t dsp_process_block_api(DspParam *inst, u16_t in_data, DspInputOutput* input_output, DspConfig* dsp_cfg) 
{
	if (dsp_cfg->fix_path_process)
	{
		//rfm
		if (inst->rfm_key == 1)
		{//printf("adc %d\t", in_data);
			input_output->rfm_io.adc_dta = in_data;
			input_output->rfm_io.burst_sta = 0;
			rfm_process_api(&inst->rfm, &input_output->rfm_io, &input_output->DSP_IRQ_STATUS);
		}
		//envp
		if (inst->envp_key)
		{
			envp_process_api(&inst->envp, in_data, input_output, dsp_cfg);
			if (input_output->frame_num > input_output->envpio.delay * dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1) && input_output->frame_num % (dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1)) == 0)
			{
				//ftc
				input_output->ftc_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
				ftc_process_api(&inst->ftc, &input_output->ftc_io);
				//printf("%d, %d  \n", input_output->envpio.ENVP_ENV_RAW, input_output->ENV_FTC);
				if (inst->ampd_key == 1)
				{
					input_output->ampd_io.ms_ts = input_output->ms_ts;
					input_output->ampd_io.ENV_FTC = input_output->ftc_io.ENV_FTC;
					//set STC_TB for AMPD
					ampd_process_api(&inst->ampd, &input_output->ampd_io);
				}
				else
					input_output->ampd_io.AMPD_ENV = input_output->ftc_io.ENV_FTC;
				//printf("%d  ", input_output->ampd_io.AMPD_ENV);
				if (inst->edet_key == 1)
				{
					input_output->edet_io.AMPD_ENV = input_output->ampd_io.AMPD_ENV;
					input_output->edet_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
					input_output->edet_io.ms_ts = input_output->ms_ts;
					//printf("ms_ts: %f\n", input_output->ENVP_ENV_RAW_point);
					edet_process_api(&inst->edet, &input_output->edet_io, input_output->DSP_IRQ_STATUS);
					//printf("%d %d %d %d %d\n", input_output->edet_io.ms_ts, input_output->edet_io.ENVP_ENV_RAW, input_output->edet_io.AMPD_ENV, input_output->edet_io.STG_TH, input_output->edet_io.atg_th);
					//printf("ms_ts:%d,AMPD:%d,ENVP_ENV_RAW:%d,STG:%d,ATG:%d\n", input_output->edet_io.ms_ts, input_output->edet_io.AMPD_ENV, input_output->edet_io.ENVP_ENV_RAW, input_output->edet_io.STG_TH, input_output->edet_io.atg_th);
				}
				if (inst->nfd_key == 1)
				{
					input_output->nfd_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
					input_output->nfd_io.ms_ts = input_output->ms_ts;
					input_output->nfd_io.rtm_rt_end = 1;
					input_output->nfd_io.burst_en = input_output->burst_en;
					nfd_process_api(&inst->nfd, &input_output->nfd_io, input_output->DSP_IRQ_STATUS);
				}
				/*if (inst->nfd_key == 2)
				{
					nfd2_process_api(&inst->nfd2, input_output->ms_ts, 1, input_output->envpio.ENVP_ENV_RAW, input_output->frame_oversize, input_output->envpio.delay);
				}*/

			}
		}
		else {
			input_output->envpio.ENVP_ENV_RAW = in_data;
			input_output->ftc_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
			//printf("%d\n", input_output->ftc_io.ENVP_ENV_RAW);
			//printf("framenum:%d ", input_output->frame_num+1);
			ftc_process_api(&inst->ftc, &input_output->ftc_io);
			//printf("%d\n",input_output->ftc_io.ENV_FTC);
			//printf("%d %d\n", input_output->ftc_io.ENVP_ENV_RAW, input_output->ftc_io.ENV_FTC);
			if (inst->ampd_key == 1)
			{
				input_output->ampd_io.ms_ts = input_output->ms_ts;
				input_output->ampd_io.ENV_FTC = input_output->ftc_io.ENV_FTC; 
				//printf("%d\t", input_output->ampd_io.ENV_FTC);
				ampd_process_api(&inst->ampd,  &input_output->ampd_io);
				//printf("%d \n", input_output->ampd_io.AMPD_ENV);
				////printf("%d %d\n", input_output->ampd_io.ms_ts, input_output->ampd_io.AMPD_ENV);
			}
			else
				input_output->ampd_io.AMPD_ENV = in_data;

			if (inst->edet_key == 1)
			{
				//printf("ms_ts:%d,AMPD:%d,ENVP_ENV_RAW:%d,STG:%d,ATG:%d\n", input_output->edet_io.ms_ts, input_output->edet_io.AMPD_ENV, input_output->edet_io.ENVP_ENV_RAW, input_output->edet_io.STG_TH, input_output->edet_io.atg_th);
				input_output->edet_io.AMPD_ENV = input_output->ampd_io.AMPD_ENV;
				input_output->edet_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
				input_output->edet_io.ms_ts = input_output->ms_ts;
				edet_process_api(&inst->edet, &input_output->edet_io, input_output->DSP_IRQ_STATUS);
				//printf("ms_ts:%d,AMPD:%d,ENVP_ENV_RAW:%d,STG:%d,ATG:%d\n", input_output->edet_io.ms_ts, input_output->edet_io.AMPD_ENV, input_output->edet_io.ENVP_ENV_RAW, input_output->edet_io.STG_TH, input_output->edet_io.atg_th);
			}

			if (inst->nfd_key == 1)
			{
				input_output->nfd_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
				input_output->nfd_io.ms_ts = input_output->ms_ts;
				input_output->nfd_io.rtm_rt_end = 1;
				input_output->nfd_io.burst_en = input_output->burst_en;
				nfd_process_api(&inst->nfd, &input_output->nfd_io, input_output->DSP_IRQ_STATUS);
			}
		}
	}
	else
	{
		if (inst->envp_key) {
			envp_process_api(&inst->envp, in_data, input_output, dsp_cfg);
			if (inst->edet_key == 1)
				if (input_output->frame_num > input_output->envpio.delay * dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1) && input_output->frame_num % (dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1)) == 0) {
					input_output->edet_io.ms_ts = input_output->ms_ts;
					//printf("%d\n", input_output->ms_ts);
					input_output->edet_io.filt1 = input_output->envpio.filt1;
					input_output->edet_io.filt2 = input_output->envpio.filt2;
					input_output->edet_io.conf1 = input_output->envpio.conf1;
					input_output->edet_io.conf2 = input_output->envpio.conf2;
					edet_process_api(&inst->edet, &input_output->edet_io, input_output->DSP_IRQ_STATUS);
				}
		}
		else {
			input_output->envpio.filt1 = in_data;
			input_output->edet_io.ms_ts = input_output->ms_ts;
			input_output->edet_io.filt1 = input_output->envpio.filt1;
			input_output->edet_io.filt2 = input_output->envpio.filt2;
			input_output->edet_io.conf1 = input_output->envpio.conf1;
			input_output->edet_io.conf2 = input_output->envpio.conf2;
			//printf("%d %d\n", input_output->edet_io.filt1, in_data);
			edet_process_api(&inst->edet, &input_output->edet_io, input_output->DSP_IRQ_STATUS);
			//printf("conf:%d,filt:%d msts:%d\n", (input_output->edet_io.adv_data_packet1>>32)&15, ((input_output->edet_io.adv_data_packet1 >>16)&65535), input_output->edet_io.adv_data_packet1 & 65535);
			//printf("%d %d\n", input_output->edet_io.aatg1_th, input_output->edet_io.aatg_filt1);
		}
	}
	interrupt_process(input_output->DSP_IRQ_STATUS);
	
	return 0;
}


void dsp_destroy_block_api(DspParam* inst, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	/*if (inst->envp_key == 1)
		envp_destroy_api(&inst->envp, input_output);
	ftc_destroy_api(&inst->ftc);
	if (inst->ampd_key == 1)
		ampd_destroy_api(&inst->ampd);
	if (inst->edet_key == 1)
		edet_destroy_api(&inst->edet);
	if (inst->nfd_key == 1)
		nfd_destroy_api(&inst->nfd);
	if (inst->rfm_key == 1)
		rfm_destroy_api(&inst->rfm);*/
}
