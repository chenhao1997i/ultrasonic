﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
u1_t AATG_process(FIXED_AATG* aatg, u16_t filt_fixed, u16_t conf)
{
    //EDET_PARAM* edet = (EDET_PARAM*)param_ptr;
//printf("%d\t", edet->aatg_buf_cnt);
    if (aatg->cell_cnt >= aatg->aatg_cw)
    {
        aatg->cell_cnt = 0;
        aatg->last_cell = aatg->aatg_buf[aatg->aatg_buf_cnt];//restore last cell for insert sort
        aatg->aatg_buf[aatg->aatg_buf_cnt] = 0;
        //printf("%d:%d\n", edet->frame_num, aatg->aatg_buf_cnt);
        u32_t temp_mul=0;
        u32_t temp_sum = 0;
        for (u16_t i = 0; i < aatg->aatg_cw; i++)//is there a better way to sum?
        {
            //aatg->aatg_buf[aatg->aatg_buf_cnt] = aatg->aatg_buf[aatg->aatg_buf_cnt] + aatg->cell_register[i] / aatg->aatg_cw;
            temp_mul = (aatg->cell_register[i] * aatg->rec_aatg_cw);//16bit*8bit
            //temp_shift= temp_mul >> rec_aatg_cw_SCALE;
            //aatg->aatg_buf[aatg->aatg_buf_cnt] = aatg->aatg_buf[aatg->aatg_buf_cnt]+temp_shift;
            temp_sum = temp_sum +temp_mul;//sum of 4-16 24bit num
        }
        aatg->aatg_buf[aatg->aatg_buf_cnt] = temp_sum >>rec_aatg_cw_SCALE;
        //printf("%f\n", (float)aatg->aatg_buf[aatg->aatg_buf_cnt]/(1<< AMPD_ENV_SCALE));
        //edet->aatg_buf[edet->aatg_buf_cnt] = edet->aatg_buf[edet->aatg_buf_cnt] / edet->aatg_cw;
        //GOSCA_CFAR_process(aatg);
        OS_CFAR_process(aatg);
        aatg->aatg_buf_cnt += 1;
    }
    aatg->cell_register[aatg->cell_cnt] = filt_fixed;
    if (aatg->aatg_buf_cnt >= aatg->aatg_cn)
    {
        aatg->aatg_buf_half = 1;
        if (aatg->aatg_buf_cnt >= aatg->aatg_buf_size)
        {
            aatg->aatg_buf_full = 1;
            aatg->aatg_buf_cnt = 0;
        }
    }
    aatg->current_filt = aatg->filt_buf[aatg->filt_cnt];
    aatg->current_conf = aatg->conf_buf[aatg->filt_cnt];
    aatg->filt_buf[aatg->filt_cnt] = filt_fixed;
    aatg->conf_buf[aatg->filt_cnt] = conf;
    aatg->cell_cnt += 1;
    aatg->filt_cnt += 1;
    if (aatg->filt_cnt >= aatg->filt_buf_size)
    {
        aatg->filt_cnt = 0;
    }
    return 0;
}




