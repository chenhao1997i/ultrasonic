﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
u1_t GOSCA_CFAR_process(FIXED_AATG* aatg)
{
    u8_t left_new_index = 0;
    if (aatg->aatg_buf_cnt >= aatg->aatg_cn)
    {
        left_new_index= aatg->aatg_buf_cnt- aatg->aatg_cn;
    }
    else
    {
        left_new_index = aatg->aatg_buf_cnt + aatg->aatg_cn;
    }
    insert_sort(aatg->L_sorted_cell_register, aatg->aatg_cn, aatg->last_cell, aatg->aatg_buf[left_new_index]);
    insert_sort(aatg->R_sorted_cell_register, aatg->aatg_cn, aatg->aatg_buf[left_new_index], aatg->aatg_buf[aatg->aatg_buf_cnt]);
    //aatg->aatg_th = (aatg->L_sorted_cell_register[aatg->Lk] + aatg->R_sorted_cell_register[aatg->Rk]) / 2 * aatg->aatg_alpha;
    aatg->aatg_th = aatg->L_sorted_cell_register[aatg->Lk] + aatg->R_sorted_cell_register[aatg->Rk];
    //aatg->aatg_th = aatg->aatg_th >> 1;
    aatg->aatg_th = (aatg->aatg_th * aatg->aatg_alpha)>>(aatg_alpha_SCALE+1);
    return 0;
}