﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
void* edet_init(EDET_PARAM* edet, DspConfig* dsp_cfg)
{
	//fixed atg
	u16_t fixed_taulist[] = { 16342,16321, 16300,16259,16217,16176, 16134,16134};//0.997444^{ 1, 1.5, 2, 3, 4, 5, 6, 6 }
    //u32_t fixed_alpha_list[] = { 23406, 29257,35109,40960,46811,52663,58514,70217 };//{ 2,2.5,3,3.5,4,4.5,5,6 }/1.4;
    u16_t fixed_alpha_list[] = { 11703, 14629,17554,20480,23406,26331,29257,35109 };//{ 2,2.5,3,3.5,4,4.5,5,6 }/1.4;
    //u32_t fixed_alpha_list[] = {4,5,6,7,8,9,10,12 };//{ 2,2.5,3,3.5,4,4.5,5,6 };
    u8_t fixed_inilist[] = { 0,32,64,80,96,112,128,144 };
    edet->fixed_freq.atg_tau = fixed_taulist[(dsp_cfg->ATG_CTRL&7)];
    edet->fixed_freq.atg_alpha = fixed_alpha_list[(dsp_cfg->ATG_CTRL & (7 << 3)) >> 3];// atg sensitivity configuration
    edet->fixed_freq.atg_ini = fixed_inilist[(dsp_cfg->ATG_CTRL & (7 << 6)) >> 6];
    edet->fixed_freq.atg_cfg = (dsp_cfg->ATG_CTRL & (1 << 9)) >> 9;
	edet->fixed_freq.tmp_atg_th = edet->fixed_freq.atg_ini*(1<<(atg_alpha_SCALE+atg_th_SCALE))/edet->fixed_freq.atg_alpha;//AMPD_ENV aren't fixed yet
	edet->fixed_freq.atg_th = edet->fixed_freq.atg_ini*(1<< atg_th_SCALE);
    //fixed EEVAL
    //edet->fixed_EEVAL = (FIXED_EEVAL*)calloc(1, sizeof(FIXED_EEVAL));
    edet->fixed_EEVAL.eeval_sel = (dsp_cfg->EEVAL_CTRL & 15);
    edet->fixed_EEVAL.eeval_sens = (dsp_cfg->EEVAL_CTRL & (7 << 4)) >> 4;
    edet->fixed_EEVAL.eeval_dma_val = (dsp_cfg->EEVAL_CTRL & (1 << 7)) >> 7;
    edet->fixed_EEVAL.ls_AMPD_ENV = 0;
    edet->fixed_EEVAL.ls_ms_ts = 0;
    edet->fixed_EEVAL.ls_threshold = 0;
    edet->fixed_EEVAL.ls_dma_val = 0;
    edet->fixed_EEVAL.threshold = 0;
    edet->fixed_EEVAL.negative_cnt = 0;
    edet->fixed_EEVAL.positive_cnt = 0;
    edet->fixed_EEVAL.data_packet = 0;

    u8_t fixed_aatg_cn_list[] = { 4,6,8,10,12,12,12,12 };
    u8_t fixed_aatg_cw_list[] = { 4,6,8,10,12,14,16,16 };
    u8_t rec_aatg_cw_list[] = { 64,43,32,26,21,18,16,16 };
    u8_t fixed_aatg_alpha_list[] = { 7,8,9,10 };
    //fixed aatg1
    edet->fixed_AATG1.aatg_th = 0;
    edet->fixed_AATG1.aatg_buf_full= (dsp_cfg->AATG1_CTRL & (1 << 9)) >> 9;
    edet->fixed_AATG1.aatg_buf_half = (dsp_cfg->AATG1_CTRL & (1 << 10)) >> 10;
    edet->fixed_AATG1.aatg_off = (dsp_cfg->AATG1_CTRL & (1 << 8)) >> 8;
    edet->fixed_AATG1.aatg_buf_cnt=0;
    edet->fixed_AATG1.aatg_cn = fixed_aatg_cn_list[(dsp_cfg->AATG1_CTRL & 7)];
    edet->fixed_AATG1.aatg_cw = fixed_aatg_cw_list[(dsp_cfg->AATG1_CTRL & (7 << 3)) >> 3];
    edet->fixed_AATG1.rec_aatg_cw = rec_aatg_cw_list[(dsp_cfg->AATG1_CTRL & (7 << 3)) >> 3];
    edet->fixed_AATG1.aatg_alpha = fixed_aatg_alpha_list[(dsp_cfg->AATG1_CTRL & (3 << 6)) >> 6];
    //printf("%d:%d:%d\n", edet->fixed_AATG1.aatg_cn, edet->fixed_AATG1.aatg_cw, edet->fixed_AATG1.aatg_alpha);
    edet->fixed_AATG1.k = edet->fixed_AATG1.aatg_cn + 1;
    edet->fixed_AATG1.Lk = edet->fixed_AATG1.aatg_cn/2;
    edet->fixed_AATG1.Rk = edet->fixed_AATG1.aatg_cn/2;
    edet->fixed_AATG1.aatg_buf_size = edet->fixed_AATG1.aatg_cn << 1;
    edet->fixed_AATG1.filt_buf_size = edet->fixed_AATG1.aatg_cw * (edet->fixed_AATG1.aatg_cn + 1);
    //printf("rec_aatg_cw=%d\n", edet->fixed_AATG1.rec_aatg_cw);
    //printf("aatg_cn=%d,aatg_cw=%d",edet->fixed_AATG1.aatg_cn,edet->fixed_AATG1.aatg_cw);
    edet->fixed_AATG1.current_filt= 0;
    edet->fixed_AATG1.current_conf = 0;
    edet->fixed_AATG1.cell_cnt = 0;
    edet->fixed_AATG1.filt_cnt = 0;
    edet->fixed_AATG1.last_cell = 0;
    for (u8_t i = 0;i < 2 * edet->fixed_AATG1.aatg_cn; i++)
    {
        edet->fixed_AATG1.sorted_cell_register[i] = 0;
        edet->fixed_AATG1.aatg_buf[i] = 0;
    }
    for (u8_t i = 0; i < edet->fixed_AATG1.aatg_cw; i++)
    {
        edet->fixed_AATG1.cell_register[i] = 0;
    }
    for (u8_t i = 0; i < edet->fixed_AATG1.aatg_cw * (edet->fixed_AATG1.aatg_cn + 1); i++)
    {
        edet->fixed_AATG1.filt_buf[i] = 0;
        edet->fixed_AATG1.conf_buf[i] = 0;
    }
    //adv EEVAL1
    edet->adv_EEVAL1.eeval_sel = (dsp_cfg->EEVAL_CTRL & 15);
    edet->adv_EEVAL1.eeval_sens = (dsp_cfg->EEVAL_CTRL & (7 << 4)) >> 4;
    edet->adv_EEVAL1.ls_filt = 0;
    edet->adv_EEVAL1.ls_conf = 0;
    edet->adv_EEVAL1.ls_ms_ts = 0;
    edet->adv_EEVAL1.threshold = 0;
    edet->adv_EEVAL1.positive_cnt = 0;
    edet->adv_EEVAL1.data_packet = 0;
    edet->adv_EEVAL1.irq_bit = 16;

    //fixed aatg2
    edet->fixed_AATG2.aatg_th = 0;
    edet->fixed_AATG2.aatg_buf_full = (dsp_cfg->AATG2_CTRL & (1 << 9)) >> 9;
    edet->fixed_AATG2.aatg_buf_half = (dsp_cfg->AATG2_CTRL & (1 << 10)) >> 10;
    edet->fixed_AATG2.aatg_off = (dsp_cfg->AATG2_CTRL & (1 << 8)) >> 8;
    edet->fixed_AATG2.aatg_buf_cnt = 0;
    edet->fixed_AATG2.aatg_cn = fixed_aatg_cn_list[(dsp_cfg->AATG2_CTRL & 7)];
    edet->fixed_AATG2.aatg_cw = fixed_aatg_cw_list[(dsp_cfg->AATG2_CTRL & (7 << 3)) >> 3];
    edet->fixed_AATG2.rec_aatg_cw = rec_aatg_cw_list[(dsp_cfg->AATG2_CTRL & (7 << 3)) >> 3];
    edet->fixed_AATG2.aatg_alpha = fixed_aatg_alpha_list[(dsp_cfg->AATG2_CTRL & (3 << 6)) >> 6];
    //printf("%d:%d:%d\n", edet->fixed_AATG2.aatg_cn, edet->fixed_AATG2.aatg_cw, edet->fixed_AATG2.aatg_alpha);
    edet->fixed_AATG2.k = edet->fixed_AATG2.aatg_cn + 1;
    edet->fixed_AATG2.Lk = edet->fixed_AATG2.aatg_cn / 2;
    edet->fixed_AATG2.Rk = edet->fixed_AATG2.aatg_cn / 2;
    edet->fixed_AATG2.aatg_buf_size = edet->fixed_AATG2.aatg_cn << 1;
    edet->fixed_AATG2.filt_buf_size = edet->fixed_AATG2.aatg_cw * (edet->fixed_AATG2.aatg_cn + 1);
    //printf("rec_aatg_cw=%d\n", edet->fixed_AATG2.rec_aatg_cw);
    //printf("aatg_cn=%d,aatg_cw=%d",edet->fixed_AATG2.aatg_cn,edet->fixed_AATG2.aatg_cw);
    edet->fixed_AATG2.current_filt = 0;
    edet->fixed_AATG2.current_conf = 0;
    edet->fixed_AATG2.cell_cnt = 0;
    edet->fixed_AATG2.filt_cnt = 0;
    edet->fixed_AATG2.last_cell = 0;
    for (u8_t i = 0; i < 2 * edet->fixed_AATG2.aatg_cn; i++)
    {
        edet->fixed_AATG2.sorted_cell_register[i] = 0;
        edet->fixed_AATG2.aatg_buf[i] = 0;
    }
    for (u8_t i = 0; i < edet->fixed_AATG2.aatg_cw; i++)
    {
        edet->fixed_AATG2.cell_register[i] = 0;
    }
    for (u8_t i = 0; i < edet->fixed_AATG2.aatg_cw * (edet->fixed_AATG2.aatg_cn + 1); i++)
    {
        edet->fixed_AATG2.filt_buf[i] = 0;
        edet->fixed_AATG2.conf_buf[i] = 0;
    }
    //adv EEVAL1
    edet->adv_EEVAL2.eeval_sel = (dsp_cfg->EEVAL_CTRL & 15);
    edet->adv_EEVAL2.eeval_sens = (dsp_cfg->EEVAL_CTRL & (7 << 4)) >> 4;
    edet->adv_EEVAL2.ls_filt = 0;
    edet->adv_EEVAL2.ls_conf = 0;
    edet->adv_EEVAL2.ls_ms_ts = 0;
    edet->adv_EEVAL2.threshold = 0;
    edet->adv_EEVAL2.positive_cnt = 0;
    edet->adv_EEVAL2.data_packet = 0;
    edet->adv_EEVAL2.irq_bit = 32;
	//edet->fixed_out_ptr.dma_val = 0;
	//edet->fixed_out_ptr.ms_ts = 0;
    return edet;
}

u1_t edet_process(EDET_PARAM* edet,EdetIO* edet_io, u16_t* DSP_IRQ_STATUS)
{
    //printf("ms_ts: %d: %d\n", edet_io->ms_ts, edet_io->filt1);
    u16_t AMPD_ENV_Q = (u16_t)(edet_io->AMPD_ENV * (1 << AMPD_ENV_SCALE));
    u16_t ENVP_ENV_RAW_Q = edet_io->ENVP_ENV_RAW ;
    //fixed-path
	/*quantified version*/
	ATG_process(& edet->fixed_freq, AMPD_ENV_Q);
	STG_process(edet, edet_io->ms_ts, edet_io);
    edet_io->atg_th = edet->fixed_freq.atg_th >> atg_th_SCALE;
	edet->fixed_EEVAL.threshold = MAX(edet_io->STG_TH, edet_io->atg_th);
    //printf("%d:%d:%d\n", edet->fixed_EEVAL.threshold>>8, edet->fixed_freq.stg_th, edet->fixed_freq.atg_th >> 8);
    //printf("%d:%d:%d\n", edet_io->STG_STEP, edet->fixed_freq.stg_th >> STG_TH_SCALE, edet_io->STG_TB);
    //printf("%d\n", edet->fixed_EEVAL.threshold>>8);
	EEVAL_process_q(& edet->fixed_EEVAL, edet_io->ms_ts, edet_io->AMPD_ENV, edet_io->ENVP_ENV_RAW, DSP_IRQ_STATUS);
    edet_io->std_data_packet= edet->fixed_EEVAL.data_packet;
    //advanced Path
    if(edet->fixed_AATG1.aatg_off)
        edet->fixed_AATG1.aatg_th=0;
    else
        AATG_process(& edet->fixed_AATG1, edet_io->filt1, edet_io->conf1);
    if (edet->fixed_AATG1.aatg_buf_full)
    {
        edet_io->aatg1_th = edet->fixed_AATG1.aatg_th;
        edet->adv_EEVAL1.threshold = edet->fixed_AATG1.aatg_th;
        //printf("f1:%d\n", edet->fixed_AATG1.aatg_th);
        adv_EEVAL_process(&edet->adv_EEVAL1, edet_io->ms_ts, edet->fixed_AATG1.current_filt, edet->fixed_AATG1.current_conf,DSP_IRQ_STATUS);
        //printf("%d %d %d\n", edet->fixed_AATG1.current_msts, edet->fixed_AATG1.current_filt, edet_io->aatg1_th);
    }
    if (edet->fixed_AATG2.aatg_off)
        edet->fixed_AATG2.aatg_th = 0;
    else
        AATG_process(&edet->fixed_AATG2, edet_io->filt2, edet_io->conf2); 
    if (edet->fixed_AATG2.aatg_buf_full)
    {
        edet_io->aatg2_th = edet->fixed_AATG2.aatg_th;
        edet->adv_EEVAL2.threshold = edet->fixed_AATG2.aatg_th;
        //printf("f2:%d\n", edet->fixed_AATG2.aatg_th);
        adv_EEVAL_process(&edet->adv_EEVAL2, edet_io->ms_ts, edet->fixed_AATG2.current_filt, edet->fixed_AATG2.current_conf, DSP_IRQ_STATUS);

    }
    //printf("%d %f %f %f %f\n", edet->fixed_AATG1.current_msts, (float)edet->fixed_AATG1.current_filt/(1.0 * (1 << 16)), (float)edet->fixed_AATG1.aatg_th / (1.0 * (1 << 16)), (float)edet->fixed_AATG2.current_filt / (1.0 * (1 << 16)), (float)edet->fixed_AATG2.aatg_th / (1.0 * (1 << 16)));
    //printf("%d %d %d\n", edet->fixed_AATG1.current_msts, edet->fixed_AATG1.current_filt, edet_io->aatg1_th);
    edet_io->adv_data_packet2 = edet->adv_EEVAL2.data_packet;
    edet_io->adv_data_packet1 = edet->adv_EEVAL1.data_packet;
    edet_io->aatg_filt1= edet->fixed_AATG1.current_filt;
    edet_io->aatg_filt2 = edet->fixed_AATG2.current_filt;
    edet_io->aatg_conf1 = edet->fixed_AATG1.current_conf;
    edet_io->aatg_conf2 = edet->fixed_AATG2.current_conf;
    return 0;
}

void edet_destroy(EDET_PARAM* edet)
{
    free (edet);
}

