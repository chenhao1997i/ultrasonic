﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#ifndef _DSP_EDET_H_
#define _DSP_EDET_H_
#include "../../include/dsp_public.h"
#include "../../include/dsp_dsptypes.h"
#ifdef __cplusplus
extern "C" {
#endif



typedef struct
{
	u8_t atg_cfg;
	u16_t atg_ini;
    u16_t atg_tau;
    u32_t atg_alpha;
    u32_t tmp_atg_th;
    u32_t atg_th;
} FIXED_FREQ;

typedef struct
{
u8_t eeval_sel;
u8_t eeval_sens;
u8_t eeval_dma_val;
u16_t positive_cnt;
u16_t negative_cnt;
u32_t threshold;
u16_t ls_ms_ts;// last ms_ts
u32_t ls_dma_val;// last dma_val
u16_t ls_AMPD_ENV;// last AMPD_ENV
u32_t ls_threshold;// threshold
u32_t data_packet;
} FIXED_EEVAL;

typedef struct
{
    u8_t irq_bit;
    u8_t eeval_sel;
    u8_t eeval_sens;
    u16_t positive_cnt;
    u32_t threshold;
    u16_t ls_ms_ts;// last ms_ts
    u64_t ls_conf;//last conf
    u64_t ls_filt;// last filt
    //u32_t ls_threshold;// threshold
    u64_t data_packet;//only 36 bit is used
} ADV_EEVAL;

typedef struct
{
    u1_t aatg_buf_full;
    u1_t aatg_buf_half;
    u1_t aatg_off;
    u16_t aatg_buf_cnt;
    u8_t aatg_alpha;
    u8_t aatg_cw;
    u8_t rec_aatg_cw;//reciprocal of aatg_cw
    u8_t aatg_cn;
    u16_t cell_cnt;
    u16_t filt_cnt;
    u32_t aatg_th;
    u8_t k;
    u16_t aatg_buf[24];
    u16_t last_cell;
    u8_t aatg_buf_size;
    u16_t cell_register[16];
    u16_t sorted_cell_register[24];
    u32_t filt_buf[208];
    u32_t conf_buf[208];
    u8_t filt_buf_size;
    u32_t current_filt;
    u32_t current_conf;
    // GOSCA_CFAR
    u8_t Lk;
    u8_t Rk;
    u16_t L_sorted_cell_register[12];
    u16_t R_sorted_cell_register[12];
} FIXED_AATG;

typedef struct
{
	FIXED_FREQ fixed_freq;
	FIXED_EEVAL fixed_EEVAL;
    FIXED_AATG fixed_AATG1;
    ADV_EEVAL adv_EEVAL1;
    FIXED_AATG fixed_AATG2;
    ADV_EEVAL adv_EEVAL2;
} EDET_PARAM;

void* edet_init(EDET_PARAM* edet, DspConfig* dsp_cfg);
u1_t edet_process(EDET_PARAM* edet,EdetIO* edet_io, u16_t* DSP_IRQ_STATUS);
void edet_destroy(EDET_PARAM* edet);
u1_t ATG_process(FIXED_FREQ* edet, u16_t AMPD_ENV_Q);
u1_t STG_process(EDET_PARAM* edet, u32_t ms_ts_fixed, EdetIO* edet_io);
u1_t AATG_process(FIXED_AATG* aatg, u16_t filt_fixed, u16_t ENVP_ENV_RAW_Q);
u1_t OS_CFAR_process(FIXED_AATG* aatg);
u1_t GOSCA_CFAR_process(FIXED_AATG* aatg);
void insert_sort(u16_t* a, u8_t len, u16_t old_element, u16_t new_element); //ascending sort
u1_t EEVAL_process_q(FIXED_EEVAL* edet, u16_t ms_ts_fixed, u16_t AMPD_ENV_Q, u16_t ENVP_ENV_RAW, u16_t* DSP_IRQ_STATUS);
u1_t adv_EEVAL_process(ADV_EEVAL* adv_EEVAL, u16_t ms_ts, u16_t filt, u8_t conf, u16_t* DSP_IRQ_STATUS);

#ifdef __cplusplus
}
#endif
#endif//_DSP_EDET_H_