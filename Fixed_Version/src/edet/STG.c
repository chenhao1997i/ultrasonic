﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"

u1_t STG_process(EDET_PARAM* edet, u32_t ms_ts_fixed, EdetIO* edet_io)
{
    //printf("tmp_atg_th: %f , ms_ts: %f  ,atg_th: %f  ,atg_alpha:%d  ,atg_tau:%d  ,AMPD_ENV_Q:%d  \n", (float)edet->fixed_atg->tmp_atg_th/256.0f, ms_ts,(float)edet->fixed_atg->atg_th/256.0f , edet->fixed_atg->atg_alpha, edet->fixed_atg->atg_tau, AMPD_ENV_Q );
    //printf("atg_th: %f , ms_ts: %f  ,stg_th: %f \n", (float)edet->fixed_atg->atg_th / 256.0f, (float)ms_ts_fixed/(1<< ms_ts_SCALE), (float)edet->fixed_atg->stg_th / 256.0f);
	//printf("stg_TB: %d , ms_ts: %f  ,stg_TH: %d \n", STG_TB_Q>>STG_TB_SCALE, (float)ms_ts_fixed / (1 << ms_ts_SCALE), STG_TH_Q>>STG_TH_SCALE);
    // STG
    if (edet_io->STG_TB != 0)
    {
        u32_t ms_ts_fixed_temp = ms_ts_fixed / edet_io->STG_TB;
        u32_t ls_ms_ts_fixed_temp = edet->fixed_EEVAL.ls_ms_ts / edet_io->STG_TB;
        s16_t stg_add = (ms_ts_fixed_temp - ls_ms_ts_fixed_temp) * edet_io->STG_STEP;
        //printf("add:%d\n",stg_add);
        s16_t temp_stg_th = edet_io->STG_TH + stg_add;
        if (temp_stg_th < 0)
            edet_io->STG_TH = 0;
        else if (temp_stg_th > 255)
            edet_io->STG_TH = 255;
        else
            edet_io->STG_TH = temp_stg_th;
    }
    //printf("\n");
    //printf(" AMPD_ENV_Q:%d  edet->threshold: %f  stg_th_q: %d  atg_th_q: %d  \n", AMPD_ENV_Q >> 8, edet->threshold, edet->fixed_atg->stg_th>>8, edet->fixed_atg->atg_th>>8);
    return 0;
}