﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
u1_t EEVAL_process_q(FIXED_EEVAL* fixed_EEVAL, u16_t ms_ts, u16_t AMPD_ENV, u16_t ENVP_ENV_RAW, u16_t* DSP_IRQ_STATUS)
{
	// EEVAL
	u8_t maximum_detc = 0;
	u8_t minimum_detc = 0;
	u32_t dma_val = (ENVP_ENV_RAW&65532)<<14;
	if (fixed_EEVAL->eeval_dma_val)
	{
		dma_val = AMPD_ENV<<16;//only used the first 8 bits of AMPD_ENV in DMA.therefore ,there is no need to shift 8 bits to right when printing.
	}
	fixed_EEVAL->data_packet = dma_val;
	//fixed_EEVAL->data_packet = 0;
	if (fixed_EEVAL->ls_AMPD_ENV < AMPD_ENV)
	{
		fixed_EEVAL->positive_cnt += 1;
		if (fixed_EEVAL->negative_cnt > fixed_EEVAL->eeval_sens)
		{
			//MIN_EVT minimum on ENVP_ENV_RAW detected
			minimum_detc = 1;
		}
		fixed_EEVAL->negative_cnt = 0;
	}
	if (fixed_EEVAL->ls_AMPD_ENV > AMPD_ENV)
	{
		fixed_EEVAL->negative_cnt += 1;
		if (fixed_EEVAL->positive_cnt > fixed_EEVAL->eeval_sens)
		{
			//MAX_EVT maximum on ENVP_ENV_RAW detected
			maximum_detc = 1;
		}
		fixed_EEVAL->positive_cnt = 0;
	}
	//printf("%d:%f:%f:%f\n", edet->frame_num, (float)ms_ts, (float)AMPD_ENV / (1 << AMPD_ENV_SCALE), (float)fixed_EEVAL->threshold / (1 << AMPD_ENV_SCALE));
	//printf("%d:%f:%f:%f\n", edet->frame_num, (float)ms_ts, (float)AMPD_ENV / (1 << AMPD_ENV_SCALE), (float)edet->fixed_out_ptr.stg_th/ (1 << AMPD_ENV_SCALE));
	if (AMPD_ENV >= fixed_EEVAL->threshold)
	{
		//printf("%d:%d:%d:%d:%d\n", fixed_EEVAL->eeval_sel, fixed_EEVAL->eeval_sel & 1 == 1, (fixed_EEVAL->eeval_sel & 8)==8 , fixed_EEVAL->eeval_sel & 4 == 4, fixed_EEVAL->eeval_sel & 2 == 2);
		if ((fixed_EEVAL->ls_AMPD_ENV < fixed_EEVAL->ls_threshold) && ((fixed_EEVAL->eeval_sel&1)==1))
		{
			//TLH_EVT positive threshold crossing
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0]|1;
			//edet->fixed_out_ptr.IRQ_ptr.eeval_tlh_evt = 1;
			fixed_EEVAL->data_packet+= ms_ts;
		}
		if (minimum_detc && ((fixed_EEVAL->eeval_sel&8) == 8))
		{
			//MIN_EVT minimum on ENVP_ENV_RAW detected
			//edet->fixed_out_ptr.IRQ_ptr.eeval_min_evt = 1;
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 8;
			fixed_EEVAL->data_packet = fixed_EEVAL->ls_dma_val;
			fixed_EEVAL->data_packet+= fixed_EEVAL->ls_ms_ts +(2<<30);
		}
		if (maximum_detc && ((fixed_EEVAL->eeval_sel&4)== 4))
		{
			//MAX_EVT maximum on ENVP_ENV_RAW detected
			//edet->fixed_out_ptr.IRQ_ptr.eeval_max_evt = 1;
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 4;
			fixed_EEVAL->data_packet = fixed_EEVAL->ls_dma_val;
			fixed_EEVAL->data_packet+= fixed_EEVAL->ls_ms_ts+ (3<<30);
		}
	}
	else
	{
		if ((fixed_EEVAL->ls_AMPD_ENV >= fixed_EEVAL->ls_threshold) && ((fixed_EEVAL->eeval_sel&2)== 2))
		{
			//THL_EVT negative threshold crossing
			DSP_IRQ_STATUS[0] = DSP_IRQ_STATUS[0] | 2;
			//edet->fixed_out_ptr.IRQ_ptr.eeval_thl_evt = 1;
			fixed_EEVAL->data_packet+= ms_ts + (1<<30);
		}
	}
	fixed_EEVAL->ls_ms_ts = ms_ts;
	fixed_EEVAL->ls_dma_val = dma_val;
	fixed_EEVAL->ls_AMPD_ENV = AMPD_ENV;
	fixed_EEVAL->ls_threshold = fixed_EEVAL->threshold;
	//edet_result
	//if ((edet->fixed_out_ptr.IRQ_ptr.eeval_tlh_evt) || (edet->fixed_out_ptr.IRQ_ptr.eeval_thl_evt) || (edet->fixed_out_ptr.IRQ_ptr.eeval_max_evt) || (edet->fixed_out_ptr.IRQ_ptr.eeval_min_evt))
	if ((DSP_IRQ_STATUS[0] & 15) > 0)
	{
		//showing results in float
		//float mstsf = (float)ms_ts;
		//printf("evttype:%d,AMPD:%d msts:%d\n", (u16_t)((fixed_EEVAL->data_packet & (3<<30))>> 30), (u16_t)((fixed_EEVAL->data_packet & (16383<<16))>> 16), (u16_t)(fixed_EEVAL->data_packet & 65535));
		//printf("evttype:%d,AMPD:%d msts:%d, %fcm\n", (u16_t)((fixed_EEVAL->data_packet & (3<<30))>> 30), (u16_t)((fixed_EEVAL->data_packet & (16383<<16))>> 16), (u16_t)(fixed_EEVAL->data_packet & 65535),(float)(fixed_EEVAL->data_packet & 65535)*0.017);
		//printf("msts: %d\n", ms_ts);
	}
	//fixed_EEVAL->data_packet = 0;
	return 0;
}

