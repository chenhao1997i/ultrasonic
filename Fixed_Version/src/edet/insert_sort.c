﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
void insert_sort(u16_t* a, u8_t len, u16_t old_element,u16_t new_element) //ascending sort
{
    //printf("old:%f,new:%f\n", (float)old_element / (1 << AMPD_ENV_SCALE), (float)new_element/ (1 << AMPD_ENV_SCALE));
    for (u16_t i=0; i < len; ++i)//find old element
    { 
        if (a[i] == old_element)
        {
            for (u16_t j = i; j<len-1; ++j)
            {
                a[j] = a[j+1];
            }
            a[len-1]= new_element;
			break;
		}
	}
    //printf("temp result:\n");
    //for (u16_t i = 0; i < len; ++i)
    //{
    //    printf("%f\t", (float)a[i] / (1 << AMPD_ENV_SCALE));
    //    //printf("%f\t", (float)(edet->fixed_AATG.aatg_buf[i])/ (1 << AMPD_ENV_SCALE));
    //}
    //printf("end \n");
    for (u16_t i = len-1; i >0; --i)//sort new element
    {
        if (a[i] < a[i - 1])
        {
            u16_t tmp = a[i - 1];
            a[i - 1] = a[i];
            a[i] = tmp;
        }
    }
    
}