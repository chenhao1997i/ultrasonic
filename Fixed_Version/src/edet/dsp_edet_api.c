﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_edet_api.h"
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
void edet_init_api(EDET_PARAM* edet,DspConfig* dsp_cfg)
{
	edet_init(edet, dsp_cfg);
}

errorReturn edet_process_api(EDET_PARAM* edet,EdetIO* edet_io, u16_t* DSP_IRQ_STATUS)
{
	u1_t ret = edet_process( edet,edet_io, DSP_IRQ_STATUS);
	if (ret != 0)
		return errorEdet;

}

void  edet_destroy_api(EDET_PARAM* edet)
{
	edet_destroy(edet);
}