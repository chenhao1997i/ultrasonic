﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
u1_t adv_EEVAL_process(ADV_EEVAL* adv_EEVAL, u16_t ms_ts, u16_t filt, u8_t conf,u16_t* DSP_IRQ_STATUS)
{
	// EEVAL
	//printf("msts:%d\tlsfilt:%f\tthreshold:%f\n", adv_EEVAL->ls_ms_ts, adv_EEVAL->ls_filt / (float)(1 << AMPD_ENV_SCALE), adv_EEVAL->threshold/(float)(1 << AMPD_ENV_SCALE));
	u8_t maximum_detc = 0;
	if (adv_EEVAL->ls_filt < filt)
	{
		adv_EEVAL->positive_cnt += 1;
	}
	if (adv_EEVAL->ls_filt > filt)
	{
		if (adv_EEVAL->positive_cnt > adv_EEVAL->eeval_sens)
		{
			//MAX_EVT maximum on ENVP_ENV_RAW detected
			maximum_detc = 1;
		}
		adv_EEVAL->positive_cnt = 0;
	}
	//printf("time:%f filt:%f threshold:%f\n", (float)ms_ts, (float)filt / (1 << filt_SCALE), (float)adv_EEVAL->threshold / (1 << filt_SCALE));
	//printf("%d:%f:%f:%f\n", edet->frame_num, (float)ms_ts, (float)filt / (1 << filt_SCALE), (float)edet->fixed_out_ptr.atg_th/ (1 << filt_SCALE));
	if (filt >= adv_EEVAL->threshold)
	{
		//printf("%d:%d:%d:%d:%d\n", adv_EEVAL->eeval_sel, adv_EEVAL->eeval_sel & 1 == 1, (adv_EEVAL->eeval_sel & 8)==8 , adv_EEVAL->eeval_sel & 4 == 4, adv_EEVAL->eeval_sel & 2 == 2);
		if (maximum_detc==1)
		{
			//MAX_EVT maximum on ENVP_ENV_RAW detected
			DSP_IRQ_STATUS[0]|=adv_EEVAL->irq_bit;
			//edet->fixed_out_ptr.IRQ_ptr.eeval1_max_evt = 1;
			adv_EEVAL->data_packet = adv_EEVAL->ls_ms_ts + (adv_EEVAL->ls_filt<<16)+(adv_EEVAL->ls_conf<<32);
		}
	}
	adv_EEVAL->ls_ms_ts = ms_ts;
	adv_EEVAL->ls_filt = filt;
	adv_EEVAL->ls_conf = conf;
	//adv_EEVAL->ls_threshold = adv_EEVAL->threshold;
	//edet_result
	//if ((edet->fixed_out_ptr.IRQ_ptr.eeval1_max_evt==1)|| (edet->fixed_out_ptr.IRQ_ptr.eeval2_max_evt == 1))
	if ((DSP_IRQ_STATUS[0]& adv_EEVAL->irq_bit)>0)
	{
		//showing results in float
		//float mstsf = (float)ms_ts;
		//printf("msts_adv: %d\n", ms_ts);
		//printf("conf:%d,filt:%d msts:%d\n", (u16_t)((adv_EEVAL->data_packet>>32)&15), (u16_t)(((adv_EEVAL->data_packet>>16)&65535)), (u16_t)(adv_EEVAL->data_packet & 65535));
		//printf("conf:%d,filt:%d msts:%d, %fcm\n", (u16_t)((adv_EEVAL->data_packet>>32)&15), (u16_t)(((adv_EEVAL->data_packet>>16)&65535)), (u16_t)(adv_EEVAL->data_packet & 65535), (float)(adv_EEVAL->data_packet & 65535)*0.017);
	}
	return 0;
}

