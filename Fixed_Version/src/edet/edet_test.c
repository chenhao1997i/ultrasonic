// Copyright 2022 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "dsp_edet.h"
#include "dsp_edet_api.h"
#include "../../include/dsp_algo_api.h"
#include "../../include/dsp_public.h"
#include "../../include/dsp_dsptypes.h"
int16_t main()
{
	printf("running edet_test\n");
	int32_t sample_rate = 58000;
	int16_t framelen = 1;
	float ATG_TH = 0;
	FILE* fp = NULL;
	fp = fopen("../matlab_data/edet_test_data/Mresult_envelop_in.bin", "rb");//loading interpolated envelop from SPM demo
	fseek(fp, 0, SEEK_END);
	int32_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);//back to the start of bin
	float* AMPD_ENV = (float*)malloc(size);
	int32_t input_len = size/sizeof(float);
	fread(AMPD_ENV, sizeof(float), input_len, fp);
	//loading interpolated msts from SPM demo
	FILE* fp2 = NULL;
	fp2 = fopen("../matlab_data/edet_test_data/Mresult_msts.bin", "rb");//read binary file
	fseek(fp2, 0, SEEK_END);
	int32_t size2 = ftell(fp2);
	fseek(fp2, 0, SEEK_SET);//back to the start of bin
	float* ms_ts = (float*)malloc(size2);
	int32_t input_len2 = size2 / sizeof(float);
	fread(ms_ts, sizeof(float), input_len2, fp2);
	////generated msts:
	//float* ms_ts = (float*)malloc(size);
	//for (int32_t i = 0; i < input_len; ++i)
	//{
	//	//ms_ts[i] = (float)i / (float)sample_rate;
	//	ms_ts[i] = (float)i ;
	//}
	//// STG based on the picture in datasheet
	//float STG_TH[16] = { 7e-3, 7e-3, 7e-3, 7e-3, 3.8e-3, 2.2e-3, 2.2e-3, 3.8e-3, 3.8e-3, 3.8e-3, 3.8e-3, 2.0e-3, 2.0e-3, 1.0e-3, 1.0e-3, 1.0e-3 };
	//float STG_TB[16] = { 20, 20, 20, 20, 5, 2, 5, 10, 10, 5, 5, 2, 2, 5, 5, 5 };
	//float STG_STEP[16] = { 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 4.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4, 1.0e-4 };
	//for (int16_t i = 0; i < 16; i++)
	//{
	//	STG_TH[i] = STG_TH[i] * 4;
	//	STG_TB[i] = STG_TB[i]/sample_rate;
	//	STG_STEP[i] = STG_STEP[i] * 3;
	//}
	//// STG based on SPM tool parameter
	float STG_TH[] = {128,64,32,32,32,32,32,32 };
	float STG_TB[] = { 2.38,2.38,2.38,2.38,2.38,2.38,2.38,2.38 };
	float STG_STEP[] = { 2,2,1,1,1,1,1,1 };
	int16_t j = 0;
	//edet initiation
	DspConfig config = { 0 };
	config.aatg_cn =10;
	config.aatg_cw = 8;
	config.aatg_alpha = 4;
	//config.atg_ini = 0.04;
	//config.atg_tau = 0.001;
	//config.atg_alpha = 5;
	config.atg_ini = 5;
	config.atg_tau = 3;
	config.atg_alpha = 3;
	config.atg_cfg = 1;
	config.eeval_dma_val = 0;
	config.eeval_sens = 7;
	config.eeval_sel = 0;
	//EDET_PARAM* edet = (EDET_PARAM*)calloc(sizeof(EDET_PARAM), 1);
	//edet_init(edet, &config);
	EDET_PARAM* edet = edet_init_api(&config);

	//edet process
	for (int32_t i = 0; i < input_len-framelen; i += framelen)
	{
		j = floor(ms_ts[i]/(ms_ts[input_len-1]/8));
		//printf("*****%d\n",edet->frame_num);
		edet_process(edet, &(ms_ts[i]), &(AMPD_ENV[i]), &(AMPD_ENV[i]),framelen, STG_TH[j], STG_TB[j], STG_STEP[j], 0, 0);
		if ((edet->out_ptr->IRQ_ptr->eeval_tlh_evt)|| (edet->out_ptr->IRQ_ptr->eeval_thl_evt)|| (edet->out_ptr->IRQ_ptr->eeval_max_evt)|| (edet->out_ptr->IRQ_ptr->eeval_min_evt))
		{
			edet->out_ptr->IRQ_ptr->eeval_tlh_evt = 0;
			edet->out_ptr->IRQ_ptr->eeval_thl_evt = 0;
			edet->out_ptr->IRQ_ptr->eeval_max_evt = 0;
			edet->out_ptr->IRQ_ptr->eeval_min_evt = 0;
			printf("%d:EVT detected type:%d,ms_ts:%f\n", i,edet->out_ptr->event_identification,edet->out_ptr->ms_ts);
		}
    }
	fclose(fp);
	fclose(fp2);
	free(ms_ts);
	free(AMPD_ENV);
	edet_destroy(edet);
	return 0;
}
