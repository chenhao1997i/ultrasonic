﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
u1_t ATG_process(FIXED_FREQ* fixed_freq, u16_t AMPD_ENV)
{
    // ATG
    if (fixed_freq->atg_cfg)
    {
        if (fixed_freq->atg_th > AMPD_ENV)
        {
                fixed_freq->tmp_atg_th = fixed_freq->tmp_atg_th * fixed_freq->atg_tau;//fixed_freq->tmp_atg_th * fixed_freq->atg_tau
                u32_t tmp_atg_th_add = AMPD_ENV << atg_tau_SCALE;
                fixed_freq->tmp_atg_th = fixed_freq->tmp_atg_th  + tmp_atg_th_add;//(fixed_freq->tmp_atg_th * fixed_freq->atg_tau + (1<<atg_tau_SCALE ) * AMPD_ENV)
                tmp_atg_th_add = fixed_freq->atg_tau * AMPD_ENV;
                fixed_freq->tmp_atg_th = (fixed_freq->tmp_atg_th - tmp_atg_th_add)>> atg_tau_SCALE;
                //fixed_freq->tmp_atg_th = (fixed_freq->tmp_atg_th * fixed_freq->atg_tau + (1<<atg_tau_SCALE - fixed_freq->atg_tau) * AMPD_ENV)>>14;
                fixed_freq->atg_th = (fixed_freq->atg_alpha * fixed_freq->tmp_atg_th)>> atg_alpha_SCALE;
                fixed_freq->atg_th=(fixed_freq->atg_th>65535? 65535 :fixed_freq->atg_th);
        }
    }
    else
    {
        fixed_freq->atg_th = 0;
    }
    return 0;
}