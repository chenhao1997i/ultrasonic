﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_edet.h"
#include "../../include/dsp_dsptypes.h"
u1_t OS_CFAR_process(FIXED_AATG* aatg)
{
    insert_sort(aatg->sorted_cell_register, aatg->aatg_cn * 2, aatg->last_cell, aatg->aatg_buf[aatg->aatg_buf_cnt]);
    //edet->fixed_EEVAL.threshold = aatg->sorted_cell_register[aatg->k] * aatg->aatg_alpha;
    aatg->aatg_th = aatg->sorted_cell_register[aatg->k] * aatg->aatg_alpha;
    aatg->aatg_th = aatg->aatg_th >> aatg_alpha_SCALE;
    return 0;
}