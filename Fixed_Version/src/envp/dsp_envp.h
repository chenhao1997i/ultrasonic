﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: liqilun@eswincomputing.com

#ifndef _DSP_ENVP_H_
#define _DSP_ENVP_H_

#include "../../include/dsp_public.h"
#include "../../include/dsp_dsptypes.h"

#ifdef __cplusplus
extern "C" {
#endif


//adv

typedef struct
{
	//s32_t frame_num;
	u3_t cic_cnt;  //u3_t
	u3_t resam_cnt;  //u3_t
	//s16_t cycstart;
	s32_t fc;
	s32_t fs;
	//s16_t framelen;
	u32_t fmode;
	u16_t bias;
	//s16_t idx;
	//s16_t update_num;
	//=========configuration parameter============
	//s16_t outbuff[512];
	//IQ demodulate

	s16_t yi[8];
	s16_t yq[8];

	//low pass filter
	s16_t filtCoeff_tx[7];
	s16_t filtCoeff_rx[65];
	s16_t resam_coefs_fixed[11];
	/*s16_t env_i_temp[128];
	s16_t env_q_temp[128];
	s16_t env_i[128];
	s16_t env_q[128];*/

	u16_t filter_len;
	u16_t filter_maxlen;
	u16_t resam_filtlen;
	u16_t resam_upfactor;
	u32_t extract_factor;
	s32_t env_raw_fm;
	s32_t env_phi_fm;
	//Fix frequency signal path process
	//s16_t env_len;
		

	//ENVP_CFG
	u1_t fspi_sel;
	u1_t aspi_sel;
	u1_t dsr_sel;
	u5_t g_cal;
	u1_t envp_afc; // Enable auto bandwidth during burst
	u3_t envp_cfg;
	//ENVP_CTRL
	u1_t envp_restart;// Restart envelope processor (set input multiplier state-counter to zero)
	u1_t envp_afc_brk;
	u32_t delta_t_fixed;

	// advanced path
	s16_t npulse;
	//float tao;
	u16_t N1;
	u16_t N2;
	u7_t filt_len_code;
	u7_t filt_len_fix;
	u1_t f1_coeff_sel;
	u1_t f2_coeff_sel;


	u16_t buffer_abs_y_code[128];
	s16_t buffer_y_code_real[128];
	s16_t buffer_y_code_imag[128];
	s16_t buffer_y_fix_real[128];
	s16_t buffer_y_fix_imag[128];
	s16_t buffer_abs_y_fix[128];
	s32_t sum_buffer_abs_y_fix_real;
	s32_t sum_buffer_abs_y_fix_imag;
	s16_t sum_real_buffer_y2;
	s16_t sum_imag_buffer_y2;


	/*s16_t buffer_abs_y[64];
	s16_t buffer_y_real[64];
	s16_t buffer_y_imag[64];*/


	s16_t yi_cic;
	s16_t yq_cic;
	s16_t yi_filt_in[65];
	s16_t yq_filt_in[65];

	s16_t yi_filt_out_fixed;
	s16_t yq_filt_out_fixed;
	s16_t yi_filt_out_adv;
	s16_t yq_filt_out_adv;

	s16_t yi_resam_in_fixed[128];
	s16_t yq_resam_in_fixed[128];
	s16_t yi_resam_out_fixed;
	s16_t yq_resam_out_fixed;
	s16_t yi_resam_in_adv[128];
	s16_t yq_resam_in_adv[128];
	s16_t yi_resam_out_adv;
	s16_t yq_resam_out_adv;


	/*s16_t yi2[128];
	s16_t yq2[128];
	s16_t yi2_filt[128];
	s16_t yq2_filt[128];
	s16_t yi2_fin[128];
	s16_t yq2_fin[128];
	s16_t yi2_filt_fin[128];
	s16_t yq2_filt_fin[128];*/

	s8_t coeffi[128];
	s8_t coeffq[128];

	//u10_t msts_delay;
	//u16_t msts_advdelay;
	u16_t N1_inverse;
	u16_t N2_inverse;
	//CIC
	//s16_t yn[128];

} ENVP_PARAM;

void envp_init(ENVP_PARAM* envp, s32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output);
s16_t envp_process(ENVP_PARAM* envp, u14_t ADC_DTA, DspInputOutput* input_output);
void envp_destroy(ENVP_PARAM* envp, DspInputOutput* input_output);

s16_t iq_demod_q(ENVP_PARAM* envp, DspInputOutput* input_output, s16_t ADC_DTA);
u16_t sqrt_fixed(s32_t x, s32_t y);
s16_t calc_atan_phase(s16_t real, s16_t imag);
s16_t resample_fixed(ENVP_PARAM* envp, DspInputOutput* input_output, s16_t* indata);
s16_t cic_simulat_fixed(s16_t R, u32_t factor, s16_t* xn);
s16_t filter_fixed(s16_t* input, s16_t filt_maxlen, ENVP_PARAM* envp, DspInputOutput* input_output);
#ifdef __cplusplus
}
#endif
#endif // _DSP_ENVP_H_