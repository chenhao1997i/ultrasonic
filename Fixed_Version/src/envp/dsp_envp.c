﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: liqilun@eswincomputing.com
#include "dsp_envp.h"

#include <math.h>
#include "../../include/dsp_dsptypes.h"
void filtercoeff_set(ENVP_PARAM* envp)
{
	envp->filter_len = 7;
	u13_t filtCoeff[] = { -29, 320, 2081, 3447, 2081, 320, -29 };
	for (u16_t i = 0; i < envp->filter_len; i++)
	{
		envp->filtCoeff_tx[i] = filtCoeff[i];
	}
	if (envp->envp_cfg == 0 || envp->envp_cfg == 1 || envp->envp_cfg == 2)
		for (u16_t i = 0; i < envp->filter_len; i++)
		{
			envp->filtCoeff_rx[i] = filtCoeff[i];
		}
	if (envp->envp_cfg == 3)
	{
		envp->filter_len = 11;
		u13_t filtCoeff[] = { 21, 115, 443, 1014, 1586, 1828, 1586, 1014, 443, 115, 21, };
		for (u16_t i = 0; i < envp->filter_len; i++)
		{
			envp->filtCoeff_rx[i] = filtCoeff[i];
		}
	}
	if (envp->envp_cfg == 4)
	{
		envp->filter_len = 17;
		u13_t filtCoeff[] = { 34, 63, 144, 283, 472, 682, 875, 1010, 1059, 1010, 875, 682, 472, 283, 144, 63, 34, };
		for (u16_t i = 0; i < envp->filter_len; i++)
		{
			envp->filtCoeff_rx[i] = filtCoeff[i] ;
		}
	}
	if (envp->envp_cfg == 5)
	{
		envp->filter_len = 27;
		u13_t filtCoeff[] = { 29, 37, 57, 91, 137, 196, 263, 336, 410, 479, 540, 586, 616, 626, 616, 586, 540, 479, 410, 336, 263, 196, 137, 91, 57, 37, 29, };
		for (u16_t i = 0; i < envp->filter_len; i++)
		{
			envp->filtCoeff_rx[i] = filtCoeff[i];
		}
	}
	if (envp->envp_cfg == 6)
	{
		envp->filter_len = 39;
		u13_t filtCoeff[] = { 25, 28, 35, 46, 62, 82, 106, 132, 162, 193, 226, 258, 290, 319, 346, 369, 388, 402, 411, 413, 411, 402, 388, 369, 346, 319, 290, 258, 226, 193, 162, 132, 106, 82, 62, 46, 35, 28, 25, };
		for (u16_t i = 0; i < envp->filter_len; i++)
		{
			envp->filtCoeff_rx[i] = filtCoeff[i];
		}
	}
	if (envp->envp_cfg == 7)
	{
		envp->filter_len = 65;
		u13_t filtCoeff[] = { 16, 16, 18, 20, 24, 28, 34, 40, 47, 55, 63, 72, 82, 92, 103, 114, 125, 136, 148, 159, 169, 180, 190, 199, 208, 216, 223, 229, 234, 238, 240, 242, 243, 242, 240, 238, 234, 229, 223, 216, 208, 199, 190, 180, 169, 159, 148, 136, 125, 114, 103, 92, 82, 72, 63, 55, 47, 40, 34, 28, 24, 20, 18, 16, 16, };
		for (u16_t i = 0; i < envp->filter_len; i++)
		{
			envp->filtCoeff_rx[i] = filtCoeff[i];
		}
	}

}

void envp_restart(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	envp->cic_cnt = 0;
	envp->resam_cnt = 0;
	envp->env_raw_fm = 0;
	envp->env_phi_fm = 0;
	//IQ demodulate
	for (u16_t i = 0; i < envp->fmode; i++)
	{
		envp->yi[i] = 0;
		envp->yq[i] = 0;
	}
	//low pass filter
	for (u16_t i = 0; i < 7; i++)
	{
		envp->filtCoeff_tx[i] = 0;
	}
	for (u16_t i = 0; i < envp->filter_maxlen; i++)
	{
		envp->filtCoeff_rx[i] = 0;
		envp->yi_filt_in[i] = 0;
		envp->yq_filt_in[i] = 0;
	}
	envp->extract_factor = (u16_t)(((1 << extract_factor_SCALE)) / envp->fmode + 1);
	//Fix frequency signal path process
	for (u16_t i = 0; i < envp->resam_filtlen; i++)
	{
		envp->yi_resam_in_adv[i] = 0;
		envp->yq_resam_in_adv[i] = 0;
		envp->yi_resam_in_fixed[i] = 0;
		envp->yq_resam_in_fixed[i] = 0;
	}
	//resample filter

	//complex conv  
	for (u16_t i = 0; i < envp->N2; i++)
	{
		envp->buffer_abs_y_code[i] = 0;
		envp->buffer_y_code_real[i] = 0;
		envp->buffer_y_code_imag[i] = 0;
	}
	//Advanced path output
	if (envp->f1_coeff_sel == 1 || envp->f2_coeff_sel == 1)
	{
		for (u16_t i = 0; i < envp->N1; i++)
		{
			envp->buffer_y_fix_real[i] = 0;
			envp->buffer_y_fix_imag[i] = 0;
			envp->buffer_abs_y_fix[i] = 0;
		}
		envp->sum_buffer_abs_y_fix_real = 0;
		envp->sum_buffer_abs_y_fix_imag = 0;
		envp->sum_real_buffer_y2 = 0;
		envp->sum_imag_buffer_y2 = 0;
	}
}


void envp_init(ENVP_PARAM* envp, s32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	envp->bias = dsp_cfg->bias;
	envp->fc = dsp_cfg->fc;
	envp->fs = sample_rate;
	envp->fmode = dsp_cfg->fmode;
	envp->env_raw_fm = 0;
	envp->env_phi_fm = 0;
	//=========configuration parameter============

	//low pass filter
	for (u16_t i = 0; i < 7; i++)
	{
		envp->filtCoeff_tx[i] = 0;
	}
	for (u16_t i = 0; i < 65; i++)
	{
		envp->filtCoeff_rx[i] = 0;
	}
	envp->filter_len = 0;
	envp->filter_maxlen = 65;
	envp->extract_factor = (u16_t)round(((float)(1 << extract_factor_SCALE) / envp->fmode)); //Please round off the implementation instead of +1

	envp->f1_coeff_sel = (dsp_cfg->ENVP_FILT_CFG >> 14) & 0x01;
	envp->f2_coeff_sel = (dsp_cfg->ENVP_FILT_CFG >> 15) & 0x01;
	envp->filt_len_fix = dsp_cfg->ENVP_FILT_CFG & 0x7f;
	envp->filt_len_code = (dsp_cfg->ENVP_FILT_CFG >> 7) & 0x7f;
	envp->N1 = envp->filt_len_fix + 1;
	envp->N2 = envp->filt_len_code + 1;
	envp->N1_inverse = (1 << 16) / envp->N1;
	envp->N2_inverse = (1 << 16) / envp->N2;


	//Fix frequency signal path process
	envp->envp_cfg = dsp_cfg->ENVP_CFG & 0x07;
	envp->envp_afc = (dsp_cfg->ENVP_CFG >> 3) & 0x01;
	envp->g_cal = (dsp_cfg->ENVP_CFG >> 4) & 0x1f;
	envp->dsr_sel = (dsp_cfg->ENVP_CFG >> 9) & 0x01;
	envp->fspi_sel = (dsp_cfg->ENVP_CFG >> 10) & 0x01;
	envp->aspi_sel = (dsp_cfg->ENVP_CFG >> 11) & 0x01;
	
	envp->envp_restart = dsp_cfg->ENVP_CTRL & 0x01;
	envp->envp_afc_brk = (dsp_cfg->ENVP_CTRL >> 1) & 0x01;

	envp->delta_t_fixed = (envp->dsr_sel == 1 ? envp->fc>>1 : envp->fc);

	envp->resam_upfactor = (envp->dsr_sel == 0 ? 1 : 2);

	
	
	//init resample
	s14_t coeff[11] = { 0, 0, 0, -399, 0, 4510, 8162, 4510, 0, -399, 0, };
	envp->resam_filtlen = 11;
	for (u16_t i = 0; i < 11; i++)
	{
		envp->resam_coefs_fixed[i] = coeff[i];
	}
	input_output->envpio.delay = 3;
	//resample filter
	// Advanced path process
	envp->fspi_sel = dsp_cfg->fspi_sel;
	envp->aspi_sel = dsp_cfg->aspi_sel;
	//envp->ncoeffs_max = 256;




	for (u16_t i = 0; i < envp->N2; i++)
	{
		envp->buffer_abs_y_code[i] = 0;
		envp->buffer_y_code_real[i] = 0;
		envp->buffer_y_code_imag[i] = 0;
		envp->coeffi[i] = 0;
		envp->coeffq[i] = 0;
	}
	for (u16_t i = 0; i < envp->N1; i++)
	{
		envp->buffer_y_fix_real[i] = 0;
		envp->buffer_y_fix_imag[i] = 0;
		envp->buffer_abs_y_fix[i] = 0;
	}
	envp_restart(envp, input_output);
	filtercoeff_set(envp);
	/*u16_t adv_npulse = 0;
	if (envp->dsr_sel == 1)
	{
		adv_npulse = 2 * envp->N2; //not real npulse,npulse=real_npulse or npulse=real_npulse+1
	}
	else
	{
		adv_npulse = envp->N2;
	}*/

//	envp->msts_delay = (envp->filter_len - 1) * 500000 / envp->fc;
//	envp->msts_advdelay = (adv_npulse * 1000000) / envp->fc ;

	for (int16_t i = 0; i < dsp_cfg->COEFF.length; ++i)
	{
		if (i < dsp_cfg->COEFF.length / 2)
		{

			envp->coeffi[i] = dsp_cfg->COEFF.coeff[i] -8 ;
			envp->coeffq[i] = dsp_cfg->COEFF.coeff[i + dsp_cfg->COEFF.length / 2] - 8;
		}
		else
		{
			envp->coeffi[i] = dsp_cfg->COEFF.coeff[dsp_cfg->COEFF.length - i - 1] - 8;//conj
			envp->coeffq[i] = dsp_cfg->COEFF.coeff[3 * dsp_cfg->COEFF.length / 2 - i - 1] - 8;

		}
	}

}

s16_t get_result_fix(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	s32_t env_i = (envp->yi_resam_out_fixed * gain_array[envp->g_cal]) >> 8;
	s32_t env_q = (envp->yq_resam_out_fixed * gain_array[envp->g_cal]) >> 8;
	input_output->envpio.ENVP_ENV_RAW = sqrt_fixed(env_i, env_q);

	input_output->envpio.env_phi = calc_atan_phase(envp->yq_resam_out_fixed, envp->yi_resam_out_fixed); //bit 8
	//printf("%d   %.10f\n", envp->frame_num, (float)(input_output->envpio.env_phi) / ((1 << 5) * 1.0f));
	int  x = (envp->dsr_sel == 1 ? envp->fmode * 2 : envp->fmode);
	//printf("raw%d: %d\n", (input_output->frame_num- envp->fmode+1)/x+1,input_output->envpio.ENVP_ENV_RAW);
	s32_t envd_raw = ((input_output->envpio.ENVP_ENV_RAW - envp->env_raw_fm) * envp->delta_t_fixed);
	if (envd_raw >= (1 << 30))envd_raw = (1 << 30) - 1;
	if (envd_raw <= -(1 << 30))envd_raw = -(1 << 30);
	input_output->envpio.envd_raw = envd_raw >> 15;
	s32_t env_phid = ((input_output->envpio.env_phi - envp->env_phi_fm) * envp->delta_t_fixed);
	if (env_phid >= (1 << 23))env_phid = (1 << 23) - 1;
	if (env_phid <= -(1 << 23))env_phid = -(1 << 23) ;
	input_output->envpio.env_phid = env_phid >> 16;

	envp->env_raw_fm = input_output->envpio.ENVP_ENV_RAW;
	envp->env_phi_fm = input_output->envpio.env_phi;

	return 0;
}

s16_t get_result_adv(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	s16_t env_i = (envp->yi_resam_out_adv * gain_array[envp->g_cal]) >> 10; //gain_array[31] = 983 does not overflow
	s16_t env_q = (envp->yq_resam_out_adv * gain_array[envp->g_cal]) >> 10;
	for (int16_t i = 1; i < envp->N2; ++i)
	{
		envp->buffer_y_code_real[i - 1] = envp->buffer_y_code_real[i];
		envp->buffer_y_code_imag[i - 1] = envp->buffer_y_code_imag[i];
		envp->buffer_abs_y_code[i - 1] = envp->buffer_abs_y_code[i];
	}
	envp->buffer_y_code_real[envp->N2 - 1] = env_i;
	envp->buffer_y_code_imag[envp->N2 - 1] = env_q;
	envp->buffer_abs_y_code[envp->N2 - 1] = sqrt_fixed(env_i, env_q);
	int filt1_conv_real = 0; int filt1_conv_imag = 0;
	int conf1_conv_real = 0; int conf1_conv_imag = 0;
	int filt2_conv_real = 0; int filt2_conv_imag = 0;
	int conf2_conv_real = 0; int conf2_conv_imag = 0;
	if (envp->f1_coeff_sel == 0)
	{
		for (int m = 0; m < envp->N2; ++m)
		{
			//printf("%d   %d\n", envp->coeffi[m / 2], envp->coeffq[m / 2]);
			int crl = envp->coeffi[m / 2] << 8;
			int cig = envp->coeffq[m / 2] << 8;
			int yrl = envp->buffer_y_code_real[m];
			int yig = envp->buffer_y_code_imag[m];

			int crl_mul_yrl = crl * yrl;
			int cig_mul_yig = cig * yig;
			int cig_mul_yrl = cig * yrl;
			int crl_mul_yig = crl * yig;
			//filt
			filt1_conv_real = filt1_conv_real + crl_mul_yrl - cig_mul_yig;
			filt1_conv_imag = filt1_conv_imag + cig_mul_yrl + crl_mul_yig;
			//confidence
			if (envp->buffer_abs_y_code[m] > 2)
			{
				int acc = crl_mul_yrl - cig_mul_yig;
				acc = acc / envp->buffer_abs_y_code[m];
				acc = acc << adc_dta_SCALE;
				conf1_conv_real = (conf1_conv_real)+acc;
				acc = cig_mul_yrl + crl_mul_yig;
				acc = acc / envp->buffer_abs_y_code[m];
				acc = acc << adc_dta_SCALE;
				conf1_conv_imag = conf1_conv_imag + acc;
				if (conf1_conv_real >= (1 << 30))
				{
					conf1_conv_real = (1 << 30) - 1;
				}
				if (conf1_conv_imag >= (1 << 30))
				{
					conf1_conv_imag = (1 << 30) - 1;
				}
				if (conf1_conv_real <= -(1 << 30))
				{
					conf1_conv_real = -(1 << 30);
				}
				if (conf1_conv_imag <= -(1 << 30))
				{
					conf1_conv_imag = -(1 << 30);
				}
			}
		}
		filt1_conv_real = filt1_conv_real >> 16;
		filt1_conv_imag = filt1_conv_imag >> 16;
		conf1_conv_real = conf1_conv_real >> 14;
		conf1_conv_imag = conf1_conv_imag >> 14;
		u32_t filt1 = sqrt_fixed(filt1_conv_real, filt1_conv_imag) * envp->N2_inverse;
		if (filt1 >= (1 << 25)) filt1 = (1 << 25) - 1;
		input_output->envpio.filt1 = filt1 >> 9;
		u32_t conf1 = sqrt_fixed(conf1_conv_real, conf1_conv_imag) * envp->N2_inverse;
		if (conf1 >= (1 << 26)) conf1 = (1 << 26) - 1;
		input_output->envpio.conf1 = conf1 >> 22;
	}
	else
	{
		for (int16_t i = 1; i < envp->N1; ++i)
		{
			envp->buffer_y_fix_real[i - 1] = envp->buffer_y_fix_real[i];
			envp->buffer_y_fix_imag[i - 1] = envp->buffer_y_fix_imag[i];
			envp->buffer_abs_y_fix[i - 1] = envp->buffer_abs_y_fix[i];
		}
		envp->buffer_y_fix_real[envp->N1 - 1] = env_i;
		envp->buffer_y_fix_imag[envp->N1 - 1] = env_q;
		envp->buffer_abs_y_fix[envp->N1 - 1] = sqrt_fixed(env_i, env_q);
		s32_t sum_buffer_y_fix_real = 0;
		s32_t sum_buffer_y_fix_imag = 0;
		for (int i = 0; i < envp->N1; ++i)
		{
			sum_buffer_y_fix_real += envp->buffer_y_fix_real[i];
			sum_buffer_y_fix_imag += envp->buffer_y_fix_imag[i];
		}
		sum_buffer_y_fix_real = sum_buffer_y_fix_real >> 4;
		sum_buffer_y_fix_imag = sum_buffer_y_fix_imag >> 4;
		u32_t filt1 = sqrt_fixed(sum_buffer_y_fix_real, sum_buffer_y_fix_imag) * envp->N1_inverse;
		if (filt1 >= (1 << 25)) filt1 = (1 << 25) - 1;
		input_output->envpio.filt1 = filt1 >> 9;
		//printf("%d \t%.10f\n", envp->frame_num, (float)envp->sum_buffer_abs_y_fix_real / ((1 << 13) * 1.0f));

		for (int16_t m = 0; m < envp->N1; ++m)
		{
			if (envp->buffer_abs_y_fix[m] > 1)
			{
				int acc = envp->buffer_y_fix_real[m];
				acc = (acc << adc_dta_SCALE) / envp->buffer_abs_y_fix[m];
				conf1_conv_real = conf1_conv_real + acc;
				acc = envp->buffer_y_fix_imag[m];
				acc = (acc << adc_dta_SCALE) / envp->buffer_abs_y_fix[m];
				conf1_conv_imag = conf1_conv_imag + acc;
			}
		}
		conf1_conv_real = conf1_conv_real >> 3;
		conf1_conv_imag = conf1_conv_imag >> 3;
		

		u32_t conf1 = sqrt_fixed(conf1_conv_real, conf1_conv_imag) * envp->N1_inverse;
		if (conf1 >= (1 << 26)) conf1 = (1 << 26) - 1;
		input_output->envpio.conf1 = conf1 >> 22;
	}
	
	//printf("%d \t%.10f\n", envp->frame_num, (float)(input_output->envpio.conf1) / ((1 << 0) * 1.0f));

	if (envp->f2_coeff_sel == 0)
	{
		for (int m = 0; m < envp->N2; ++m)
		{
			//printf("%d   %d\n", envp->coeffi[m / 2], envp->coeffq[m / 2]);
			int crl = envp->coeffi[m / 2] << 8;
			int cig = envp->coeffq[m / 2] << 8;
			int yrl = envp->buffer_y_code_real[m];
			int yig = envp->buffer_y_code_imag[m];

			int crl_mul_yrl = crl * yrl; // s12_t * s16_t
			int cig_mul_yig = cig * yig;
			int cig_mul_yrl = cig * yrl;
			int crl_mul_yig = crl * yig;
			//filt
			filt2_conv_real = filt2_conv_real + crl_mul_yrl + cig_mul_yig;
			filt2_conv_imag = filt2_conv_imag + crl_mul_yig - cig_mul_yrl;

			//confidence
			if (envp->buffer_abs_y_code[m] > 2)
			{
				int acc = crl_mul_yrl + cig_mul_yig;
				acc = acc / envp->buffer_abs_y_code[m];
				acc = acc << adc_dta_SCALE;
				conf2_conv_real = conf2_conv_real + acc;
				acc = cig_mul_yrl - crl_mul_yig;
				acc = acc / envp->buffer_abs_y_code[m];
				acc = acc << adc_dta_SCALE;
				conf2_conv_imag = conf2_conv_imag + acc;
				if (conf2_conv_real >= (1 << 30))
				{
					conf2_conv_real = (1 << 30) - 1;
				}
				if (conf2_conv_imag >= (1 << 30))
				{
					conf2_conv_imag = (1 << 30) - 1;
				}
				if (conf2_conv_real <= -(1 << 30))
				{
					conf2_conv_real = -(1 << 30);
				}
				if (conf2_conv_imag <= -(1 << 30))
				{
					conf2_conv_imag = -(1 << 30);
				}
			}
		}
		filt2_conv_real = filt2_conv_real >> 16;
		filt2_conv_imag = filt2_conv_imag >> 16;
		conf2_conv_real = conf2_conv_real >> 14;
		conf2_conv_imag = conf2_conv_imag >> 14;
		//if (ABS(filt2_conv_real) > 32767 || ABS(filt2_conv_imag) > 32767)printf("error\n");
		u32_t filt2 = sqrt_fixed(filt2_conv_real, filt2_conv_imag) * envp->N2_inverse;
		if (filt2 >= (1 << 25)) filt2 = (1 << 25) - 1;
		input_output->envpio.filt2 = filt2 >> 9;
		//input_output->envpio.filt2 = input_output->envpio.filt2 >> 1;
		u32_t conf2 = sqrt_fixed(conf2_conv_real, conf2_conv_imag) * envp->N2_inverse;
		if (conf2 >= (1 << 26)) conf2 = (1 << 26) - 1;
		input_output->envpio.conf2 = conf2 >> 22;
		//printf("%d \t%.10f\n", envp->frame_num, (float)(input_output->envpio.conf2) / ((1 << 0) * 1.0f));
	}
	else
	{

		if (envp->f1_coeff_sel == 1)
		{
			s32_t sum_buffer_y_fix_real = 0;
			s32_t sum_buffer_y_fix_imag = 0;
			for (int i = 0; i < envp->N2; ++i)
			{
				sum_buffer_y_fix_real += envp->buffer_y_fix_real[i];
				sum_buffer_y_fix_imag += envp->buffer_y_fix_imag[i];
			}
			sum_buffer_y_fix_real = sum_buffer_y_fix_real >> 4;
			sum_buffer_y_fix_imag = sum_buffer_y_fix_imag >> 4;
			u32_t filt2 = sqrt_fixed(sum_buffer_y_fix_real, sum_buffer_y_fix_imag) * envp->N2_inverse;
			if (filt2 >= (1 << 25)) filt2 = (1 << 25) - 1;
			input_output->envpio.filt2 = filt2 >> 9;

			for (int16_t m = 0; m < envp->N2; ++m)
			{
				if (envp->buffer_abs_y_fix[m] > 2)
				{
					int acc = envp->buffer_y_code_real[m];
					acc = (acc << adc_dta_SCALE) / envp->buffer_abs_y_code[m];
					conf2_conv_real = conf2_conv_real + acc;
					acc = envp->buffer_y_code_imag[m];
					acc = (acc << adc_dta_SCALE) / envp->buffer_abs_y_code[m];
					conf2_conv_imag = conf2_conv_imag + acc;
				}
			}
			conf2_conv_real = conf2_conv_real >> 3;
			conf2_conv_imag = conf2_conv_imag >> 3;
			u32_t conf2 = sqrt_fixed(conf2_conv_real, conf2_conv_imag) * envp->N2_inverse;
			if (conf2 >= (1 << 26)) conf2 = (1 << 26) - 1;
			input_output->envpio.conf2 = conf2 >> 22;
		}
		else
		{
			for (int16_t i = 1; i < envp->N1; ++i)
			{
				envp->buffer_y_fix_real[i - 1] = envp->buffer_y_fix_real[i];
				envp->buffer_y_fix_imag[i - 1] = envp->buffer_y_fix_imag[i];
				envp->buffer_abs_y_fix[i - 1] = envp->buffer_abs_y_fix[i];
			}
			envp->buffer_y_fix_real[envp->N1 - 1] = env_i;
			envp->buffer_y_fix_imag[envp->N1 - 1] = env_q;
			envp->buffer_abs_y_fix[envp->N1 - 1] = sqrt_fixed(env_i, env_q);

			s32_t sum_buffer_y_fix_real = 0;
			s32_t sum_buffer_y_fix_imag = 0;
			for (int i = 0; i < envp->N1; ++i)
			{
				sum_buffer_y_fix_real += envp->buffer_y_fix_real[i];
				sum_buffer_y_fix_imag += envp->buffer_y_fix_imag[i];
			}
			sum_buffer_y_fix_real = sum_buffer_y_fix_real >> 4;
			sum_buffer_y_fix_imag = sum_buffer_y_fix_imag >> 4;
			u32_t filt2 = sqrt_fixed(sum_buffer_y_fix_real, sum_buffer_y_fix_imag) * envp->N1_inverse;
			if (filt2 >= (1 << 25)) filt2 = (1 << 25) - 1;
			input_output->envpio.filt2 = filt2 >> 9;
			//printf("%d \t%.10f\n", envp->frame_num, (float)(envp->sum_buffer_abs_y_fix_real) / ((1 << 13) * 1.0f));
			for (int16_t m = 0; m < envp->N2; ++m)
			{
				if (envp->buffer_abs_y_fix[m] > 2)
				{
					int acc = envp->buffer_y_fix_real[m];
					acc = (acc << adc_dta_SCALE) / envp->buffer_abs_y_fix[m];
					conf2_conv_real = conf2_conv_real + acc;
					acc = envp->buffer_y_fix_imag[m];
					acc = (acc << adc_dta_SCALE) / envp->buffer_abs_y_fix[m];
					conf2_conv_imag = conf2_conv_imag + acc;
				}
			}

			conf2_conv_real = conf2_conv_real >> 3;
			conf2_conv_imag = conf2_conv_imag >> 3;
			
			u32_t conf2 = sqrt_fixed(conf2_conv_real, conf2_conv_imag) * envp->N1_inverse;
			if (conf2 >= (1 << 26)) conf2 = (1 << 26) - 1;
			input_output->envpio.conf2 = conf2 >> 22;
		}
	}
	int  x = (envp->dsr_sel == 1 ? envp->fmode * 2 : envp->fmode);
	//printf("%d \t%d  %d\n", (input_output->frame_num - envp->fmode + 1) / x + 1, input_output->envpio.filt1, input_output->envpio.filt2);
	//printf("%d \t%d  %d\n", input_output->ms_ts, input_output->envpio.conf1, input_output->envpio.conf2);

	//printf("%d \t%.10f\n", envp->frame_num, (float)(envp->buffer_abs_y_code[envp->N2 - 1]) / ((1 << adc_dta_SCALE) * 1.0f));


	return 0;
}

s16_t envp_process(ENVP_PARAM* envp, u14_t ADC_DTA, DspInputOutput* input_output)
{
	s16_t adc_dta = (ADC_DTA - envp->bias);

	if (adc_dta > 8191)
		adc_dta = 8191;
	if (adc_dta < -8191)
		adc_dta = -8191;
	if (envp->envp_restart == 1)
		envp_restart(envp, input_output);
	if (input_output->envpio.adc_eoc == 1)
	{
		//data input
		iq_demod_q(envp, input_output, adc_dta);
		envp->cic_cnt++;
		//envp->frame_num++;
		//printf("%f %f\n", ((float)envp->yi[envp->fmode - 2]) / ((1 << adc_dta_SCALE) * 20*1.0f), ((float)envp->yq[envp->fmode - 1]) / ((1 << adc_dta_SCALE) * 20*1.0f));
		if (envp->cic_cnt == envp->fmode)
		{
			envp->yi_cic = cic_simulat_fixed(envp->fmode, envp->extract_factor, envp->yi);
			envp->yq_cic = cic_simulat_fixed(envp->fmode, envp->extract_factor, envp->yq);
			//printf("%d %d\n", envp->yi_cic, envp->yq_cic);
			//printf("%f %f\n", ((float)envp->yi_cic) / ((1 << adc_dta_SCALE) * 20* 1.0f), ((float)envp->yq_cic) / ((1 << adc_dta_SCALE) * 20 * 1.0f));
			for (int16_t i = 0; i < envp->filter_maxlen - 1; i++) {
				envp->yi_filt_in[i] = envp->yi_filt_in[i + 1];
				envp->yq_filt_in[i] = envp->yq_filt_in[i + 1];
			}
			envp->yi_filt_in[envp->filter_maxlen - 1] = envp->yi_cic;
			envp->yq_filt_in[envp->filter_maxlen - 1] = envp->yq_cic;
			envp->cic_cnt = 0;

			/*================fixed path===================*/

			//FIR low-pass filter
			if (envp->fspi_sel == 1)
			{
				envp->yi_filt_out_fixed = filter_fixed(envp->yi_filt_in, envp->filter_maxlen, envp, input_output);
				envp->yq_filt_out_fixed = filter_fixed(envp->yq_filt_in, envp->filter_maxlen, envp, input_output);//success
			}
			else
			{
				envp->yi_filt_out_fixed = envp->yi_filt_in[envp->filter_maxlen - 1];
				envp->yq_filt_out_fixed = envp->yq_filt_in[envp->filter_maxlen - 1];
			}
			for (int16_t i = 0; i < envp->resam_filtlen - 1; i++) {
				envp->yi_resam_in_fixed[i] = envp->yi_resam_in_fixed[i + 1];
				envp->yq_resam_in_fixed[i] = envp->yq_resam_in_fixed[i + 1];
			}
			envp->yi_resam_in_fixed[envp->resam_filtlen - 1] = envp->yi_filt_out_fixed;
			envp->yq_resam_in_fixed[envp->resam_filtlen - 1] = envp->yq_filt_out_fixed;
			if (envp->resam_cnt % envp->resam_upfactor == 0) {
				if (envp->dsr_sel == 1)
				{
					envp->yi_resam_out_fixed = resample_fixed(envp, input_output, envp->yi_resam_in_fixed);
					envp->yq_resam_out_fixed = resample_fixed(envp, input_output, envp->yq_resam_in_fixed);
				}
				else
				{
					envp->yi_resam_out_fixed = envp->yi_resam_in_fixed[envp->resam_filtlen - 1];
					envp->yq_resam_out_fixed = envp->yq_resam_in_fixed[envp->resam_filtlen - 1];
				}
				input_output->envpio.env_i = envp->yi_resam_out_fixed;
				input_output->envpio.env_q = envp->yq_resam_out_fixed;
				get_result_fix(envp, input_output);
			}

			/*================adv path===================*/
			if (envp->aspi_sel == 1)
			{
				envp->yi_filt_out_adv = filter_fixed(envp->yi_filt_in, envp->filter_maxlen, envp, input_output);
				envp->yq_filt_out_adv = filter_fixed(envp->yq_filt_in, envp->filter_maxlen, envp, input_output);//success
			}
			else
			{
				envp->yi_filt_out_adv = envp->yi_filt_in[envp->filter_maxlen - 1];
				envp->yq_filt_out_adv = envp->yq_filt_in[envp->filter_maxlen - 1];
			}
			//printf("%f %f\n", ((float)envp->yi_filt_out) / ((1 << adc_dta_SCALE) * 20 * 1.0f), ((float)envp->yq_filt_out) / ((1 << adc_dta_SCALE) * 20 * 1.0f));
			//input for resample
			for (int16_t i = 0; i < envp->resam_filtlen - 1; i++) {
				envp->yi_resam_in_adv[i] = envp->yi_resam_in_adv[i + 1];
				envp->yq_resam_in_adv[i] = envp->yq_resam_in_adv[i + 1];
			}
			envp->yi_resam_in_adv[envp->resam_filtlen - 1] = envp->yi_filt_out_adv;
			envp->yq_resam_in_adv[envp->resam_filtlen - 1] = envp->yq_filt_out_adv;
			//rsample
			if (envp->resam_cnt % envp->resam_upfactor == 0) {
				if (envp->dsr_sel == 1)
				{
					envp->yi_resam_out_adv = resample_fixed(envp, input_output, envp->yi_resam_in_adv);
					envp->yq_resam_out_adv = resample_fixed(envp, input_output, envp->yq_resam_in_adv);
				}
				else
				{
					envp->yi_resam_out_adv = envp->yi_resam_in_adv[envp->resam_filtlen - 1];
					envp->yq_resam_out_adv = envp->yq_resam_in_adv[envp->resam_filtlen - 1];
				}
				get_result_adv(envp, input_output);
					//printf("%d ", input_output->ms_ts);
					/*if (envp->spi_sel == 1)
						if (input_output->ms_ts < envp->msts_advdelay + envp->msts_delay)
							input_output->ms_ts = 0;
						else
							input_output->ms_ts = input_output->ms_ts - envp->msts_delay - envp->msts_advdelay;//ms_ts:us
					else
						if (input_output->ms_ts < envp->msts_advdelay)
							input_output->ms_ts = 0;
						else
							input_output->ms_ts = input_output->ms_ts - envp->msts_advdelay;//ms_ts:us*/
							//printf("%d %d %d\n", input_output->ms_ts, envp->msts_advdelay, envp->msts_advdelay + envp->msts_delay);

			}
			envp->resam_cnt++;
			if (envp->resam_cnt == 0x0E)
				envp->resam_cnt = input_output->envpio.delay * 2;

		}
	}
	


	return 0;
}

void envp_destroy(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	free(envp);
}