﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#include "dsp_envp_api.h"
#include "dsp_envp.h"

void envp_init_api(ENVP_PARAM* envp,int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	envp_init(envp, sample_rate, dsp_cfg, input_output);
}

errorReturn envp_process_api(ENVP_PARAM* envp, u14_t ADC_DTA, DspInputOutput* input_output, DspConfig* dsp_cfg)
{
	
	int16_t ret = envp_process(envp, ADC_DTA, input_output);
	if (ret != 0)
		return errorEnvp;
}

void  envp_destroy_api(ENVP_PARAM* envp, DspInputOutput* input_output)
{
	envp_destroy(envp, input_output);
}