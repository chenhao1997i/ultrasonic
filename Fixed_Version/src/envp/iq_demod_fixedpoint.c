// Copyright 2023 Eswin. All Rights Reserved.
// Author: liqilun@eswincomputing.com
#include "dsp_envp.h"
#include "../../include/dsp_dsptypes.h"
s16_t iq_demod_q(ENVP_PARAM* envp, DspInputOutput* input_output, s16_t ADC_DTA)
{
	s32_t temp = 0;
	if (envp->fmode == 4)
	{
		s16_t cos[4] = { 8191,0,-8191,0 };
		s16_t sin[4] = { 0,8191,0,-8191 };

		temp = ADC_DTA * (cos[envp->cic_cnt] << 1);
		envp->yi[envp->cic_cnt] = temp >> 12;
		temp = -ADC_DTA * (sin[envp->cic_cnt] << 1);
		envp->yq[envp->cic_cnt] = temp >> 12;
		//printf("%d  %d\n", envp->yi[envp->cic_cnt], envp->yq[envp->cic_cnt]);
	}
	if (envp->fmode == 6)
	{
		s32_t cos[6] = { 8191,4095,-4095,-8191,-4095,4095 };
		s32_t sin[6] = { 0,7093,7093,0,-7093,-7093 };

		temp = ADC_DTA * (cos[envp->cic_cnt] << 1);
		envp->yi[envp->cic_cnt] = temp >> 12;
		temp = -ADC_DTA * (sin[envp->cic_cnt] << 1);
		envp->yq[envp->cic_cnt] = temp >> 12;
		// printf("sin[%d]:%.20f\n", j, sin[j]);

	}
	if (envp->fmode == 8)
	{
		s16_t cos[8] = { 8191,5723,0,-5723,-8191,-5723,0,5723 };
		s16_t sin[8] = { 0,5723,8191,5723,0,-5723,-8191,-5723 };

		temp = ADC_DTA * (cos[envp->cic_cnt] << 1);
		envp->yi[envp->cic_cnt] = temp >> 12;
		temp = -ADC_DTA * (sin[envp->cic_cnt] << 1);
		envp->yq[envp->cic_cnt] = temp >> 12;

	}
	return 0;
}