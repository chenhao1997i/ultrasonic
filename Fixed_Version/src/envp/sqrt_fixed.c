// Copyright 2023 Eswin. All Rights Reserved.
// Author: liqilun@eswincomputing.com
#include "dsp_envp.h"
#include "../../include/dsp_dsptypes.h"
// 定义定点数的平方根函数
u16_t sqrt_fixed(s32_t x, s32_t y)
{
    u32_t abs_x = ABS(x);
    u32_t abs_y = ABS(y);
    u32_t max = MAX(abs_x, abs_y);
    u32_t min = MIN(abs_x, abs_y);
    u32_t fx = 0;
    //if ((max + min) > 65535)  return 65535;
    if (max > 65535)
    {
        return 65535;
    }
    u32_t fmax = max * max;
    u32_t fmin = min * min;
    if (fmax > 0xffffffff - fmin) return 65535;
    fx = fmax + fmin;
    u32_t x0 = max + (min >> 1);
    u16_t r = (max == 0) ? 0 : (x0 + fx / x0) >> 1;
    return r;
}