// Copyright 2023 Eswin. All Rights Reserved.
// Author: liqilun@eswincomputing.com
#include "dsp_envp.h"
#include "../../include/dsp_dsptypes.h"
s16_t resample_fixed(ENVP_PARAM* envp, DspInputOutput* input_output, s16_t* indata)
{

		s16_t outdata = 0;
		s32_t add = 0;
		for (s16_t j = 0; j < envp->resam_filtlen; ++j)
		{
				
			s16_t k = envp->resam_filtlen - 1 - j;
			s32_t acc = 0;
			
			acc = indata[k] * envp->resam_coefs_fixed[j];
			add = add + acc;
			if (envp->resam_cnt < input_output->envpio.delay * 2 - 1)
				add = 0;
			//printf("%f %f\n", ((float)acc) / ((1 << yiyq_SCALE) * (1 << resam_coefs_SCALE) * (1 << 3) * 1.0f), ((float)indata[k]) / ((1 << yiyq_SCALE) * (1 << 3) * 1.0f));
		}
		if (add >= (1 << 29))
		{
			add = (1 << 29) - 1;
		}
		if (add <= -(1 << 29))
		{
			add = -(1 << 29);
		}
		outdata = (s16_t)(add >> resam_coefs_SCALE);
		//if (ABS(add >> 14) > (1 << 15))
		//printf("<%d> %f %d\n", i/2, ((float)outdata[i/2])/((1 << yiyq_SCALE)  * (1 << 3) * 1.0f), add);
	
		
		
		return outdata;
}