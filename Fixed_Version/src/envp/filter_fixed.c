// Copyright 2023 Eswin. All Rights Reserved.
// Author: liqilun@eswincomputing.com
#include "dsp_envp.h"
#include <stdio.h>
#include "../../include/dsp_dsptypes.h"
s16_t filter_fixed(s16_t* input, s16_t filt_maxlen, ENVP_PARAM* envp, DspInputOutput* input_output)
{

		s16_t output = 0;
		s32_t acc = 0;
		s32_t add = 0;
		int16_t idx = (filt_maxlen < envp->filter_len) ? filt_maxlen : envp->filter_len;
		if (envp->envp_afc == 1) // 1:hardware control filter
		{
			if (input_output->envpio.burst_sta == 2)
			{
				printf("burst aborted due to VTANK overvoltage!");
				exit(-1);
			}
			if (input_output->envpio.burst_sta == 1 || input_output->envpio.rtm_rt_end == 0)//input_output->burst_sta
			{
				for (s16_t j = 0; j < idx; ++j)
				{
					s16_t k = filt_maxlen - 1 - j;

					acc = input[k];
					acc = acc * envp->filtCoeff_tx[j];
					add = add + acc;
					if (add >= (1 << 29))
					{
						add = (1 << 29) - 1;
					}
					if (add <= -(1 << 29))
					{
						add = -(1 << 29);
					}
					output = add >> filtCoeff_SCALE;
				}
			}
			else
				for (s16_t j = 0; j < idx; ++j)
				{
					s16_t k = filt_maxlen - 1 - j;
					acc = input[k];
					acc = acc * envp->filtCoeff_rx[j];
					add = add + acc;
					if (add >= (1 << 29))
					{
						add = (1 << 29) - 1;
					}
					if (add <= -(1 << 29))
					{
						add = -(1 << 29);
					}
					output = add >> filtCoeff_SCALE;

				}
			if (envp->envp_afc_brk == 1)
				envp->envp_afc = 0;
		}
		else if (envp->envp_afc == 0)
			for (s16_t j = 0; j < idx; ++j)
			{
				s16_t k = filt_maxlen - 1 - j;
				acc = input[k];
				acc = acc * envp->filtCoeff_rx[j];
				//printf("%d %f %f\n", i, ((float)input[k]) / ((1 << adc_dta_SCALE) * 1.0f), ((float)acc) / ((1 << adc_dta_SCALE) * (1 << filtCoeff_SCALE) * 1.0f));
				add = add + acc;
				if (add >= (1 << 29))
				{
					add = (1 << 29) - 1;
				}
				if (add <= -(1 << 29))
				{
					add = -(1 << 29);
				}
				output = add >> filtCoeff_SCALE;
				
				//output[i] = output[i] >> 15;
				//printf("%d = %d * %d\n", output[i], input[k], envp->filtCoeff_tx[j]);
			}
	/*	if (ABS(add >> filtCoeff_SCALE) > 0xfff)printf("%d \n", add >> filtCoeff_SCALE);*/
			
		//if (ABS(add >> 8) > (1 << 15))
		//printf("%d %d %f\n", start_point, i, ((float)output[i])/((1 << adc_dta_SCALE) * 1.0f));
		return output;
}