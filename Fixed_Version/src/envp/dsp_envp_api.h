﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#ifndef _ENVP_API_H_
#define _ENVP_API_H_
#include "../../include/dsp_algo_api.h"
#include "../../include/dsp_dsptypes.h"
#ifdef __cplusplus 
extern "C" {
#endif

void envp_init_api(ENVP_PARAM* envp, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output);
errorReturn envp_process_api(ENVP_PARAM* envp, u14_t ADC_DTA, DspInputOutput* input_output, DspConfig* dsp_cfg);
void envp_destroy_api(ENVP_PARAM* envp, DspInputOutput* input_output);

#ifdef __cplusplus 
}
#endif
#endif 
