// Copyright 2023 Eswin. All Rights Reserved.
// Author: liqilun@eswincomputing.com
#include "dsp_envp.h"
#include "../../include/dsp_dsptypes.h"
s16_t cic_simulat_fixed(s16_t R, u32_t factor, s16_t* xn)
{

	s16_t  IntegratorIn = 0;
	s32_t  IntegratorBuffer = 0;
	for (s16_t i = 0; i < R; i++)
	{
		IntegratorIn = xn[i] ;
		IntegratorBuffer = IntegratorIn + IntegratorBuffer;
	}

	s16_t y = (factor * IntegratorBuffer) >> (extract_factor_SCALE - 1); //The averaging process does not overflow
	return y;
}