﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#ifndef _NFD_API_H_
#define _NFD_API_H_
#include "../../include/dsp_algo_api.h"
#include "../../include/dsp_dsptypes.h"
#include "dsp_nfd.h"
#ifdef __cplusplus 
extern "C" {
#endif
	void nfd_init_api(NFD_PARAM* nfd, DspConfig* dsp_cfg);
	errorReturn nfd_process_api(NFD_PARAM* nfd, NfdIO* nfd_io, u16_t* DSP_IRQ_STATUS);
	void nfd_destroy_api(NFD_PARAM* nfd);
#ifdef __cplusplus 
}
#endif
#endif