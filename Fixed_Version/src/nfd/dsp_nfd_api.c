﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_nfd_api.h"
#include "../../include/dsp_dsptypes.h"

void nfd_init_api(NFD_PARAM* nfd, DspConfig* dsp_cfg)
{
	nfd_init(nfd, dsp_cfg);
}

errorReturn nfd_process_api(NFD_PARAM* nfd, NfdIO* nfd_io, u16_t* DSP_IRQ_STATUS)
{
	u1_t ret = nfd_process(nfd, nfd_io, DSP_IRQ_STATUS);
	if (ret != 0)
		return errorFtc;
}

void nfd_destroy_api(NFD_PARAM* nfd)
{
	nfd_destroy(nfd);
}