﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#ifndef _DSP_NFD_H_
#define _DSP_NFD_H_
#include "../../include/dsp_public.h"
#include "../../include/dsp_dsptypes.h"
#ifdef __cplusplus
extern "C" {
#endif
typedef struct
{
	//input
	u16_t nfd_th;
	//output
	u16_t nfd_evt;
	u16_t nfd_echos[5];
	u16_t nfd_ts[5];

	//parameter
	u16_t nfd_sens;
	u16_t nfd_irq_cfg;
	u16_t echos_num;
	u16_t nfd_max_num;
	u1_t nfd_flag;
	u16_t fm_data;
	u16_t  fm_ms_ts;
	u16_t positive_cnt;
} NFD_PARAM;

void nfd_init(NFD_PARAM* ptr_nfd, DspConfig* dsp_cfg);
u1_t nfd_process(NFD_PARAM* ptr_nfd, NfdIO* nfd_io, u16_t* DSP_IRQ_STATUS);
void nfd_destroy(NFD_PARAM* ptr_nfd);

#ifdef __cplusplus
}
#endif
#endif //_DSP_NFD_H_