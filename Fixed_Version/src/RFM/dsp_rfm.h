﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#ifndef _DSP_RFM_H_
#define _DSP_RFM_H_
#include "../../include/dsp_public.h"
#include "../../include/dsp_dsptypes.h"
#ifdef __cplusplus
extern "C" {
#endif
typedef struct
{
	u16_t RFM_CTRL;
	u1_t rfm_status;
	u5_t rfm_width;
	u5_t rfm_start;
	u6_t RFM_CROSS_COUNT;
	u6_t RFM_COUNTS;
	u10_t RFM_SAMPLES;
	u14_t ls_ADC_DTA;
	//u16_t npulses;
	//u16_t busrt_count;
	u14_t bias;
} RFM_PARAM;



void rfm_init(RFM_PARAM* ptr_rfm, DspConfig* dsp_cfg);
u1_t rfm_process(RFM_PARAM* ptr_rfm, RfmIO* rfm_io, u16_t* DSP_IRQ_STATUS);
void rfm_destroy(RFM_PARAM* ptr_rfm);


#ifdef __cplusplus
}
#endif
#endif //_DSP_RFM_H_