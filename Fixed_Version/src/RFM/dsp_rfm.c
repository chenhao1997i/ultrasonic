﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: baiyangliu@eswincomputing.com
#include "dsp_rfm.h"
#include "../../include/dsp_dsptypes.h"
void rfm_init(RFM_PARAM* rfm, DspConfig* dsp_cfg)
{
	rfm->RFM_CTRL = dsp_cfg->RFM_CTRL;
	//rfm->npulses = dsp_cfg->npulse;
	rfm->bias = dsp_cfg->bias;
	rfm->rfm_status = (rfm->RFM_CTRL&(1<<10))>>10;
	rfm->rfm_width = (rfm->RFM_CTRL&(31<<5))>>5;
	rfm->rfm_start = rfm->RFM_CTRL&15;
	//printf("init:%d:%d:%d:%d\n", (rfm->RFM_CTRL&15), ((rfm->RFM_CTRL&(31 << 5))>>5), ((rfm->RFM_CTRL&(1<<10))>> 10), dsp_cfg->RFM_CTRL);
	rfm->RFM_CROSS_COUNT = 0;
	rfm->RFM_COUNTS = 0;
	rfm->RFM_SAMPLES = 0;
	rfm->ls_ADC_DTA = 0;
	//rfm->busrt_count = 0;


}

void rfm_destroy(RFM_PARAM* rfm)
{
	free(rfm);
}

u1_t rfm_process(RFM_PARAM* rfm, RfmIO* rfm_io,u16_t* DSP_IRQ_STATUS)
{
	if (rfm_io->adc_dta < rfm->bias)
	{
		if (rfm->ls_ADC_DTA >= rfm->bias)//negative crossing
		{
			if (rfm->rfm_status == 1)
			{
				if (rfm_io->burst_sta == 0)rfm->rfm_status = 0;//wait for burst to end
			}
			else
			{
				rfm->RFM_CROSS_COUNT += 1;
			}
		}
	}
	if (rfm->rfm_status == 0)
	{
		if (rfm->rfm_start <= rfm->RFM_CROSS_COUNT)
		{
			rfm->RFM_SAMPLES += 1;
			rfm->RFM_COUNTS = rfm->RFM_CROSS_COUNT - rfm->rfm_start;
		}
		if (rfm->RFM_COUNTS >= rfm->rfm_width)rfm->rfm_status = 1;
	}
	rfm->ls_ADC_DTA = rfm_io->adc_dta;
	rfm_io->rfm_status = rfm->rfm_status;
	rfm_io->RFM_CROSS_COUNT = rfm->RFM_CROSS_COUNT;
	rfm_io->RFM_COUNTS = rfm->RFM_COUNTS;
	rfm_io->RFM_SAMPLES = rfm->RFM_SAMPLES;
	//printf("%d:%d:%d\n", rfm->ls_ADC_DTA, rfm->RFM_SAMPLES, rfm->RFM_CROSS_COUNT);
	return 0;
}