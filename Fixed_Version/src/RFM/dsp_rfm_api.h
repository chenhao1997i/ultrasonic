﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: chenjingjing@eswincomputing.com
#ifndef _RFM_API_H_
#define _RFM_API_H_
#include "../../include/dsp_algo_api.h"
#include "../../include/dsp_dsptypes.h"
#include "../../include/dsp_public.h"
#include "dsp_rfm.h"
#ifdef __cplusplus 
extern "C" {
#endif
	void rfm_init_api(RFM_PARAM* rfm, DspConfig* dsp_cfg);
	errorReturn rfm_process_api(RFM_PARAM* ptr_rfm, RfmIO* rfm_io, u16_t* DSP_IRQ_STATUS);
	void  rfm_destroy_api(RFM_PARAM* rfm);
#ifdef __cplusplus 
}
#endif
#endif 
