﻿// Copyright 2022 Eswin. All Rights Reserved.r
// Author: chenjingjing@eswincomputing.com
#include "dsp_rfm_api.h"
#include "../../include/dsp_dsptypes.h"
#include "../../include/dsp_public.h"
#include "../../include/dsp_algo_api.h"
void rfm_init_api(RFM_PARAM* rfm, DspConfig* dsp_cfg)
{
	rfm_init(rfm, dsp_cfg);
}

errorReturn rfm_process_api(RFM_PARAM* ptr_rfm, RfmIO* rfm_io, u16_t* DSP_IRQ_STATUS)
{
	int16_t ret = rfm_process((RFM_PARAM*)ptr_rfm, rfm_io, DSP_IRQ_STATUS);
	if (ret != 0)
		return errorNfd;
}

void  rfm_destroy_api(RFM_PARAM* rfm)
{
	rfm_destroy(rfm);
}




