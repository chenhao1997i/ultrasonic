﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_ampd.h"
#include "../../include/dsp_dsptypes.h"

u1_t stc_control(AMPD_PARAM* ptr_ampd, AmpdIO* input_output)
{
	if (ptr_ampd->stc_tb > 0)
	{
		if (ptr_ampd->g_dig < ptr_ampd->g_stc_target)
		{
			u32_t ts_div_tb = input_output->ms_ts / ptr_ampd->stc_tb;
			u32_t ls_ts_div_tb = ptr_ampd->ls_ms_ts / ptr_ampd->stc_tb;
			u16_t g_dig_add= ts_div_tb - ls_ts_div_tb;
			if (g_dig_add>0)
			//if (input_output->ms_ts / ptr_ampd->stc_tb> ptr_ampd->ls_ms_ts / ptr_ampd->stc_tb)
			{
				ptr_ampd->g_dig += g_dig_add;
				if (ptr_ampd->g_dig > ptr_ampd->g_stc_target)
					ptr_ampd->g_dig = ptr_ampd->g_stc_target;
				//ptr_ampd->g_dig += 1;
				if (ptr_ampd->stc_mod == 1)
				{
					ptr_ampd->step_cnt += g_dig_add;//increase step when tb is reached
					if (ptr_ampd->step_cnt >= 16)
					{
						ptr_ampd->step_cnt = 0;
						ptr_ampd->stc_tb = ptr_ampd->stc_tb * 2;
					}

				}
			}
			//ptr_ampd->stc_gain = ptr_ampd->step_cnt * ptr_ampd->g_dig_step;
			//printf("%f:%f:%f:%f\n", floor(input_output->ms_ts / ptr_ampd->stc_tb), floor(ptr_ampd->ls_ms_ts / ptr_ampd->stc_tb), ptr_ampd->stc_gain, ptr_ampd->g_stc_target);
		}
	}
	ptr_ampd->ls_ms_ts = input_output->ms_ts;
	return 0;
}


void ampd_init(AMPD_PARAM* ptr_ampd, DspConfig* dsp_cfg)
{
	ptr_ampd->g_dig = dsp_cfg->AMPD_CTRL & ((1<<8)-1);
	//ptr_ampd->stc_tb = dsp_cfg->STC_TB;
	ptr_ampd->stc_tb = 0;//will be to given value after stc_start,stc_start is a a parameter in software
	ptr_ampd->stc_mod = (dsp_cfg->STC_CTRL&(1<<7))>>7;
	ptr_ampd->g_stc_target = dsp_cfg->STC_CTRL&((1<<7)-1);
	//printf("%d:%d:%d:%d\n", ptr_ampd->g_dig, ptr_ampd->stc_mod, ptr_ampd->g_stc_target, ((1 << 7) - 1));
	ptr_ampd->step_cnt = 0;
	ptr_ampd->ls_ms_ts = 0;
	ptr_ampd->limit = 256 - 1;
	ptr_ampd->ENV_FTC_shift=0;

}

void ampd_destroy(AMPD_PARAM* ptr_ampd)
{
	free(ptr_ampd);
}

u1_t amplifier(AMPD_PARAM* ptr_ampd, AmpdIO* input_output)
{
	//printf("%d:%f:%f:%f\n", ptr_ampd->step_cnt, ptr_ampd->g_dig , ptr_ampd->g_dig * ptr_ampd->step_cnt,input_output->ms_ts);
	//printf("%d\n",gain_array[ptr_ampd->g_dig]);
	//printf("dig: %d\n", ptr_ampd->g_dig);
	ptr_ampd->ENV_FTC_shift = input_output->ENV_FTC;
	ptr_ampd->ENV_FTC_shift = ptr_ampd->ENV_FTC_shift * gain_array[ptr_ampd->g_dig];
	ptr_ampd->ENV_FTC_shift = ptr_ampd->ENV_FTC_shift >> 16;//8bits for env_ftc,8bits for gain
	return 0;
}

u1_t limiter(AMPD_PARAM* ptr_ampd, AmpdIO* input_output)
{
	input_output->AMPD_ENV = (ptr_ampd->ENV_FTC_shift > ptr_ampd->limit) ? ptr_ampd->limit : ptr_ampd->ENV_FTC_shift;
	//printf("%d\n", input_output->AMPD_ENV);
	return 0;
}

u1_t ampd_process(AMPD_PARAM* ptr_ampd, AmpdIO* input_output)
{
	stc_control(ptr_ampd, input_output);
	amplifier(ptr_ampd, input_output);
	limiter(ptr_ampd, input_output);
	//printf("msts:%d,TB:%d\n", input_output->ms_ts, ptr_ampd->stc_tb);
	return 0;
}
