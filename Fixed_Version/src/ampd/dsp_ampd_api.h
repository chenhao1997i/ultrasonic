﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#ifndef _AMPD_API_H_
#define _AMPD_API_H_
#include "../../include/dsp_algo_api.h"
#include "../../include/dsp_dsptypes.h"

#ifdef __cplusplus 
extern "C" {
#endif

void ampd_init_api(AMPD_PARAM* ampd, DspConfig* dsp_cfg);
errorReturn ampd_process_api(AMPD_PARAM* ampd, AmpdIO* input_output);
void  ampd_destroy_api(AMPD_PARAM* ampd);

#ifdef __cplusplus 
}
#endif
#endif 
