﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#ifndef _DSP_AMPD_H_
#define _DSP_AMPD_H_
#include "../../include/dsp_public.h"
#include "../../include/dsp_dsptypes.h"
#ifdef __cplusplus
extern "C" {
#endif


	typedef struct
	{
		//input
		u7_t g_dig;
		u7_t g_stc_target;
		u11_t stc_tb;
		u1_t stc_mod;
		u16_t step_cnt;
		u32_t ENV_FTC_shift;
		u8_t limit;
		u16_t ls_ms_ts;
	}AMPD_PARAM;

	void ampd_init(AMPD_PARAM* ptr_ampd, DspConfig* dsp_cfg);
	u1_t ampd_process(AMPD_PARAM* ptr_ampd, AmpdIO* input_output);
	void ampd_destroy(AMPD_PARAM* ptr_ampd);


#ifdef __cplusplus
}
#endif
#endif //_DSP_AMPD_H_