﻿// Copyright 2023 Eswin. All Rights Reserved.
// Author: pankaida@eswincomputing.com
#include "dsp_ampd_api.h"
#include "dsp_ampd.h"
#include "../../include/dsp_dsptypes.h"
void ampd_init_api(AMPD_PARAM* ampd,DspConfig* dsp_cfg)
{
	ampd_init(ampd, dsp_cfg);
}

errorReturn ampd_process_api(AMPD_PARAM* ampd, AmpdIO* input_output)
{
	u1_t ret = ampd_process(ampd, input_output);
	if (ret != 0)
		return errorAmpd;
}

void  ampd_destroy_api(AMPD_PARAM* ampd)
{
	ampd_destroy(ampd);
}





