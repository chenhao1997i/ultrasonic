﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com
#include "../include/dsp_algo_api.h"
#include "../src/envp/dsp_envp_api.h"
#include "../src/edet/dsp_edet_api.h"
#include "../src/ampd/dsp_ampd_api.h"
#include "../src/nfd/dsp_nfd_api.h"
#include "../src/ftc/dsp_ftc_api.h"
#include "../src/RFM/dsp_rfm_api.h"
#include "../src/interruptprocess/interruptprocess.h"

void dsp_init_api(DspParam* inst, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	envp_init_api(&inst->envp, sample_rate, dsp_cfg, input_output);
	ftc_init_api(&inst->ftc, dsp_cfg);
	ampd_init_api(&inst->ampd, dsp_cfg);
	edet_init_api(&inst->edet, dsp_cfg);
	nfd_init_api(&inst->nfd, dsp_cfg);
	rfm_init_api(&inst->rfm, dsp_cfg);
}

int16_t dsp_process_api(DspParam *inst, u16_t in_data, DspInputOutput* input_output, DspConfig* dsp_cfg) 
{
	
		/*rfm*/
		input_output->rfm_io.adc_dta = in_data;
		input_output->rfm_io.burst_sta = 0;
		rfm_process_api(&inst->rfm, &input_output->rfm_io, &input_output->DSP_IRQ_STATUS);
		/*envp*/
		envp_process_api(&inst->envp, in_data, input_output, dsp_cfg);
		if (input_output->frame_num > input_output->envpio.delay * dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1) && input_output->frame_num % (dsp_cfg->fmode * (dsp_cfg->dsr_sel + 1)) == 0) {
			/*nfd*/
			input_output->ftc_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
			ftc_process_api(&inst->ftc, &input_output->ftc_io);
			//printf("%d, %d  \n", input_output->envpio.ENVP_ENV_RAW, input_output->ENV_FTC);
			input_output->ampd_io.ms_ts = input_output->ms_ts;
			input_output->ampd_io.ENV_FTC = input_output->ftc_io.ENV_FTC;
			//set STC_TB for AMPD
			/*ampd*/
			ampd_process_api(&inst->ampd, &input_output->ampd_io);
			//printf("%d  ", input_output->ampd_io.AMPD_ENV);
			/*edet*/
			input_output->edet_io.AMPD_ENV = input_output->ampd_io.AMPD_ENV;
			input_output->edet_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
			input_output->edet_io.filt1 = input_output->envpio.filt1;
			input_output->edet_io.filt2 = input_output->envpio.filt2;
			input_output->edet_io.conf1 = input_output->envpio.conf1;
			input_output->edet_io.conf2 = input_output->envpio.conf2;
			input_output->edet_io.ms_ts = input_output->ms_ts;
			//printf("ms_ts: %f\n", input_output->ENVP_ENV_RAW_point);
			edet_process_api(&inst->edet, &input_output->edet_io, input_output->DSP_IRQ_STATUS);
			//printf("%d %d %d %d %d\n", input_output->edet_io.ms_ts, input_output->edet_io.ENVP_ENV_RAW, input_output->edet_io.AMPD_ENV, input_output->edet_io.STG_TH, input_output->edet_io.atg_th);
			//printf("ms_ts:%d,AMPD:%d,ENVP_ENV_RAW:%d,STG:%d,ATG:%d\n", input_output->edet_io.ms_ts, input_output->edet_io.AMPD_ENV, input_output->edet_io.ENVP_ENV_RAW, input_output->edet_io.STG_TH, input_output->edet_io.atg_th);
			/*nfd*/
			input_output->nfd_io.ENVP_ENV_RAW = input_output->envpio.ENVP_ENV_RAW;
			input_output->nfd_io.ms_ts = input_output->ms_ts;
			input_output->nfd_io.rtm_rt_end = 1;
			input_output->nfd_io.burst_en = input_output->burst_en;
			nfd_process_api(&inst->nfd, &input_output->nfd_io, input_output->DSP_IRQ_STATUS);
		}

	interrupt_process(input_output->DSP_IRQ_STATUS);
	
	return 0;
}

void dsp_destroy_api(DspParam* inst, DspConfig* dsp_cfg, DspInputOutput* input_output)
{
	/*envp_destroy_api(&inst->envp, input_output);
	ftc_destroy_api(&inst->ftc);
	ampd_destroy_api(&inst->ampd);
	edet_destroy_api(&inst->edet);
	nfd_destroy_api(&inst->nfd);
	rfm_destroy_api(&inst->rfm);*/
}
