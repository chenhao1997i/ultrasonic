﻿// Copyright 2022 Eswin. All Rights Reserved.
// Author: qinyaguang@eswincomputing.com

#ifndef _DSP_ALGO_API_H_
#define _DSP_ALGO_API_H_
#include "../src/edet/dsp_edet.h"
#include "../src/ampd/dsp_ampd.h"
#include "../src/nfd/dsp_nfd.h"
#include "../src/ftc/dsp_ftc.h"
#include "../src/RFM/dsp_rfm.h"
#include "../src/envp/dsp_envp.h"
#include "dsp_public.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    EDET_PARAM  edet;
    FTC_PARAM ftc;
    AMPD_PARAM ampd;
    NFD_PARAM nfd;
    RFM_PARAM rfm;
    ENVP_PARAM envp;

    u1_t envp_key;
    u1_t ampd_key;
    u1_t edet_key;
    u1_t nfd_key;
    u1_t rfm_key;
} DspParam;
/*
    input:
        -len: samples per frame
        -sample_rate: sample rate at least 4fc(fc：58kHz)
*/

//overall processing
void dsp_init_api(DspParam* inst, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output);
int16_t dsp_process_api(DspParam* inst, u16_t in_data, DspInputOutput* input_output, DspConfig* dsp_cfg);
void dsp_destroy_api(DspParam* inst, DspConfig* dsp_cfg, DspInputOutput* input_output);
//block processing
void dsp_init_block_api(DspParam* inst, int32_t sample_rate, DspConfig* dsp_cfg, DspInputOutput* input_output, int* mod_switch);
int16_t dsp_process_block_api(DspParam* inst, u16_t in_data, DspInputOutput* input_output, DspConfig* dsp_cfg);
void dsp_destroy_block_api(DspParam* inst, DspConfig* dsp_cfg, DspInputOutput* input_output);

#ifdef __cplusplus
}
#endif
#endif// _DSP_ALGO_API_H_