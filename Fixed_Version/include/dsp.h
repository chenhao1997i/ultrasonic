#include "dsp_algo_api.h"

typedef struct {
    DspParam inst;
    DspConfig cfg;
    DspInputOutput io;
    uint32_t sample_rate;
} dsp_config_t;

void dspGetConfig(const char *file, dsp_config_t* dsp);
void dspReleaseConfig(dsp_config_t *dsp);
int dsp_process(dsp_config_t *dsp, uint16_t sample, uint32_t timestamp, u1_t* mod_switch);