#ifndef _DSP_PUBLIC_H_
#define _DSP_PUBLIC_H_
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include "dsp_dsptypes.h"

#ifdef __cplusplus
extern "C" {
#endif
#define M_PI 3.14159265358979323846

#ifndef profile
    //#define	profile "../cJSON/tests/profileSA.json"
    //#define	profile "../cJSON/tests/profileSB.json"
    //#define	profile "../cJSON/tests/profileSC.json"
    #define	profile "../cJSON/tests/profileAA.json"
    //#define	profile "../cJSON/tests/profileAB.json"
#endif



//envp
#ifndef extract_factor_SCALE
    #define	extract_factor_SCALE 14
#endif
#ifndef resam_coefs_SCALE
    #define resam_coefs_SCALE 14
#endif
#ifndef filtCoeff_SCALE
    #define	filtCoeff_SCALE 13
#endif
#ifndef adc_dta_SCALE
    #define	adc_dta_SCALE 13
#endif

//ftc
#ifndef Coeff_SCALE
    #define Coeff_SCALE  8
#endif

//ampd
#ifndef AMPD_ENV_SCALE
    #define AMPD_ENV_SCALE  8
#endif
#ifndef ENVP_ENV_FTC_SCALE
    #define ENVP_ENV_FTC_SCALE  32768   //Q = 15
#endif
#ifndef ENVP_ENV_RAW_SCALE
    #define ENVP_ENV_RAW_SCALE  65536   //Q = 16
#endif

//edet
#ifndef AMPD_ENV_SCALE
    #define AMPD_ENV_SCALE  8
#endif
#ifndef filt_SCALE
    #define filt_SCALE  0
#endif
#ifndef ENVP_ENV_RAW_SCALE
    #define ENVP_ENV_RAW_SCALE  0
#endif
#ifndef STG_TB_SCALE
    #define STG_TB_SCALE  10
#endif
#ifndef STG_TH_SCALE
    #define STG_TH_SCALE  8
#endif
#ifndef STG_STEP_SCALE
    #define STG_STEP_SCALE  8
#endif
#ifndef atg_tau_SCALE
    #define atg_tau_SCALE  14 
#endif
#ifndef atg_ini_SCALE
    #define atg_ini_SCALE  8 
#endif
#ifndef atg_alpha_SCALE
    #define atg_alpha_SCALE  13
#endif
#ifndef stg_th_SCALE
    #define stg_th_SCALE  8 
#endif
#ifndef atg_th_SCALE
    #define atg_th_SCALE  8 
#endif
    //aatg scale
#ifndef rec_aatg_cw_SCALE
    #define rec_aatg_cw_SCALE  8
#endif
#ifndef aatg_alpha_SCALE
    #define aatg_alpha_SCALE  1
#endif

    static u16_t gain_array[] = {256,267,279,291,304,318,332,346,362,378,395,412,430,450,470,490,512,535,559,583,609,636,665,694,725,757,791,826,863,901,941,983,1026,1072,1119,1169,1221,1275,1332,1391,1452,1517,1584,1654,1728,1805,1885,1968,2056,2147,2242,2342,2445,2554,2667,2786,2909,3038,3173,3314,3461,3614,3775,3942,4117,4300,4490,4690,4898,5115,5342,5579,5826,6085,6355,6637,6931,7239,7560,7895,8245,8611,8993,9392,9809,10244,10698,11173,11669,12186,12727,13291,13881,14497,15140,15812,16513,17246,18011,18810,19644,20515,21425,22376,23368,24405,25488,26618,27799,29032,30320,31665,33070,34537,36069,37669,39340,41085,42908,44811,46799,48875,51043,53307,55672,58142,60721,63414};

    typedef enum
    {
        errorEnvp,
        errorEdet,
        errorAmpd,
        errorFtc,
        errorNfd,
    } errorReturn;

    typedef struct {
        u4_t coeff[64];
        int16_t length;
    } genAdvCoeffReturn;


    typedef struct
    {
        //BRG
        u15_t brg_fc;

        //RFM
        u16_t RFM_CTRL;
        u16_t bias;
        
        // envp
        //ENVP_CFG
        u1_t fspi_sel;
        u1_t aspi_sel;
        u1_t dsr_sel;
        u5_t g_cal;
        u1_t envp_afc; // Enable auto bandwidth during burst
        u3_t envp_cfg;
        //ENVP_CTRL
        u1_t envp_restart;// Restart envelope processor (set input multiplier state-counter to zero)
        u1_t envp_afc_brk;
        s32_t fc;
        s16_t fmode;
        u16_t ENVP_CFG;
        u16_t ENVP_CTRL;
        u16_t ENVP_FILT_CFG;


        // ftc
        u16_t FTC_CTRL;
        // ampd
        u16_t STC_START;
        u16_t AMPD_CTRL;
        u16_t STC_CTRL;
        u16_t STC_TB;
        u16_t framelen;
        //edet
        u16_t ATG_CTRL;
        u16_t EEVAL_CTRL;
        //s16_t eeval_dma_val;
        //s16_t eeval_sens;
        //s16_t eeval_sel;
        u16_t AATG1_CTRL;
        u16_t AATG2_CTRL;
        //s16_t aatg_cw;
        //s16_t aatg_cn;
        // nfd
        u16_t nfd_th;        
        u16_t NFD_CTRL;
        //float ring_time;
        s16_t measure_mod;
        //s32_t ring_bin;

        

        //fix or advanced path
        bool fix_path_process;
        s16_t npulse;
        int16_t bandPass;

        int16_t f1_coeff_sel;
        int16_t f2_coeff_sel;
        int16_t filt_len_code;
        int16_t filt_len_fix;
        genAdvCoeffReturn COEFF;
    } DspConfig;


    typedef struct
    {
        //in
        u16_t ENVP_ENV_RAW;
        u16_t ms_ts;
        u16_t STG_TB;
        s16_t STG_STEP;
        u16_t AMPD_ENV;
        u16_t filt1;
        u16_t filt2;
        u16_t conf1;
        u16_t conf2;
        u16_t ATG_TH;
        //out
        u32_t std_data_packet;
        u32_t adv_data_packet1;
        u32_t adv_data_packet2;
        u16_t atg_th;
        u8_t STG_TH;
        u16_t aatg1_th;
        u16_t aatg2_th;
        u16_t aatg_filt1;
        u16_t aatg_filt2;
        u16_t aatg_conf1;
        u16_t aatg_conf2;

    } EdetIO;

    typedef struct
    {
        //in
        u16_t ENVP_ENV_RAW;
        u16_t ms_ts;
        u1_t burst_en;
        u1_t rtm_rt_end;
        //out
        u16_t NFD_TS1;
        u16_t NFD_TS2;
        u16_t NFD_TS3;
        u16_t NFD_TS4;
        u16_t NFD_TS5;
        u16_t NFD_ECHO1;
        u16_t NFD_ECHO2;
        u16_t NFD_ECHO3;
        u16_t NFD_ECHO4;
        u16_t NFD_ECHO5;

    } NfdIO;


    typedef struct
    {
        //in
        u1_t adc_eoc;
        u1_t burst_sta;//BRG give out 
        u16_t delay;
        u1_t rtm_rt_end;

        //out
        s16_t env_i;
        s16_t env_q;
        u16_t ENVP_ENV_RAW;
        s16_t envd_raw;
        s8_t env_phi;
        s8_t env_phid;
        u16_t filt1;
        u4_t conf1;
        u16_t filt2;
        u4_t conf2;
    } EnvpIO;

    typedef struct
    {
        //in
        u16_t ENVP_ENV_RAW;
        u16_t ENV_FTC;
    } FtcIO;

    typedef struct
    {
        //in
        u16_t ms_ts;
        u16_t ENV_FTC;
        //out
        u8_t AMPD_ENV;
    } AmpdIO;

    typedef struct
    {
        //in
        u14_t adc_dta;
        u2_t burst_sta;
        //out
        u1_t  rfm_status;
        u6_t RFM_CROSS_COUNT;
        u6_t RFM_COUNTS;
        u10_t RFM_SAMPLES;
    } RfmIO;

    typedef struct
    {
        u16_t framelen;
        u16_t ms_ts;
        u32_t inputlen;

        //rfm
        RfmIO rfm_io;
        // envp
        EnvpIO envpio;

        // ftc
        FtcIO ftc_io;
        //u16_t ENV_FTC;
        // edet
        EdetIO edet_io;
        //nfd
        NfdIO nfd_io;
        // ampd
        AmpdIO ampd_io;
        //u8_t AMPD_ENV[64];
        // nfd
        u1_t burst_en;

        //float* NFD_ECHO;
        //float* NFD_TS;
        //u16_t NFD_ECHO[5];
        //u16_t NFD_TS[5];
    


        //interrupt
        u16_t DSP_IRQ_STATUS[1];

        //shift
        s16_t frame_num;

        

    } DspInputOutput;

#ifdef __cplusplus
}
#endif
#endif // _DSP_PUBLIC_H_
