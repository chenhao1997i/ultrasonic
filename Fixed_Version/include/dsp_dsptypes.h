#ifndef _DSP_DSPTYPES_H_
#define _DSP_DSPTYPES_H_

#include <inttypes.h>

typedef unsigned long long u64_t;  //uint64_t
typedef u64_t          u40_t;
typedef unsigned long  u32_t;  //uint32_t
typedef u32_t          u31_t;
typedef u32_t          u30_t;
typedef u32_t          u29_t;
typedef u32_t          u28_t;
typedef u32_t          u27_t;
typedef u32_t          u26_t;
typedef u32_t          u25_t;
typedef u32_t          u24_t;
typedef u32_t          u23_t;
typedef u32_t          u22_t;
typedef u32_t          u21_t;
typedef u32_t          u20_t;
typedef u32_t          u19_t;
typedef u32_t          u18_t;
typedef u32_t          u17_t;

typedef unsigned short  u16_t;  //uint16_t
typedef u16_t          u15_t;
typedef u16_t          u14_t;
typedef u16_t          u13_t;
typedef u16_t          u12_t;
typedef u16_t          u11_t;
typedef u16_t          u10_t;
typedef u16_t          u9_t;


typedef unsigned char  u8_t;  //uint8_t
typedef u8_t          u7_t;
typedef u8_t          u6_t;
typedef u8_t          u5_t;
typedef u8_t          u4_t;
typedef u8_t          u3_t;
typedef u8_t          u2_t;
typedef u8_t          u1_t;


typedef long   s32_t; //signed int32
typedef short  s16_t; //signed int16
typedef s16_t  s15_t;
typedef s16_t  s14_t;
typedef s16_t  s13_t;
typedef s16_t  s12_t;
typedef s16_t  s11_t;
typedef s16_t  s10_t;
typedef s16_t  s9_t;
typedef s16_t  s8_t; //signed int8

#ifndef sign
#define sign(a,b) ((a) > (b) ? 1 : -1)
#endif // !sign

#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif // !MAX

#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif // !MIN

#ifndef ABS
#define ABS(a) ((a) > (0) ? (a) : -(a))
#endif // !ABS


#endif // !_DSPCODE_DSPTYPES_H_
