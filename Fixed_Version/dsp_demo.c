#include "./include/dsp.h"
#include "./cJSON/readjson.h"
void generateAdvCoeff(genAdvCoeffReturn* result, int32_t fc, int16_t B, int16_t npulse, int16_t dsr_sel) {
    int16_t N2;
    if (dsr_sel == 0)
        N2 = npulse;
    else
        N2 = (npulse - 1) / 2 + 1;
    float t_down[127];
    float freq_base[127];
    float upi[127];
    float upq[127];
    float tao = (float)npulse / fc;
    for (int16_t i = 0; i < N2; ++i) {
        if (N2 == 1)
            t_down[i] = -tao / 2.0;
        else
            t_down[i] = i / (N2 - 1.0) * tao - tao / 2.0;
        freq_base[i] = B / tao * t_down[i];
        upi[i] = cos(M_PI * freq_base[i] * t_down[i]);
        upq[i] = sin(M_PI * freq_base[i] * t_down[i]);
    }
    int len_half = (N2 - 1) / 2 + 1;
    for (int i = 0; i < len_half; ++i)
    {
        if (i < len_half / 2)
        {
            result->coeff[i] = upi[2 * i] * 8.0f + 8;
        }
        else
        {
            result->coeff[i] = -upq[(i - len_half / 2) * 2] * 8.0f + 8;
        }

    }
    result->length = len_half;
}

void dspGetConfig(const char *file, dsp_config_t* dsp)
{
    //(void)file;
    //dsp_config_t* dsp = calloc(1, sizeof(dsp_config_t));

    if (NULL == dsp) {
        printf("calloc error\n");
        return NULL;
    }
    if (NULL == file) {
        printf("Get default dsp config\n");
        

        dsp->cfg.fix_path_process = false;
        dsp->sample_rate = 348000;
        /*cfg for modules*/
        // RFM
        dsp->cfg.bias = 8192;
        dsp->cfg.RFM_CTRL = (1 << 10) + (8 << 5) + 4; // 0-4 bit rfmstart 5-9 bit rfm_width 10 bit rfmstatus
       //envp
        dsp->cfg.dsr_sel = 1;
        dsp->cfg.envp_cfg = 3;
        dsp->cfg.envp_afc = 0;
        dsp->cfg.fspi_sel = 1;
        dsp->cfg.g_cal = 16;
        dsp->cfg.aspi_sel = 0;//advanced path input:0	
        dsp->cfg.ENVP_CFG = (dsp->cfg.envp_cfg & 0x07) | ((dsp->cfg.envp_afc & 0x01) << 3) | ((dsp->cfg.g_cal & 0x1f) << 4) | ((dsp->cfg.dsr_sel & 0x1) << 9) | ((dsp->cfg.fspi_sel & 0x1) << 10) | ((dsp->cfg.aspi_sel & 0x1) << 11);
        dsp->cfg.envp_restart = 0;
        dsp->cfg.envp_afc_brk = 0;
        dsp->cfg.ENVP_CTRL = (dsp->cfg.envp_afc_brk & 0x01) | ((dsp->cfg.envp_restart & 0x01) << 1);
        dsp->cfg.fc = 58000;
        dsp->cfg.fmode = dsp->sample_rate / dsp->cfg.fc;
        dsp->cfg.npulse = 64;
        dsp->cfg.bandPass = 8000;
        dsp->cfg.f1_coeff_sel = 0;
        dsp->cfg.f2_coeff_sel = 0;
        if (dsp->cfg.dsr_sel == 1)
        {
            dsp->cfg.filt_len_code = (dsp->cfg.npulse - 1) / 2;
        }
        else
        {
            dsp->cfg.filt_len_code = dsp->cfg.npulse - 1;
        }
        dsp->cfg.filt_len_fix = dsp->cfg.filt_len_code;
        dsp->cfg.COEFF.length = 0;
        for (int i = 0; i < 64; i++)
        {
            dsp->cfg.COEFF.coeff[i] = 0;
        }
        generateAdvCoeff(&dsp->cfg.COEFF, dsp->cfg.fc, dsp->cfg.bandPass, dsp->cfg.npulse, dsp->cfg.dsr_sel);
        // ampd
        dsp->cfg.STC_START = 0;//software config,SPMdemo default parameter shows 1024,but seems to be set to 0 anyway.
        dsp->cfg.AMPD_CTRL = 10;//default 10
        dsp->cfg.STC_CTRL = 98 + (1 << 7);//bit 6:0 g_stc_target default 98 bit 7 stc_mod default 1
        dsp->cfg.STC_TB = 20;//0-4096 microsecond default 20 microsecond
         // ftc
        dsp->cfg.FTC_CTRL = 5 + (0 << 3);//bit 2:0 ftc_cfg bit 3 ftc_en
          // edet
        dsp->cfg.AATG1_CTRL = 3 + (2 << 3) + (1 << 6) + (0 << 8) + (0 << 9) + (0 << 10);//bit2:0 aatg1_cn bit5:3 aatg1_cw bit7:6 aatg1_alpha bit8 aatg1_off bit9 aatg1_buf_full bit10 aatg1_buf_half bit15:11 0
        dsp->cfg.AATG2_CTRL = 3 + (2 << 3) + (1 << 6) + (0 << 8) + (0 << 9) + (0 << 10);//bit2:0 aatg1_cn bit5:3 aatg1_cw bit7:6 aatg1_alpha bit8 aatg1_off bit9 aatg1_buf_full bit10 aatg1_buf_half bit15:11 0
        dsp->cfg.ATG_CTRL = 3 + (5 << 3) + (7 << 6) + (1 << 9);////bit2:0 atg_tau bit5:3 atg_alpha bit8:6 atg_ini bit9 atg_cfg bit15:10 0
        dsp->cfg.EEVAL_CTRL = 15 + (2 << 4) + (1 << 7);//bit3:0 eeval_sel bit6:4 eeval_sens bit6:8 eeval_dma_val 
        //nfd
        dsp->cfg.nfd_th = 500;//to be tuned
        dsp->cfg.measure_mod = 0; //0:direct measure 1:indirect measure
        dsp->cfg.NFD_CTRL = 3 + (1 << 3) + (0 << 5);//bit2:0 nfd_sens bit4:3 nfd_irq_cfg bit7:5 nfd_max_num

        //init
        dsp->io.frame_num = 0;
        //envp output init
        dsp->io.envpio.env_phi = 0;
        dsp->io.envpio.env_i = 0;
        dsp->io.envpio.env_q = 0;
        dsp->io.envpio.envd_raw = 0;
        dsp->io.envpio.env_phid = 0;
        //interrupt init
        dsp->io.DSP_IRQ_STATUS[0] = 0;

       
    }
    else {
        
        dofile(file, &dsp->cfg);
        dsp->cfg.fc = (u64_t)dsp->cfg.brg_fc * 24000000 / (1 << 22);
        
        

        if (dsp->cfg.dsr_sel == 1)
        {
            dsp->cfg.filt_len_code = (dsp->cfg.npulse - 1) / 2;
        }
        else
        {
            dsp->cfg.filt_len_code = dsp->cfg.npulse - 1;
        }
        dsp->cfg.filt_len_fix = dsp->cfg.filt_len_code;
        dsp->cfg.ENVP_FILT_CFG = (dsp->cfg.filt_len_fix & 0x7F) | ((dsp->cfg.filt_len_code & 0x7F) << 7) | ((dsp->cfg.f1_coeff_sel & 0x1) << 14) | ((dsp->cfg.f2_coeff_sel & 0x1) << 15);
        dsp->cfg.COEFF.length = 0;
        for (int i = 0; i < 64; i++)
        {
            dsp->cfg.COEFF.coeff[i] = 0;
        }
        generateAdvCoeff(&dsp->cfg.COEFF, dsp->cfg.fc, dsp->cfg.bandPass, dsp->cfg.npulse, dsp->cfg.dsr_sel);

        //init
        dsp->io.frame_num = 0;
        //envp output init
        dsp->io.envpio.env_phi = 0;
        dsp->io.envpio.env_i = 0;
        dsp->io.envpio.env_q = 0;
        dsp->io.envpio.envd_raw = 0;
        dsp->io.envpio.env_phid = 0;
        //interrupt init
        dsp->io.DSP_IRQ_STATUS[0] = 0;
        dsp->io.ampd_io.ms_ts = 0;
        
    }
   // return NULL;
}

void dspReleaseConfig(dsp_config_t *dsp)
{
    if (NULL == dsp) {
        return;
    }
    free(dsp);
}

int dsp_process(dsp_config_t *dsp, uint16_t sample, uint32_t timestamp, u1_t* mod_switch)
{
    // algo process
    s14_t in_data = sample;
    dsp->io.ms_ts  = timestamp;
    dsp->io.envpio.adc_eoc      = 1;
    dsp->io.envpio.burst_sta    = 0; 
    if (dsp->cfg.measure_mod)
        dsp->io.envpio.rtm_rt_end = 1;
    else if (dsp->io.ms_ts < 1000) //(dsp->io.frame_num < dsp->cfg.ring_bin)
        dsp->io.envpio.rtm_rt_end = 0;
    else
        dsp->io.envpio.rtm_rt_end = 1;
    dsp->io.burst_en = 1;
    if ((dsp->io.ms_ts >= dsp->cfg.STC_START) && (dsp->io.ms_ts <= dsp->cfg.STC_START + dsp->io.ms_ts - dsp->io.ampd_io.ms_ts))
    {
        dsp->inst.ampd.stc_tb = dsp->cfg.STC_TB;
    }

    if (mod_switch[5] == 0)
        dsp_process_block_api(&dsp->inst, in_data, &dsp->io, &dsp->cfg);
    else
        dsp_process_api(&dsp->inst, in_data, &dsp->io, &dsp->cfg);
    dsp->io.frame_num++;
}

int main()
{
    

    FILE* fp1 = NULL;
    FILE* fp2 = NULL;
    //const char* indata_path = "../data/ADC_DTA/fixed_path_bin/16pulse_320cm_adc.bin";//for fix path
    const char* indata_path = "../data/ADC_DTA/advanced_path_bin/96pulse_320cm_adv.bin";//for fix path
    const char* msts_path = "../data/ADC_DTA/fixed_path_bin/ms_ts.bin";
    fp1 = fopen(indata_path, "rb");
    fp2 = fopen(msts_path, "rb");
    fseek(fp1, 0L, SEEK_END);
    u32_t dta_size = ftell(fp1);
    fseek(fp2, 0L, SEEK_END);
    u32_t ts_size = ftell(fp2);
    fseek(fp1, 0L, SEEK_SET);//back to the start of bin
    u32_t size = dta_size / sizeof(u14_t);
    u14_t* input_data = (u14_t*)malloc(dta_size);
    fread(input_data, sizeof(u14_t), size, fp1);
    u16_t* ms_ts = (u16_t*)malloc(ts_size);
    fseek(fp2, 0L, SEEK_SET);//back to the start of bin
    fread(ms_ts, sizeof(u16_t), size, fp2);

    dsp_config_t dsp;
    /*Get config*/
    dspGetConfig(profile, &dsp);
    printlog(&dsp.cfg);
    //edet param
    //u8_t STG_TH[] = { 128,64,32,32,32,32,32,32 };
    u9_t STG_TB[] = { 238,238,238,238,238,238,238,238 };
    s8_t STG_STEP[] = { -2,-2,-1,-1,0,0,0,0 };
    dsp.io.edet_io.STG_TH = 128;
    //switch key
    u1_t mod_switch[] = {
        0 /* envp */,
        0 /* ampd */,
        0 /* edet */,
        0 /*nfd*/,
        0 /*rfm*/,
        1 /*overall*/,
    };
    if (mod_switch[5] == 0) 
        dsp_init_block_api(&dsp.inst, dsp.sample_rate, &dsp.cfg, &dsp.io, &mod_switch); //ini
    else
        dsp_init_api(&dsp.inst, dsp.sample_rate, &dsp.cfg, &dsp.io);
    for (u32_t i = 0; i < size; i++) {
        if (i > dsp.io.envpio.delay * dsp.cfg.fmode * (dsp.cfg.dsr_sel + 1)) {
            uint16_t k = floor(ms_ts[i] / (4096));
            if (k > 7)
                k = 7;
            dsp.io.edet_io.STG_TB = STG_TB[k];
            dsp.io.edet_io.STG_STEP = STG_STEP[k];
        }
        dsp_process(&dsp, input_data[i], ms_ts[i], &mod_switch); // process
    }

    //memory free
    if (mod_switch[5] == 0)
        dsp_destroy_block_api(&dsp.inst, &dsp.cfg, &dsp.io);
    else
        dsp_destroy_api(&dsp.inst, &dsp.cfg, &dsp.io);
    //dspReleaseConfig(dsp);
    fclose(fp1);
    fclose(fp2);
    return 0;
}
