/*
  Copyright (c) 2009 Dave Gamble

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"
#include "../include/dsp_public.h"

typedef struct
{
	int id;
	int firstName;
	int lastName;
	int email;
	int age;
	int height;
}people;

void printlog(DspConfig* config);
/* Parse text to JSON, then render back to text, and print! */
void doit(char* text)
{
	char* out; cJSON* json;

	json = cJSON_Parse(text);
	if (!json) { printf("Error before: [%s]\n", cJSON_GetErrorPtr()); }
	else
	{
		out = cJSON_Print(json);
		cJSON_Delete(json);
		printf("%s\n", out);
		free(out);
	}
}




//parse a struct array
int cJSON_to_struct_array(char* text, DspConfig* config)
{
	cJSON* json, * arrayItem, * item, * object;

	json = cJSON_Parse(text);
	if (!json)
	{
		printf("Error before: [%s]\n", cJSON_GetErrorPtr());
	}
	else
	{
		arrayItem = cJSON_GetObjectItem(json, "profile");
		if (arrayItem != NULL)
		{
			int size = cJSON_GetArraySize(arrayItem);
			//printf("cJSON_GetArraySize: size=%d\n",size);

			object = cJSON_GetArrayItem(arrayItem, 0);


			item = cJSON_GetObjectItem(object, "fix_path_process");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->fix_path_process = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get fix_path_process failed\n");
			}
			item = cJSON_GetObjectItem(object, "bias");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->bias = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get bias failed\n");
			}

			int rfmstart = 0, rfm_width = 0, rfmstatus = 0;
			item = cJSON_GetObjectItem(object, "RING_FREQ_START");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				rfmstart = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get rfmstart failed\n");
			}
			item = cJSON_GetObjectItem(object, "RING_FREQ_WIDTH");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				rfm_width = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get rfm_width failed\n");
			}
			item = cJSON_GetObjectItem(object, "rfmstatus");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				rfmstatus = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get rfmstatus failed\n");
			}
			config->RFM_CTRL = (rfmstatus << 10) + (rfm_width << 5) + rfmstart;

			item = cJSON_GetObjectItem(object, "SAMPLING_FREQUENCY");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n",item->type,item->string,item->valueint);
				config->brg_fc = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get fc failed\n");
			}

			item = cJSON_GetObjectItem(object, "BURST_FREQ");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n",item->type,item->string,item->valueint);
				config->brg_fc = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get fc failed\n");
			}

			item = cJSON_GetObjectItem(object, "CALIBRATION_GAIN");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n",item->type,item->string,item->valueint);
				config->g_cal = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get g_cal failed\n");
			}

			item = cJSON_GetObjectItem(object, "DS");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n",item->type,item->string,item->valueint);
				config->dsr_sel = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get DS failed\n");
			}

			item = cJSON_GetObjectItem(object, "MEAS_FILTER_BW");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n",item->type,item->string,item->valueint);
				config->envp_cfg = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get MEAS_FILTER_BW failed\n");
			}

			item = cJSON_GetObjectItem(object, "envp_afc");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n",item->type,item->string,item->valueint);
				config->envp_afc = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get envp_afc failed\n");
			}
			item = cJSON_GetObjectItem(object, "aspi_sel");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->aspi_sel = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get aspi_sel failed\n");
			}
			item = cJSON_GetObjectItem(object, "fspi_sel");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->fspi_sel = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get fspi_sel failed\n");
			}

			config->ENVP_CFG = (config->envp_cfg & 0x07) | ((config->envp_afc & 0x01) << 3) | ((config->g_cal & 0x1f) << 4) | ((config->dsr_sel & 0x1) << 9) | ((config->fspi_sel & 0x1) << 10) | ((config->aspi_sel & 0x1) << 11);


			item = cJSON_GetObjectItem(object, "envp_restart");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->envp_restart = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get envp_restart failed\n");
			}
			item = cJSON_GetObjectItem(object, "envp_afc_brk");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->envp_afc_brk = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get envp_afc_brk failed\n");
			}
			config->ENVP_CTRL = (config->envp_afc_brk & 0x01) | ((config->envp_restart & 0x01) << 1);

			item = cJSON_GetObjectItem(object, "fmode");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->fmode = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get fmode failed\n");
			}

			item = cJSON_GetObjectItem(object, "BURST_PULSES");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->npulse = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get npulse failed\n");
			}

			item = cJSON_GetObjectItem(object, "bandPass");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->bandPass = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get bandPass failed\n");
			}
			item = cJSON_GetObjectItem(object, "f1_coeff_sel");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->f1_coeff_sel = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get f1_coeff_sel failed\n");
			}
			item = cJSON_GetObjectItem(object, "f2_coeff_sel");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->f2_coeff_sel = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get f2_coeff_sel failed\n");
			}

			int FTC_EN = 0, FTC_CFG = 0;
			item = cJSON_GetObjectItem(object, "FTC_EN");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				FTC_EN = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get FTC_EN failed\n");
			}
			item = cJSON_GetObjectItem(object, "FTC_CFG");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				FTC_CFG = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get FTC_CFG failed\n");
			}

			config->FTC_CTRL = FTC_CFG + (FTC_EN << 3);


			item = cJSON_GetObjectItem(object, "BURST_FREQ");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->fc = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get BURST_FREQ failed\n");
			}

			int STC_MOD = 0, STC_GAIN = 0;
			item = cJSON_GetObjectItem(object, "STC_START");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->STC_START = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get STC_START failed\n");
			}
			item = cJSON_GetObjectItem(object, "STC_MOD");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				STC_MOD = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get STC_MOD failed\n");
			}
			item = cJSON_GetObjectItem(object, "STC_GAIN");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				STC_GAIN = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get STC_GAIN failed\n");
			}
			item = cJSON_GetObjectItem(object, "STC_TB");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->STC_TB = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get STC_TB failed\n");
			}
			config->STC_CTRL = STC_GAIN + (STC_MOD << 7);

			item = cJSON_GetObjectItem(object, "AMPD_CTRL");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->AMPD_CTRL = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get AMPD_CTRL failed\n");
			}


			int ATG_CFG = 0, ATG_INI = 0, ATG_ALPHA = 0, ATG_TAU = 0;
			item = cJSON_GetObjectItem(object, "ATG_CFG");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				ATG_CFG = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get ATG_CFG failed\n");
			}
			item = cJSON_GetObjectItem(object, "ATG_INI");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				ATG_INI = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get ATG_INI failed\n");
			}
			item = cJSON_GetObjectItem(object, "ATG_ALPHA");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				ATG_ALPHA = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get ATG_ALPHA failed\n");
			}
			item = cJSON_GetObjectItem(object, "ATG_TAU");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				ATG_TAU = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get ATG_TAU failed\n");
			}
			config->ATG_CTRL = ATG_TAU + (ATG_ALPHA << 3) + (ATG_INI << 6) + (ATG_CFG << 9);


			int EEVAL_VAL = 0, EEVAL_SENS = 0, EEVAL_SEL = 0;
			item = cJSON_GetObjectItem(object, "EEVAL_VAL");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				EEVAL_VAL = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get EEVAL_VAL failed\n");
			}
			item = cJSON_GetObjectItem(object, "EEVAL_SENS");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				EEVAL_SENS = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get EEVAL_SENS failed\n");
			}
			item = cJSON_GetObjectItem(object, "EEVAL_SEL");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				EEVAL_SEL = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get EEVAL_SEL failed\n");
			}
			config->EEVAL_CTRL = EEVAL_SEL + (EEVAL_SENS << 4) + (EEVAL_VAL << 7);

			int AATG_OFF = 0, AATG_ALPHA = 0, AATG_CW = 0, AATG_CN = 0, aatg_buf_full = 0, aatg_buf_half = 0;
			item = cJSON_GetObjectItem(object, "AATG_OFF");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				AATG_OFF = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get AATG_OFF failed\n");
			}
			item = cJSON_GetObjectItem(object, "AATG_ALPHA");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				AATG_ALPHA = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get AATG_ALPHA failed\n");
			}
			item = cJSON_GetObjectItem(object, "AATG_CW");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				AATG_CW = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get AATG_CW failed\n");
			}
			item = cJSON_GetObjectItem(object, "AATG_CN");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				AATG_CN = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get AATG_CN failed\n");
			}
			item = cJSON_GetObjectItem(object, "aatg_buf_full");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				aatg_buf_full = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get aatg_buf_full failed\n");
			}
			item = cJSON_GetObjectItem(object, "aatg_buf_half");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				aatg_buf_half = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get aatg_buf_half failed\n");
			}
			config->AATG1_CTRL = AATG_CN + (AATG_CW << 3) + (AATG_ALPHA << 6) + (AATG_OFF << 8) + (aatg_buf_full << 9) + (aatg_buf_half << 10);
			config->AATG2_CTRL = config->AATG1_CTRL;


			int NFD1_CTRL_SENS = 0, NFD1_CTRL_IRQ = 0, nfd_max_num = 0;
			item = cJSON_GetObjectItem(object, "NFD1_CTRL_SENS");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				NFD1_CTRL_SENS = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get NFD1_CTRL_SENS failed\n");
			}
			item = cJSON_GetObjectItem(object, "NFD1_CTRL_IRQ");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				NFD1_CTRL_IRQ = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get NFD1_CTRL_IRQ failed\n");
			}
			item = cJSON_GetObjectItem(object, "nfd_max_num");
			if (item != NULL)
			{
				//printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				nfd_max_num = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get nfd_max_num failed\n");
			}
			item = cJSON_GetObjectItem(object, "NFD1_THRESHOLD");
			if (item != NULL)
			{
				////printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->nfd_th = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get NFD1_THRESHOLD failed\n");
			}

			config->NFD_CTRL = NFD1_CTRL_SENS + (NFD1_CTRL_IRQ << 3) + (nfd_max_num << 5);

			item = cJSON_GetObjectItem(object, "measure_mod");
			if (item != NULL)
			{
				////printf("cJSON_GetObjectItem: type=%d, string is %s, valueint=%d\n", item->type, item->string, item->valueint);
				config->measure_mod = item->valueint;
			}
			else
			{
				printf("cJSON_GetObjectItem: get measure_mod failed\n");
			}


		}

		//printlog(config);

		cJSON_Delete(json);
	}
	return 0;
}

void printlog(DspConfig* config)
{
	printf("config->fix_path_process: %d\n", config->fix_path_process);
	printf("config->bias: %d\n", config->bias);
	printf("config->RFM_CTRL: %d , rfmstart: %d, rfm_width: %d, rfmstatus: %d\n", config->RFM_CTRL, (config->RFM_CTRL >> 0) & 0x1f, (config->RFM_CTRL >> 5) & 0x1f, (config->RFM_CTRL >> 10) & 0x1);
	printf("config->fc: %d\n", config->fc);
	printf("config->g_cal: %d\n", config->g_cal);
	printf("config->dsr_sel: %d\n", config->dsr_sel);
	printf("config->envp_cfg: %d\n", config->envp_cfg);
	printf("config->envp_afc: %d\n", config->envp_afc);
	printf("config->aspi_sel: %d\n", config->aspi_sel);
	printf("config->fspi_sel: %d\n", config->fspi_sel);
	printf("config->envp_restart: %d\n", config->envp_restart);
	printf("config->envp_afc_brk: %d\n", config->envp_afc_brk);
	printf("config->fmode: %d\n", config->fmode);
	printf("config->npulse: %d\n", config->npulse);
	printf("config->bandPass: %d\n", config->bandPass);
	printf("config->f1_coeff_sel: %d\n", config->f1_coeff_sel);
	printf("config->f2_coeff_sel: %d\n", config->f2_coeff_sel);
	printf("config->FTC_CTRL: %d, ftc_cfg: %d, ftc_en:%d\n", config->FTC_CTRL, (config->FTC_CTRL >> 0) & 0x7, (config->FTC_CTRL >> 3) & 0x1);
	printf("config->STC_START: %d\n", config->STC_START);
	printf("config->STC_CTRL: %d, g_stc_target: %d, stc_mod: %d\n", config->STC_CTRL, (config->STC_CTRL >> 0) & 0x7f, (config->STC_CTRL >> 7) & 0x1);
	printf("config->STC_TB: %d\n", config->STC_TB);
	printf("config->AMPD_CTRL: %d\n", config->AMPD_CTRL);
	printf("config->ATG_CTRL: %d, atg_tau: %d, atg_alpha: %d, atg_ini: %d, atg_cfg: %d \n", config->ATG_CTRL, (config->ATG_CTRL >> 0) & 0x7, (config->ATG_CTRL >> 3) & 0x3, (config->ATG_CTRL >> 6) & 0x7, (config->ATG_CTRL >> 9) & 0x1);
	printf("config->EEVAL_CTRL: %d, eeval_sel: %d, eeval_sens: %d, eeval_dma_val: %d\n", config->EEVAL_CTRL, (config->EEVAL_CTRL >> 0) & 0xf, (config->EEVAL_CTRL >> 4) & 0x7, (config->EEVAL_CTRL >> 7) & 0x1);
	printf("config->AATG1_CTRL: %d, aatg1_cn: %d, aatg1_cw: %d, aatg1_alpha: %d, aatg1_off: %d, aatg1_buf_full: %d, aatg1_buf_half: %d\n", config->AATG1_CTRL, (config->AATG1_CTRL >> 0) & 0x7, (config->AATG1_CTRL >> 3) & 0x7, (config->AATG1_CTRL >> 6) & 0x3, (config->AATG1_CTRL >> 8) & 0x1, (config->AATG1_CTRL >> 9) & 0x1, (config->AATG1_CTRL >> 10) & 0x1);
	printf("config->NFD_CTRL: %d, nfd_sens: %d, nfd_irq_cfg: %d, nfd_max_num: %d\n", config->NFD_CTRL, (config->NFD_CTRL >> 0) & 0x7, (config->NFD_CTRL >> 3) & 0x3, (config->NFD_CTRL >> 5) & 0x7);
	printf("config->nfd_th: %d\n", config->nfd_th);
	printf("config->measure_mod: %d\n", config->measure_mod);

}

/* Read a file, parse, render back, etc. */
void dofile(char* filename, DspConfig* config)
{
	FILE* f; int len; char* data;

	f = fopen(filename, "rb"); fseek(f, 0, SEEK_END); len = ftell(f); fseek(f, 0, SEEK_SET);
	data = (char*)malloc(len + 1); fread(data, 1, len, f); fclose(f);
	//doit(data);
	printf("read file %s complete, len=%d.\n", filename, len);

	cJSON_to_struct_array(data, config);
	free(data);
}



//int main (int argc, const char * argv[]) {
//	/* a bunch of json: */
//	/*char text1[]="{\n\"name\": \"Jack (\\\"Bee\\\") Nimble\", \n\"format\": {\"type\":       \"rect\", \n\"width\":      1920, \n\"height\":     1080, \n\"interlace\":  false,\"frame rate\": 24\n}\n}";	
//	char text2[]="[\"Sunday\", \"Monday\", \"Tuesday\", \"Wednesday\", \"Thursday\", \"Friday\", \"Saturday\"]";
//	char text3[]="[\n    [0, -1, 0],\n    [1, 0, 0],\n    [0, 0, 1]\n	]\n";
//	char text4[]="{\n		\"Image\": {\n			\"Width\":  800,\n			\"Height\": 600,\n			\"Title\":  \"View from 15th Floor\",\n			\"Thumbnail\": {\n				\"Url\":    \"http:/*www.example.com/image/481989943\",\n				\"Height\": 125,\n				\"Width\":  \"100\"\n			},\n			\"IDs\": [116, 943, 234, 38793]\n		}\n	}";
//	char text5[]="[\n	 {\n	 \"precision\": \"zip\",\n	 \"Latitude\":  37.7668,\n	 \"Longitude\": -122.3959,\n	 \"Address\":   \"\",\n	 \"City\":      \"SAN FRANCISCO\",\n	 \"State\":     \"CA\",\n	 \"Zip\":       \"94107\",\n	 \"Country\":   \"US\"\n	 },\n	 {\n	 \"precision\": \"zip\",\n	 \"Latitude\":  37.371991,\n	 \"Longitude\": -122.026020,\n	 \"Address\":   \"\",\n	 \"City\":      \"SUNNYVALE\",\n	 \"State\":     \"CA\",\n	 \"Zip\":       \"94085\",\n	 \"Country\":   \"US\"\n	 }\n	 ]";*/
//
//	/* Process each json textblock by parsing, then rebuilding: */
//	//doit(text1);
//	//doit(text2);	
//	//doit(text3);
//	//doit(text4);
//	//doit(text5);
//
//	 /*Parse standard testfiles: */
//	dofile("tests/1.json"); 
//
//
//	/* Now some samplecode for building objects concisely: */
//	//create_objects();
//	
//	return 0;
//}
