#ifndef readjson__h
#define readjson__h

#ifdef __cplusplus
extern "C"
{
#endif


	void dofile(char* filename, DspConfig* config);
	void printlog(DspConfig* config);
#ifdef __cplusplus
}
#endif

#endif